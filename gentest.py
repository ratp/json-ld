import requests
import os

if not(os.path.exists('src/tests')):
    os.mkdir('src/tests')

TESTS = [ 'expand' ]
BASE_URL = 'https://w3c.github.io/json-ld-api/tests/'

for test in TESTS:
    print('GENERATING', test, 'tests\n')
    r = requests.get(BASE_URL + test + '-manifest.jsonld')
    json = r.json()
    file = open(f'src/tests/{test}.rs', 'w')
    file.write('use pretty_assertions::{assert_eq, assert_ne};\n\n')
    total = len(json['sequence'])
    idx = 1
    for test_case in json['sequence']:
        fn_name = test_case['@id'][1:]
        print('Generating test', fn_name, '(' + str(idx) + '/' + str(total) + ')')
        comment = '// ' + test_case['name'] + ('\n//\n// ' + test_case['purpose'] + '\n' if test_case.get('purpose') is not None else '\n')
        neg_test = 'jld:NegativeEvaluationTest' in test_case['@type']
        error_code = test_case['expectErrorCode'] if neg_test else None
        url = BASE_URL + test_case['input']
        inp = requests.get(url).text

        file.write(comment)
        file.write('#[tokio::test]\n')
        file.write('async fn {fn_name}() {{\n'.format(fn_name = fn_name))
        file.write('    let input = r###"' + inp + '"###;\n')
        file.write('    let result = crate::tests::' + test + '_base(input, "' + url + '").await;\n')
        if neg_test:
            file.write('    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "' + error_code + '");')
        else:
            out = requests.get(BASE_URL + test_case['expect']).text
            file.write('    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"' + out + '"###).expect("Invalid output JSON"));')
        file.write('}\n\n')
        idx += 1

    file.close()
