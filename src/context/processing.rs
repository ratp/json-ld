use std::collections::HashMap;
use iref::{IriBuf, IriRef};
use futures_util::future::{BoxFuture, FutureExt};

use crate::context::term_definition::{TermDef, create_term_def, CreateTermError};
use crate::expansion::iri::{ExpandedIri, IriExpandable};
use crate::types::Direction;
use crate::context::{merge_maps, load_document};

#[derive(Clone, PartialEq)]
pub struct Context {
    pub term_defs: HashMap<String, TermDef>,
    pub base_iri: IriBuf,
    pub original_base_url: IriBuf,
    /// Note: it might only be `ExpandedIri::Null`, `ExpandedIri::BlankNodeIdent` or
    /// `ExpandedIri::Iri`.
    pub vocabulary_mapping: ExpandedIri,
    pub default_language: Option<String>,
    pub default_base_direction: Option<Direction>,
    pub previous_context: Option<Box<Context>>,
}

#[derive(Debug)]
pub enum ContextProcessingError {
    InvalidContextNullification,
    ContextOverflow,
    RemoteContextLoading(reqwest::Error),
    InvalidRemoteContext,
    InvalidLocalContext,
    InvalidVersion,
    InvalidImport,
    InvalidContextEntry,
    InvalidBaseIri,
    InvalidVocabMapping,
    InvalidDefaultLanguage,
    InvalidBaseDirection,
    InvalidPropagate,
    TermCreation(CreateTermError),
}

impl ContextProcessingError {
    pub(crate) fn error_code(&self) -> &'static str {
        use ContextProcessingError::*;
        match *self {
            InvalidContextNullification => "invalid context nullification",
            ContextOverflow => "context overflow",
            RemoteContextLoading(_) => "remote context loading error",
            InvalidRemoteContext => "invalid remote context",
            InvalidLocalContext => "invalid local context",
            InvalidVersion => "invalid version",
            InvalidImport => "invalid @import value",
            InvalidContextEntry => "invalid context entry",
            InvalidBaseIri => "invalid base iri",
            InvalidVocabMapping => "invalid vocab mapping",
            InvalidDefaultLanguage => "invalid default language",
            InvalidBaseDirection => "invalid base direction",
            InvalidPropagate => "invalid propagate",
            TermCreation(ref e) => e.error_code(),
        }
    }
}

impl From<reqwest::Error> for ContextProcessingError {
    fn from(err: reqwest::Error) -> ContextProcessingError {
        ContextProcessingError::RemoteContextLoading(err)
    }
}

impl From<CreateTermError> for ContextProcessingError {
    fn from(err: CreateTermError) -> ContextProcessingError {
        ContextProcessingError::TermCreation(err)
    }
}

impl Context {
    fn contains_protected_term_defintions(&self) -> bool {
        self.term_defs.iter().any(|(_, x)| x.protected)
    }
}

const MAX_REMOTE_CONTEXTS: usize = 1024;

pub(crate) async fn async_fold<A, B, F: std::future::Future<Output=B>>(i: impl Iterator<Item=A>, d: B, f: impl Fn(B, A) -> F) -> B {
    let mut res = d;
    for x in i {
        res = f(res, x).await;
    }
    res
}

struct State {
    result: Context,
    active_ctx: Context,
    base_url: IriBuf,
    remote_ctxs: Vec<IriBuf>,
}

// https://www.w3.org/TR/json-ld11-api/#algorithm
pub fn context_processing<'a>(
    active_ctx: Context,
    local_ctx: serde_json::Value,
    base_url: IriBuf,
    remote_ctxs: Option<Vec<IriBuf>>,
    override_protected: Option<bool>,
    propagate: Option<bool>,
) -> BoxFuture<'a, Result<Context, ContextProcessingError>> {
    async move {
        // default parameters values
        let remote_ctxs = remote_ctxs.unwrap_or_default();
        let override_protected = override_protected.unwrap_or(false);
        let propagate = propagate.unwrap_or(true);

        // Additional step that is not in the spec:
        // If a context is including itself, just continue.
        if remote_ctxs.iter().find(|c| **c == base_url).is_some() {
            return Ok(active_ctx);
        }

        // Step 1
        let result = active_ctx.clone();
        // Step 2
        let propagate = local_ctx["@propagate"].as_bool().unwrap_or(propagate);
        // Step 3
        let result = set_previous_context(propagate, result, active_ctx.clone());

        // Step 4
        let local_ctx = match local_ctx {
            serde_json::Value::Array(x) => x,
            x => vec![ x ],
        };

        // Step 5
        let res = async_fold(
            local_ctx.into_iter(),
                Ok(State {
                    result,
                    active_ctx,
                    base_url,
                    remote_ctxs,
                }),
                async move |state, ctx| {
                    if let Ok(state) = state {
                        match ctx {
                            serde_json::Value::Null => {
                                if !override_protected && state.active_ctx.contains_protected_term_defintions() {
                                    Err(ContextProcessingError::InvalidContextNullification)
                                } else {
                                    Ok(State {
                                        result: Context {
                                            term_defs: HashMap::new(),
                                            base_iri: state.active_ctx.original_base_url.clone(),
                                            original_base_url: state.active_ctx.original_base_url.clone(),
                                            vocabulary_mapping: ExpandedIri::Null,
                                            default_language: None,
                                            default_base_direction: None,
                                            previous_context: if !propagate { Some(Box::new(state.result.clone())) } else { None },
                                        },
                                        ..state
                                    })
                                }
                            },
                            serde_json::Value::String(ref string) => {
                                let context = if let Ok(iri_ref) = IriRef::new(string) {
                                    iri_ref.resolved(state.base_url.as_iri())
                                } else {
                                    state.base_url.clone()
                                };

                                if state.remote_ctxs.len() > MAX_REMOTE_CONTEXTS {
                                    Err(ContextProcessingError::ContextOverflow)
                                } else {
                                    let mut remote = state.remote_ctxs.clone();
                                    remote.append(&mut vec![ context.clone() ]);
                                    let state = State {
                                        remote_ctxs: remote,
                                        ..state
                                    };

                                    let context_doc = load_document(context.as_str()).await?;
                                    if let Some(c) = context_doc.get("@context") {
                                        let loaded_context = c;
                                        let processed = context_processing(
                                            state.result,
                                            loaded_context.clone(),
                                            context.clone(),
                                            Some(state.remote_ctxs.clone()),
                                            Some(override_protected),
                                            Some(propagate),
                                        ).await?;
                                        Ok(State { result: processed, ..state })
                                    } else {
                                        Err(ContextProcessingError::InvalidRemoteContext)
                                    }   
                                }
                            },
                            serde_json::Value::Object(obj) => {
                                let obj = obj.clone();
                                if let Some(serde_json::Value::String(ver)) = obj.get("@version") {
                                    if &ver[..] != "1.1" {
                                        return Err(ContextProcessingError::InvalidVersion);
                                    }
                                }

                                if obj.contains_key("@propagate") {
                                    if obj["@propagate"].as_bool().is_none() {
                                        return Err(ContextProcessingError::InvalidPropagate)
                                    }
                                }

                                let obj = if obj.contains_key("@import") {
                                    if let Some(imp) = obj["@import"].as_str() {
                                        let import = IriRef::new(imp)
                                            .map_err(|_| ContextProcessingError::InvalidImport)?
                                            .resolved(state.base_url.as_iri());
                                        let import_doc = load_document(import.as_str()).await?;
                                        if let Some(imported_ctx) = import_doc["@context"].as_object() {
                                            if imported_ctx.contains_key("@import") {
                                                return Err(ContextProcessingError::InvalidContextEntry);
                                            }

                                            merge_maps(obj, imported_ctx.clone())
                                        } else {
                                            return Err(ContextProcessingError::InvalidRemoteContext);
                                        }
                                    } else {
                                        return Err(ContextProcessingError::InvalidImport);
                                    }
                                } else {
                                    obj
                                };

                                let (voc_map, result) = if obj.contains_key("@vocab") {
                                    match obj["@vocab"].expand_to_iri(state.result, Some(true), None, None, None).await {
                                        (res @ ExpandedIri::Null, c) |
                                        (res @ ExpandedIri::BlankNodeIdent(_), c) |
                                        (res @ ExpandedIri::Iri(_), c) => {
                                            (res, c)
                                        },
                                        _ => return Err(ContextProcessingError::InvalidVocabMapping),
                                    }
                                } else {
                                    (state.result.vocabulary_mapping.clone(), state.result)
                                };

                                let res = State {
                                    result: Context {
                                        base_iri: if obj.contains_key("@base") && state.remote_ctxs.is_empty() {
                                            match obj["@base"] {
                                                serde_json::Value::String(ref s) => {
                                                    if let Some(iri) = IriRef::new(&s).ok().map(|x| x.resolved(result.base_iri.as_iri())) {
                                                        iri
                                                    } else {
                                                        return Err(ContextProcessingError::InvalidBaseIri);
                                                    }
                                                },
                                                serde_json::Value::Null => {
                                                    // TODO: delete result.base_iri in some way
                                                    result.base_iri
                                                },
                                                _ => return Err(ContextProcessingError::InvalidBaseIri),
                                            }
                                        } else {
                                            result.base_iri
                                        },
                                        vocabulary_mapping: voc_map,
                                        default_language: if obj.contains_key("@language") {
                                            match obj["@language"] {
                                                serde_json::Value::Null => {
                                                    None
                                                },
                                                serde_json::Value::String(ref s) => {
                                                    // TODO check it looks like a language string (https://tools.ietf.org/html/bcp47)
                                                    Some(s.clone())
                                                },
                                                _ => return Err(ContextProcessingError::InvalidDefaultLanguage),
                                            }
                                        } else {
                                            result.default_language
                                        },
                                        default_base_direction: if obj.contains_key("@direction") {
                                            match obj["@direction"] {
                                                serde_json::Value::Null => {
                                                    None
                                                },
                                                serde_json::Value::String(ref s) => {
                                                    if let Some(dir) = s.parse().ok() {
                                                        Some(dir)
                                                    } else {
                                                        return Err(ContextProcessingError::InvalidBaseDirection);
                                                    }
                                                },
                                                _ => return Err(ContextProcessingError::InvalidBaseDirection),
                                            }
                                        } else {
                                            result.default_base_direction
                                        },
                                        ..result
                                    },
                                    ..state
                                };

                                let base_url = res.base_url.clone();
                                // 5.13
                                let keys_to_filter = ["@base", "@direction", "@import", "@language", "@propagate", "@protected", "@version", "@vocab"];
                                let (res, _, _, _) = async_fold(
                                    obj.clone().keys().filter(|key| !keys_to_filter.contains(&key.as_str())),
                                    Ok((res, HashMap::new(), obj, base_url)),
                                    async move |state: Result<_, ContextProcessingError>, k| {
                                        if let Ok((state, defined, obj, base_url)) = state {
                                            let (ctx, defined) = create_term_def(
                                                state.result.clone(),
                                                serde_json::Value::Object(obj.clone()),
                                                k.clone(),
                                                defined,
                                                Some(base_url.clone()),
                                                obj.get("@protected").and_then(|x| x.as_bool()),
                                                Some(override_protected),
                                            ).await?;
                                            Ok((State { result: ctx, ..state }, defined, obj, base_url))
                                        } else {
                                            state
                                        }
                                    }
                                ).await?;
                                
                                Ok(res)
                            },
                            _ => Err(ContextProcessingError::InvalidLocalContext)
                        }
                    } else {
                        state
                    }
                }).await;
        res.map(|r| r.result)
    }.boxed()
}

fn set_previous_context<'a>(propagate: bool, ctx: Context, active_ctx: Context) -> Context {
    if !propagate && ctx.previous_context.is_none() {
        Context {
            previous_context: Some(Box::new(active_ctx)),
            ..ctx       
        }
    } else {
        ctx
    }
}
