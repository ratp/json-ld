use iref::IriBuf;
use log::warn;
use std::collections::HashMap;
use std::str::FromStr;
use futures_util::future::{BoxFuture, FutureExt};

use crate::types::{
    Direction, Keyword, TypeMapping, 
};
use crate::expansion::iri::{IriExpandable, ExpandedIri};
use crate::context::processing::{context_processing, Context, ContextProcessingError};

#[derive(Clone, PartialEq)]
pub struct TermDef {
    pub iri: ExpandedIri,
    pub prefix: bool,
    pub protected: bool,
    pub reverse_property: bool,
    pub base_url: Option<IriBuf>,
    pub context: Option<Context>,
    pub container_mapping: Vec<Keyword>,
    pub direction_mapping: Option<Direction>,
    pub index_mapping: ExpandedIri,
    pub language_mapping: Option<String>,
    pub nest_value: Option<String>,
    pub type_mapping: TypeMapping,
    pub local_ctx: serde_json::Value,
}

#[derive(Debug)]
pub enum CreateTermError {
    CyclicIriMapping,
    KeywordRedefinition,
    InvalidTermDefinition,
    InvalidTypeMapping,
    InvalidReverseProperty,
    InvalidIriMapping,
    InvalidContainerMapping,
    InvalidScopedContext(Box<ContextProcessingError>),
    InvalidLanguageMapping,
    ProtectedTermRedefinition,
    InvalidPrefixValue,
    InvalidNestValue,
    InvalidBaseDirection,
    InvalidKeywordAlias,
}

impl CreateTermError {
    pub(crate) fn error_code(&self) -> &'static str {
        use CreateTermError::*;
        match self {
            CyclicIriMapping => "cyclic IRI mapping",
            KeywordRedefinition => "keyword redifinition",
            InvalidTermDefinition => "invalid term definition",
            InvalidTypeMapping => "invalid @type mapping",
            InvalidReverseProperty => "invalid reverse property",
            InvalidIriMapping => "invalid IRI mapping",
            InvalidContainerMapping => "invalid container mapping",
            InvalidScopedContext(ref e) => e.error_code(),
            InvalidLanguageMapping => "invalid @language mapping",
            ProtectedTermRedefinition => "protected term redefinition",
            InvalidPrefixValue => "invalid @prefix value",
            InvalidNestValue => "invalid @nest value",
            InvalidBaseDirection => "invalid base direction",
            InvalidKeywordAlias => "invalid keyword alias",
        }
    }
}

// https://www.w3.org/TR/json-ld11-api/#create-term-definition
pub fn create_term_def<'a>(
    mut active_ctx: Context,
    local_ctx: serde_json::Value,
    term: String,
    mut defined: HashMap<String, bool>,
    base_url: Option<IriBuf>,
    protected: Option<bool>,
    override_protected: Option<bool>,
) -> BoxFuture<'a, Result<(Context, HashMap<String, bool>), CreateTermError>> {
    async move {
        let override_protected = override_protected.unwrap_or(false);
        match defined.get(&term) {
            Some(true) => Ok((active_ctx, defined)),
            Some(false) => Err(CreateTermError::CyclicIriMapping),
            None => {
                defined.insert(term.clone(), false);
                let value = local_ctx[&term].clone();
                if &term == "@type" {
                    let valid = value.as_object()
                        .map(|v| {
                            v.keys().len() < 3 &&
                            !v.is_empty() &&
                            (v.get("@container").map(|c| c == "@set").unwrap_or(false) ||
                            v.get("@protected").is_some())
                        })
                        .unwrap_or(false);
                    if !valid {
                        return Err(CreateTermError::KeywordRedefinition);
                    }
                } else if term.starts_with('@') {
                    warn!("Tried to redefine keyword {}. Stopping term creation.", term);
                    return Err(CreateTermError::KeywordRedefinition);
                }

                let previous_def = active_ctx.term_defs.remove(&term);
                let (value, simple_term) = match value {
                    serde_json::Value::Null => ({
                        let mut map = serde_json::Map::new();
                        map.insert("@id".into(), serde_json::Value::Null);
                        map
                    }, false),
                    serde_json::Value::String(ref s) => ({
                        let mut map = serde_json::Map::new();
                        map.insert("@id".into(), serde_json::Value::String(s.clone()));
                        map
                    }, true),
                    serde_json::Value::Object(m) => (m, false),
                    _ => return Err(CreateTermError::InvalidTermDefinition)
                };
                let (type_mapping, active_ctx) = if value.contains_key("@type") {
                    let ty = match value["@type"] {
                        serde_json::Value::String(ref s) => s.clone(),
                        _ => return Err(CreateTermError::InvalidTypeMapping),
                    };
                    match ty.expand_to_iri(
                        active_ctx,
                        None,
                        None,
                        Some(local_ctx.clone()),
                        Some(defined.clone())
                    ).await {
                        (ExpandedIri::Keyword(Keyword::Id), c) => (TypeMapping::Id, c),
                        (ExpandedIri::Keyword(Keyword::Json), c) => (TypeMapping::Json, c),
                        (ExpandedIri::Keyword(Keyword::None), c) => (TypeMapping::None, c),
                        (ExpandedIri::Keyword(Keyword::Vocab), c) => (TypeMapping::Vocab, c),
                        (ExpandedIri::Iri(i), c) => (TypeMapping::Iri(i), c),
                        _ => return Err(CreateTermError::InvalidTypeMapping),
                    }
                } else {
                    (TypeMapping::None, active_ctx)
                };

                let (iri, container_mapping, reverse_property, active_ctx, mut defined) = if value.contains_key("@reverse") {
                    if value.contains_key("@id") || value.contains_key("@nest") {
                        return Err(CreateTermError::InvalidReverseProperty);
                    }
                    if let Some(s) = value["@reverse"].as_str() {
                        if s.starts_with('@') {
                            warn!("@reverse value looking like a keyword: {}", s);
                            return Ok((active_ctx, defined));
                        }

                        match s.expand_to_iri(
                            active_ctx,
                            None,
                            None,
                            Some(local_ctx.clone()),
                            Some(defined.clone())
                        ).await {
                            (ExpandedIri::Null, _) | (ExpandedIri::Keyword(_), _) => return Err(CreateTermError::InvalidIriMapping),
                            (iri, active_ctx) => {
                                let container_mapping = if value.contains_key("@container") {
                                    match value["@container"] {
                                        serde_json::Value::String(ref s) if s == "@set" || s == "@index" => {
                                            vec![ s.parse().expect("Can't parse @set or @index as keywords") ]
                                        },
                                        serde_json::Value::Null => {
                                            vec![]
                                        },
                                        _ => return Err(CreateTermError::InvalidReverseProperty),
                                    }
                                } else {
                                    vec![]
                                };
                                
                                let mut term_defs = active_ctx.term_defs;
                                term_defs.insert(term.clone(), TermDef {
                                    iri: iri.clone(),
                                    container_mapping: container_mapping.clone(),
                                    direction_mapping: None,
                                    index_mapping: ExpandedIri::Null,
                                    base_url: None,
                                    prefix: false,
                                    protected: protected.unwrap_or(false),
                                    reverse_property: false,
                                    context: None,
                                    language_mapping: None,
                                    nest_value: None,
                                    type_mapping: type_mapping.clone(),
                                    local_ctx: local_ctx.clone(),
                                });
                                let active_ctx = Context {
                                    term_defs,
                                    ..active_ctx
                                };
                                defined.insert(term.clone(), true);
                                (iri, container_mapping, true, active_ctx, defined)
                            },
                        }
                    } else {
                        return Err(CreateTermError::InvalidReverseProperty);
                    }
                } else {
                    (ExpandedIri::Null, vec![], false, active_ctx, defined)
                };

                let (iri, mut active_ctx, mut defined) = if value.contains_key("@id") && value["@id"].as_str().map(|id| id != term).unwrap_or(false) {
                    match value["@id"] {
                        serde_json::Value::Null => (iri, active_ctx, defined),
                        serde_json::Value::String(ref s) => {
                            if Keyword::keyword_like(s) {
                                warn!("@id in term definition looking like a keyword: {}", s);
                            }
                            match s.expand_to_iri(
                                active_ctx,
                                None,
                                None,
                                Some(local_ctx.clone()),
                                Some(defined.clone())
                            ).await {
                                (ExpandedIri::Null, _) => return Err(CreateTermError::InvalidIriMapping),
                                (ExpandedIri::Keyword(kw), _) if kw == Keyword::Context => return Err(CreateTermError::InvalidKeywordAlias),
                                (iri @ ExpandedIri::Iri(_), active_ctx) |
                                (iri @ ExpandedIri::Keyword(_), active_ctx) |
                                (iri @ ExpandedIri::BlankNodeIdent(_), active_ctx) => {
                                    if (term.len() > 1 && term[1..term.len() - 1].contains(':')) || term.contains('/') {
                                        defined.insert(term.clone(), true);
                                        let (expanded, active_ctx) = term.expand_to_iri(active_ctx, None, None, Some(local_ctx.clone()), Some(defined.clone())).await;
                                        if expanded != iri {
                                            return Err(CreateTermError::InvalidIriMapping);
                                        } else {
                                            (iri, active_ctx, defined)
                                        }
                                    } else {
                                        (iri, active_ctx, defined)
                                    }
                                }
                            }
                        },
                        _ => return Err(CreateTermError::InvalidIriMapping)
                    }
                } else if term[1..].contains(':') {
                    let mut split = term.splitn(2, ':');
                    let prefix = split.next().unwrap();
                    let suffix = split.next().unwrap();
                    let (active_ctx, defined) = if local_ctx.as_object().map(|l| l.contains_key(prefix)).unwrap_or(false) {
                        create_term_def(
                            active_ctx,
                            local_ctx.clone(),
                            prefix.to_owned(),
                            defined,
                            base_url.clone(),
                            protected,
                            Some(override_protected),
                        ).await?
                    } else {
                        (active_ctx, defined)
                    };

                    let iri = if let Some(TermDef { iri: ExpandedIri::Iri(i), .. }) = active_ctx.term_defs.get(prefix) {
                        IriBuf::new(&(i.to_string() + suffix)).map(ExpandedIri::Iri).unwrap_or(ExpandedIri::Null)
                    } else {
                        // TODO: term might be a blank node identifier
                        IriBuf::new(&term).map(ExpandedIri::Iri).unwrap_or(ExpandedIri::Null)
                    };
                    (iri, active_ctx, defined)
                } else if term.contains('/') {
                    if let (ExpandedIri::Iri(i), c) = local_ctx.expand_to_iri(active_ctx, None, None, None, None).await {
                        (ExpandedIri::Iri(i), c, defined)
                    } else {
                        return Err(CreateTermError::InvalidIriMapping);
                    }
                } else if term == "@type" {
                    (ExpandedIri::Keyword(Keyword::Type), active_ctx, defined)
                } else if let ExpandedIri::Iri(ref voc) = active_ctx.vocabulary_mapping {
                    (IriBuf::new(&(voc.to_string() + &term)).map(ExpandedIri::Iri).unwrap_or(ExpandedIri::Null), active_ctx, defined)
                } else {
                    return Err(CreateTermError::InvalidIriMapping);
                };

                let (container_mapping, type_mapping) = if let Some(cont) = value.get("@container") {
                    let expected_keyword = |k| [
                        "@graph",
                        "@id",
                        "@index",
                        "@language",
                        "@list",
                        "@set",
                        "@type"
                    ].contains(k);
                    let container_mapping: Vec<Keyword> = match cont {
                        serde_json::Value::Array(arr) if arr.len() == 1 => {
                            match arr[0] {
                                serde_json::Value::String(ref s) if expected_keyword(&s.as_str()) => vec![ s.parse().expect("Keyword parse error") ],
                                _ => return Err(CreateTermError::InvalidContainerMapping)
                            }
                        },
                        serde_json::Value::Array(arr) if arr.len() == 2 || arr.len() == 3 => {
                            let mut found_id_or_index = false;
                            for elt in arr {
                                match elt {
                                    serde_json::Value::String(ref s) => if s == "@id" || s == "@index" {
                                        if found_id_or_index {
                                            return Err(CreateTermError::InvalidContainerMapping);
                                        }
                                        found_id_or_index = true;
                                    } else if s != "@set" && s != "@graph" {
                                        return Err(CreateTermError::InvalidContainerMapping);
                                    },
                                    _ => return Err(CreateTermError::InvalidContainerMapping),
                                }
                            }

                            let arr: Vec<_> = arr.iter().filter_map(serde_json::Value::as_str).collect();
                            if !found_id_or_index {
                                if arr.contains(&"@set") {
                                    let mut others = arr.iter().filter(|x| **x != "@set");
                                    if let Some(o) = others.next() {
                                        if !["@index", "@graph", "@id", "@type", "@language"].contains(o) || others.next().is_some() {
                                            return Err(CreateTermError::InvalidContainerMapping);
                                        }
                                    } else {
                                        return Err(CreateTermError::InvalidContainerMapping);
                                    }
                                } else {
                                    return Err(CreateTermError::InvalidContainerMapping);
                                }
                            }

                            arr.into_iter().map(|s| s.parse().expect("Keyword array parsing error")).collect()
                        },
                        serde_json::Value::String(s) if expected_keyword(&s.as_str()) => {
                            vec![ s.parse().expect("Keyword parsing error") ]
                        },
                        _ => return Err(CreateTermError::InvalidContainerMapping)
                    };

                    let type_mapping = if container_mapping.contains(&Keyword::Type) {
                        match type_mapping {
                            TypeMapping::None => {
                                TypeMapping::Id
                            },
                            TypeMapping::Id | TypeMapping::Vocab => type_mapping,
                            _ => return Err(CreateTermError::InvalidTypeMapping),
                        }
                    } else {
                        type_mapping
                    };

                    (container_mapping, type_mapping)
                } else {
                    (Vec::new(), TypeMapping::None)
                };

                let index_mapping = if value.contains_key("@index") {
                    if !container_mapping.contains(&Keyword::Index) {
                        return Err(CreateTermError::InvalidTermDefinition);
                    }

                    if let Some(iri) = value["@index"].as_str().and_then(|i| IriBuf::new(i).ok()) {
                        ExpandedIri::Iri(iri)
                    } else {
                        return Err(CreateTermError::InvalidTermDefinition);
                    }
                } else {
                    ExpandedIri::Null
                };

                // 21
                let local_ctx = if value.contains_key("@context") {
                    match context_processing(active_ctx.clone(), value["@context"].clone(), base_url.clone().expect("TODO"), None, Some(true), None).await {
                        Ok(_) => value["@context"].clone(),
                        Err(e) => return Err(CreateTermError::InvalidScopedContext(Box::new(e))),
                    }
                } else {
                    local_ctx
                };

                let language_mapping = if value.contains_key("@language") && !value.contains_key("@type") {
                    match value["@language"] {
                        serde_json::Value::String(ref s) => Some(s.clone()),
                        serde_json::Value::Null => None,
                        _ => return Err(CreateTermError::InvalidLanguageMapping),
                    }
                } else {
                    None
                };

                let direction_mapping = if value.contains_key("@direction") && !value.contains_key("@type") {
                    if let Some(dir) = value["@direction"].as_str().and_then(|d| Direction::from_str(d).ok()) {
                        Some(dir)
                    } else {
                        return Err(CreateTermError::InvalidBaseDirection);
                    }
                } else {
                    None
                };

                let nest_value = if value.contains_key("@nest") {
                    if let Some(s) = value["@nest"].as_str() {
                        if Keyword::keyword_like(s) && s != "@nest" {
                            return Err(CreateTermError::InvalidNestValue);
                        }

                        Some(s.to_owned())
                    } else {
                        return Err(CreateTermError::InvalidNestValue);
                    }
                } else {
                    None
                };

                let prefix = if value.contains_key("@prefix") {
                    if term.contains(':') || term.contains('/') {
                        return Err(CreateTermError::InvalidTermDefinition);
                    }

                    if let Some(p) = value["@prefix"].as_bool() {
                        p
                    } else {
                        return Err(CreateTermError::InvalidPrefixValue);
                    }
                } else {
                    false
                };

                if value.keys()
                    .filter(|x| ![
                        "@id",
                        "@reverse",
                        "@container",
                        "@context",
                        "@direction",
                        "@index",
                        "@language",
                        "@nest",
                        "@prefix",
                        "@protected",
                        "@type"
                    ].contains(&x.as_str())).count() > 0 {
                    return Err(CreateTermError::InvalidTermDefinition);
                }

                let definition = TermDef {
                    iri,
                    prefix,
                    protected: protected.unwrap_or(false),
                    reverse_property,
                    base_url,
                    context: None, // TODO: set it???
                    container_mapping,
                    direction_mapping,
                    index_mapping,
                    language_mapping,
                    nest_value,
                    type_mapping,
                    local_ctx: local_ctx.clone(),
                };

                // 27
                let definition = match previous_def {
                    Some(prev) if !override_protected && prev.protected => {
                        let def = TermDef { protected: prev.protected, ..definition };
                        if def != prev {
                            return Err(CreateTermError::ProtectedTermRedefinition);
                        }

                        prev
                    },
                    _ => definition,
                };

                active_ctx.term_defs.insert(term.clone(), definition);
                defined.insert(term, true);

                Ok((active_ctx, defined))
            }
        }    
    }.boxed()
}


