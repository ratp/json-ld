pub mod processing;
pub mod term_definition;

async fn get_act<T: serde::de::DeserializeOwned>(url: &str) -> Result<T, reqwest::Error> {
    let client = reqwest::Client::new();
    client.get(url).header("Accept", "application/activity+json")
        .send().await?
        .json().await
}

async fn load_document(url: &str) -> Result<serde_json::Value, reqwest::Error> {
    // TODO: implement the actual spec?
    // https://www.w3.org/TR/json-ld11-api/#dom-loaddocumentcallback
    get_act(url).await
}

fn merge_maps(
    mut map1: serde_json::Map<String, serde_json::Value>,
    map2: serde_json::Map<String, serde_json::Value>,
) -> serde_json::Map<String, serde_json::Value> {
    for (k, v) in map2.into_iter() {
        if !map1.contains_key(&k) {
            map1.insert(k, v);
        }
    }
    map1
}


