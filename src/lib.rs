#![feature(async_closure)]
#![feature(try_trait)]

pub mod context;
pub mod expansion;
pub mod compaction;
pub mod flattening;
#[cfg(test)]
mod tests;
pub mod types;
// TODO: section 8 and 9?
