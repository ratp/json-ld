use crate::expansion::document::{expand_document, DocumentExpansionError};
use crate::context::processing::{Context, context_processing};

mod expand;

async fn expand_base(input: &'static str, bu: &'static str) -> Result<serde_json::Value, DocumentExpansionError> {
    let json = serde_json::from_str::<serde_json::Value>(input).expect("JSON parsing error");
    let base_url = iref::IriBuf::new(bu).unwrap();
    let ctx = context_processing(Context {
        term_defs: std::collections::HashMap::new(),
        base_iri: base_url.clone(),
        original_base_url: base_url.clone(),
        vocabulary_mapping: crate::expansion::iri::ExpandedIri::Null,
        default_language: None,
        default_base_direction: None,
        previous_context: None,
    }, json["@context"].clone(), base_url.clone(), None, None, None).await?;
    let (expanded_ouput, _) = expand_document(
        ctx,
        None,
        json,
        base_url,
        None,
        None,
        None,
    ).await?;
    let expanded_ouput = expanded_ouput.to_json();
    if let Some(graph) = expanded_ouput.get("@graph") {
        Ok(graph.clone())
    } else if expanded_ouput.is_null() {
        Ok(serde_json::json!([]))
    } else if expanded_ouput.is_array() {
        Ok(expanded_ouput)
    } else {
        Ok(serde_json::json!([ expanded_ouput ]))
    }
}
