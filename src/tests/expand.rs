use pretty_assertions::{assert_eq, assert_ne};

// drop free-floating nodes
//
// Expand drops unreferenced nodes having only @id
#[tokio::test]
async fn t0001() {
    let input = r###"{"@id": "http://example.org/test#example"}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0001-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[ ]
"###).expect("Invalid output JSON"));}

// basic
//
// Expanding terms with different types of values
#[tokio::test]
async fn t0002() {
    let input = r###"{
  "@context": {
    "t1": "http://example.com/t1",
    "t2": "http://example.com/t2",
    "term1": "http://example.com/term1",
    "term2": "http://example.com/term2",
    "term3": "http://example.com/term3",
    "term4": "http://example.com/term4",
    "term5": "http://example.com/term5"
  },
  "@id": "http://example.com/id1",
  "@type": "t1",
  "term1": "v1",
  "term2": {"@value": "v2", "@type": "t2"},
  "term3": {"@value": "v3", "@language": "en"},
  "term4": 4,
  "term5": [50, 51]
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0002-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@id": "http://example.com/id1",
  "@type": ["http://example.com/t1"],
  "http://example.com/term1": [{"@value": "v1"}],
  "http://example.com/term2": [{"@value": "v2", "@type": "http://example.com/t2"}],
  "http://example.com/term3": [{"@value": "v3", "@language": "en"}],
  "http://example.com/term4": [{"@value": 4}],
  "http://example.com/term5": [{"@value": 50}, {"@value": 51}]
}]"###).expect("Invalid output JSON"));}

// drop null and unmapped properties
//
// Verifies that null values and unmapped properties are removed from expanded output
#[tokio::test]
async fn t0003() {
    let input = r###"{
  "@id": "http://example.org/id",
  "http://example.org/property": null,
  "regularJson": {
    "nonJsonLd": "property",
    "deep": [{
      "foo": "bar"
    }, {
      "bar": "foo"
    }]
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0003-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[ ]
"###).expect("Invalid output JSON"));}

// optimize @set, keep empty arrays
//
// Uses of @set are removed in expansion; values of @set, or just plain values which are empty arrays are retained
#[tokio::test]
async fn t0004() {
    let input = r###"{
  "@context": {
    "mylist1": {"@id": "http://example.com/mylist1", "@container": "@list"},
    "mylist2": {"@id": "http://example.com/mylist2", "@container": "@list"},
    "myset2": {"@id": "http://example.com/myset2", "@container": "@set"},
    "myset3": {"@id": "http://example.com/myset3", "@container": "@set"}
  },
  "@id": "http://example.org/id",
  "mylist1": { "@list": [ ] },
  "mylist2": "one item",
  "myset2": { "@set": [ ] },
  "myset3": [ "v1" ],
  "http://example.org/list1": { "@list": [ null ] },
  "http://example.org/list2": { "@list": [ {"@value": null} ] },
  "http://example.org/set1": { "@set": [ ] },
  "http://example.org/set2": { "@set": [ null ] },
  "http://example.org/set3": [ ],
  "http://example.org/set4": [ null ],
  "http://example.org/set5": "one item",
  "http://example.org/property": { "@list": "one item" }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0004-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@id": "http://example.org/id",
  "http://example.com/mylist1": [ { "@list": [ ] } ],
  "http://example.com/mylist2": [ { "@list": [ {"@value": "one item"} ] } ],
  "http://example.com/myset2": [ ],
  "http://example.com/myset3": [ {"@value": "v1"} ],
  "http://example.org/list1": [ { "@list": [ ] } ],
  "http://example.org/list2": [ { "@list": [ ] } ],
  "http://example.org/set1": [ ],
  "http://example.org/set2": [ ],
  "http://example.org/set3": [ ],
  "http://example.org/set4": [ ],
  "http://example.org/set5": [ {"@value": "one item"} ],
  "http://example.org/property": [ { "@list": [ {"@value": "one item"} ] } ]
}]
"###).expect("Invalid output JSON"));}

// do not expand aliased @id/@type
//
// If a keyword is aliased, it is not used when expanding
#[tokio::test]
async fn t0005() {
    let input = r###"{
  "@context": {
    "name": "http://xmlns.com/foaf/0.1/name",
    "homepage": {
      "@id": "http://xmlns.com/foaf/0.1/homepage",
      "@type": "@id"
    },
    "know": "http://xmlns.com/foaf/0.1/knows",
    "@iri": "@id"
  },
  "@id": "#me",
  "know": [
    {
      "@id": "http://example.com/bob#me",
      "name": "Bob",
      "homepage": "http://example.com/bob"
    }, {
      "@id": "http://example.com/alice#me",
      "name": "Alice",
      "homepage": "http://example.com/alice"
    }
  ]
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0005-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@id": "https://w3c.github.io/json-ld-api/tests/expand/0005-in.jsonld#me",
  "http://xmlns.com/foaf/0.1/knows": [
    {
      "@id": "http://example.com/bob#me",
      "http://xmlns.com/foaf/0.1/name": [{"@value": "Bob"}],
      "http://xmlns.com/foaf/0.1/homepage": [{
        "@id": "http://example.com/bob"
      }]
    }, {
      "@id": "http://example.com/alice#me",
      "http://xmlns.com/foaf/0.1/name": [{"@value": "Alice"}],
      "http://xmlns.com/foaf/0.1/homepage": [{
        "@id": "http://example.com/alice"
      }]
    }
  ]
}]"###).expect("Invalid output JSON"));}

// alias keywords
//
// Aliased keywords expand in resulting document
#[tokio::test]
async fn t0006() {
    let input = r###"{
  "@context": {
    "http://example.org/test#property1": {
      "@type": "@id"
    },
    "http://example.org/test#property2": {
      "@type": "@id"
    },
    "uri": "@id"
  },
  "http://example.org/test#property1": {
    "http://example.org/test#property4": "foo",
    "uri": "http://example.org/test#example2"
  },
  "http://example.org/test#property2": "http://example.org/test#example3",
  "http://example.org/test#property3": {
    "uri": "http://example.org/test#example4"
  },
  "uri": "http://example.org/test#example1"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0006-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@id": "http://example.org/test#example1",
  "http://example.org/test#property1": [{
    "@id": "http://example.org/test#example2",
    "http://example.org/test#property4": [{"@value": "foo"}]
  }],
  "http://example.org/test#property2": [{
    "@id": "http://example.org/test#example3"
  }],
  "http://example.org/test#property3": [{
    "@id": "http://example.org/test#example4"
  }]
}]"###).expect("Invalid output JSON"));}

// date type-coercion
//
// Expand strings to expanded value with @type: xsd:dateTime
#[tokio::test]
async fn t0007() {
    let input = r###"{
  "@context": {
    "ex": "http://example.org/vocab#",
    "ex:date": {
      "@type": "xsd:dateTime"
    },
    "ex:parent": {
      "@type": "@id"
    },
    "xsd": "http://www.w3.org/2001/XMLSchema#"
  },
  "@id": "http://example.org/test#example1",
  "ex:date": "2011-01-25T00:00:00Z",
  "ex:embed": {
    "@id": "http://example.org/test#example2",
    "ex:parent": "http://example.org/test#example1"
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0007-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@id": "http://example.org/test#example1",
  "http://example.org/vocab#date": [{
    "@value": "2011-01-25T00:00:00Z",
    "@type": "http://www.w3.org/2001/XMLSchema#dateTime"
  }],
  "http://example.org/vocab#embed": [{
    "@id": "http://example.org/test#example2",
    "http://example.org/vocab#parent": [{
      "@id": "http://example.org/test#example1"
    }]
  }]
}]"###).expect("Invalid output JSON"));}

// @value with @language
//
// Keep expanded values with @language, drop non-conforming value objects containing just @language
#[tokio::test]
async fn t0008() {
    let input = r###"{
  "@context": {
    "ex": "http://example.org/vocab#"
  },
  "@id": "http://example.org/test",
  "ex:test": { "@value": "test",  "@language": "en" },
  "ex:drop-lang-only": { "@language": "en" },
  "ex:keep-full-value": { "@value": "only value" }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0008-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.org/test",
    "http://example.org/vocab#test": [ { "@value": "test", "@language": "en" } ],
    "http://example.org/vocab#keep-full-value": [ {"@value": "only value"} ]
  }
]
"###).expect("Invalid output JSON"));}

// @graph with terms
//
// Use of @graph to contain multiple nodes within array
#[tokio::test]
async fn t0009() {
    let input = r###"{
  "@context": {
    "authored": {
      "@id": "http://example.org/vocab#authored",
      "@type": "@id"
    },
    "contains": {
      "@id": "http://example.org/vocab#contains",
      "@type": "@id"
    },
    "contributor": "http://purl.org/dc/elements/1.1/contributor",
    "description": "http://purl.org/dc/elements/1.1/description",
    "name": "http://xmlns.com/foaf/0.1/name",
    "title": {
      "@id": "http://purl.org/dc/elements/1.1/title"
    }
  },
  "@graph": [
    {
      "@id": "http://example.org/test#chapter",
      "description": "Fun",
      "title": "Chapter One"
    },
    {
      "@id": "http://example.org/test#jane",
      "authored": "http://example.org/test#chapter",
      "name": "Jane"
    },
    {
      "@id": "http://example.org/test#john",
      "name": "John"
    },
    {
      "@id": "http://example.org/test#library",
      "contains": {
        "@id": "http://example.org/test#book",
        "contains": "http://example.org/test#chapter",
        "contributor": "Writer",
        "title": "My Book"
      }
    }
  ]
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0009-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.org/test#chapter",
    "http://purl.org/dc/elements/1.1/description": [{"@value": "Fun"}],
    "http://purl.org/dc/elements/1.1/title": [{"@value": "Chapter One"}]
  },
  {
    "@id": "http://example.org/test#jane",
    "http://example.org/vocab#authored": [{
      "@id": "http://example.org/test#chapter"
    }],
    "http://xmlns.com/foaf/0.1/name": [{"@value": "Jane"}]
  },
  {
    "@id": "http://example.org/test#john",
    "http://xmlns.com/foaf/0.1/name": [{"@value": "John"}]
  },
  {
    "@id": "http://example.org/test#library",
    "http://example.org/vocab#contains": [{
      "@id": "http://example.org/test#book",
      "http://example.org/vocab#contains": [{
        "@id": "http://example.org/test#chapter"
      }],
      "http://purl.org/dc/elements/1.1/contributor": [{"@value": "Writer"}],
      "http://purl.org/dc/elements/1.1/title": [{"@value": "My Book"}]
    }]
  }
]"###).expect("Invalid output JSON"));}

// native types
//
// Expanding native scalar retains native scalar within expanded value
#[tokio::test]
async fn t0010() {
    let input = r###"{
  "@context": {
    "d": "http://purl.org/dc/elements/1.1/",
    "e": "http://example.org/vocab#",
    "f": "http://xmlns.com/foaf/0.1/",
    "xsd": "http://www.w3.org/2001/XMLSchema#"
  },
  "@id": "http://example.org/test",
  "e:bool": true,
  "e:int": 123
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0010-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@id": "http://example.org/test",
  "http://example.org/vocab#bool": [{"@value": true}],
  "http://example.org/vocab#int": [{"@value": 123}]
}]"###).expect("Invalid output JSON"));}

// coerced @id
//
// A value of a property with @type: @id coercion expands to a node reference
#[tokio::test]
async fn t0011() {
    let input = r###"{
  "@context": {
    "dc11": "http://purl.org/dc/elements/1.1/",
    "ex": "http://example.org/vocab#",
    "ex:contains": {
      "@type": "@id"
    },
    "xsd": "http://www.w3.org/2001/XMLSchema#"
  },
  "@id": "http://example.org/test#book",
  "dc11:title": "Title",
  "ex:contains": "http://example.org/test#chapter"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0011-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@id": "http://example.org/test#book",
  "http://example.org/vocab#contains": [{
    "@id": "http://example.org/test#chapter"
  }],
  "http://purl.org/dc/elements/1.1/title": [{"@value": "Title"}]
}]"###).expect("Invalid output JSON"));}

// @graph with embed
//
// Use of @graph to contain multiple nodes within array
#[tokio::test]
async fn t0012() {
    let input = r###"{
  "@context": {
    "dc11": "http://purl.org/dc/elements/1.1/",
    "ex": "http://example.org/vocab#",
    "ex:authored": {
      "@type": "@id"
    },
    "ex:contains": {
      "@type": "@id"
    },
    "foaf": "http://xmlns.com/foaf/0.1/",
    "xsd": "http://www.w3.org/2001/XMLSchema#"
  },
  "@graph": [
    {
      "@id": "http://example.org/test#chapter",
      "dc11:description": "Fun",
      "dc11:title": "Chapter One"
    },
    {
      "@id": "http://example.org/test#jane",
      "ex:authored": "http://example.org/test#chapter",
      "foaf:name": "Jane"
    },
    {
      "@id": "http://example.org/test#john",
      "foaf:name": "John"
    },
    {
      "@id": "http://example.org/test#library",
      "ex:contains": {
        "@id": "http://example.org/test#book",
        "dc11:contributor": "Writer",
        "dc11:title": "My Book",
        "ex:contains": "http://example.org/test#chapter"
      }
    }
  ]
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0012-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.org/test#chapter",
    "http://purl.org/dc/elements/1.1/description": [{"@value": "Fun"}],
    "http://purl.org/dc/elements/1.1/title": [{"@value": "Chapter One"}]
  },
  {
    "@id": "http://example.org/test#jane",
    "http://example.org/vocab#authored": [{
      "@id": "http://example.org/test#chapter"
    }],
    "http://xmlns.com/foaf/0.1/name": [{"@value": "Jane"}]
  },
  {
    "@id": "http://example.org/test#john",
    "http://xmlns.com/foaf/0.1/name": [{"@value": "John"}]
  },
  {
    "@id": "http://example.org/test#library",
    "http://example.org/vocab#contains": [{
      "@id": "http://example.org/test#book",
      "http://example.org/vocab#contains": [{
        "@id": "http://example.org/test#chapter"
      }],
      "http://purl.org/dc/elements/1.1/contributor": [{"@value": "Writer"}],
      "http://purl.org/dc/elements/1.1/title": [{"@value": "My Book"}]
    }]
  }
]"###).expect("Invalid output JSON"));}

// expand already expanded
//
// Expand does not mess up already expanded document
#[tokio::test]
async fn t0013() {
    let input = r###"[{
  "@id": "http://example.com/id1",
  "@type": ["http://example.com/t1"],
  "http://example.com/term1": ["v1"],
  "http://example.com/term2": [{"@value": "v2", "@type": "http://example.com/t2"}],
  "http://example.com/term3": [{"@value": "v3", "@language": "en"}],
  "http://example.com/term4": [4],
  "http://example.com/term5": [50, 51]
}]"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0013-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@id": "http://example.com/id1",
  "@type": ["http://example.com/t1"],
  "http://example.com/term1": [{"@value": "v1"}],
  "http://example.com/term2": [{"@value": "v2", "@type": "http://example.com/t2"}],
  "http://example.com/term3": [{"@value": "v3", "@language": "en"}],
  "http://example.com/term4": [{"@value": 4}],
  "http://example.com/term5": [{"@value": 50}, {"@value": 51}]
}]"###).expect("Invalid output JSON"));}

// @set of @value objects with keyword aliases
//
// Expanding aliased @set and @value
#[tokio::test]
async fn t0014() {
    let input = r###"{
  "@context": {
    "ex": "http://example.org/test#",
    "property1": {
      "@id": "http://example.org/test#property1",
      "@type": "@id"
    },
    "property2": {
      "@id": "ex:property2",
      "@type": "@id"
    },
    "uri": "@id",
    "set": "@set",
    "value": "@value",
    "type": "@type",
    "xsd": "http://www.w3.org/2001/XMLSchema#"
  },
  "property1": {
    "uri": "ex:example2",
    "http://example.org/test#property4": "foo"
  },
  "property2": "http://example.org/test#example3",
  "http://example.org/test#property3": {
    "uri": "http://example.org/test#example4"
  },
  "ex:property4": {
    "uri": "ex:example4",
    "ex:property5": [
      {
        "set": [
          {
          "value": "2012-03-31",
          "type": "xsd:date"
          }
        ]
      }
    ]
  },
  "ex:property6": [
    {
      "set": [
        {
        "value": null,
        "type": "xsd:date"
        }
      ]
    }
  ],
  "uri": "http://example.org/test#example1"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0014-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "http://example.org/test#property1": [
      {
        "@id": "http://example.org/test#example2",
        "http://example.org/test#property4": [
          {"@value": "foo"}
        ]
      }
    ],
    "http://example.org/test#property2": [
      { "@id": "http://example.org/test#example3" }
    ],
    "http://example.org/test#property3": [
      { "@id": "http://example.org/test#example4" }
    ],
    "http://example.org/test#property4": [
      {
        "@id": "http://example.org/test#example4",
        "http://example.org/test#property5": [
          {
            "@value": "2012-03-31",
            "@type": "http://www.w3.org/2001/XMLSchema#date"
          }
        ]
      }
    ],
    "http://example.org/test#property6": [],
    "@id": "http://example.org/test#example1"
  }
]
"###).expect("Invalid output JSON"));}

// collapse set of sets, keep empty lists
//
// An array of multiple @set nodes are collapsed into a single array
#[tokio::test]
async fn t0015() {
    let input = r###"{
  "@context": {
    "mylist1": {"@id": "http://example.com/mylist1", "@container": "@list"},
    "mylist2": {"@id": "http://example.com/mylist2", "@container": "@list"},
    "myset1": {"@id": "http://example.com/myset1", "@container": "@set" },
    "myset2": {"@id": "http://example.com/myset2", "@container": "@set" },
    "myset3": {"@id": "http://example.com/myset3", "@container": "@set" }
  },
  "@id": "http://example.org/id",
  "mylist1": [],
  "myset1": { "@set": [] },
  "myset2": [ { "@set": [] }, [], { "@set": [ null ] }, [ null ] ],
  "myset3": [ { "@set": [ "hello", "this" ] }, "will", { "@set": [ "be", "collapsed" ] } ]
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0015-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.org/id",
    "http://example.com/mylist1": [ { "@list": [] } ],
    "http://example.com/myset1": [ ],
    "http://example.com/myset2": [ ],
    "http://example.com/myset3": [
      {"@value": "hello"},
      {"@value": "this"},
      {"@value": "will"},
      {"@value": "be"},
      {"@value": "collapsed"} ]
  }
]
"###).expect("Invalid output JSON"));}

// context reset
//
// Setting @context to null within an embedded object resets back to initial context state
#[tokio::test]
async fn t0016() {
    let input = r###"{
  "@context": {
    "myproperty": { "@id": "http://example.com/myproperty" },
    "mylist1": {"@id": "http://example.com/mylist1", "@container": "@list"},
    "mylist2": {"@id": "http://example.com/mylist2", "@container": "@list"},
    "myset1": {"@id": "http://example.com/myset1", "@container": "@set" },
    "myset2": {"@id": "http://example.com/myset2", "@container": "@set" }
  },
  "@id": "http://example.org/id1",
  "mylist1": [],
  "mylist2": [ 2, "hi" ],
  "myset1": { "@set": [] },
  "myset2": [ { "@set": [] }, [], { "@set": [ null ] }, [ null ] ],
  "myproperty": {
    "@context": null,
    "@id": "http://example.org/id2",
    "mylist1": [],
    "mylist2": [ 2, "hi" ],
    "myset1": { "@set": [] },
    "myset2": [ { "@set": [] }, [], { "@set": [ null ] }, [ null ] ],
    "http://example.org/myproperty2": "ok"
  },
  "http://example.com/emptyobj": {
    "@context": null,
    "mylist1": [],
    "mylist2": [ 2, "hi" ],
    "myset1": { "@set": [] },
    "myset2": [ { "@set": [] }, [], { "@set": [ null ] }, [ null ] ]
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0016-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.org/id1",
    "http://example.com/mylist1": [ { "@list": [] } ],
    "http://example.com/mylist2": [
      { "@list": [ {"@value": 2}, {"@value": "hi"} ] }
    ],
    "http://example.com/myset1": [ ],
    "http://example.com/myset2": [ ],
    "http://example.com/myproperty": [
      {
        "@id": "http://example.org/id2",
        "http://example.org/myproperty2": [ {"@value": "ok"} ]
      }
    ],
    "http://example.com/emptyobj": [ { } ]
  }
]
"###).expect("Invalid output JSON"));}

// @graph and @id aliased
//
// Expanding with @graph and @id aliases
#[tokio::test]
async fn t0017() {
    let input = r###"{
  "@context": {
    "authored": {
      "@id": "http://example.org/vocab#authored",
      "@type": "@id"
    },
    "contains": {
      "@id": "http://example.org/vocab#contains",
      "@type": "@id"
    },
    "contributor": "http://purl.org/dc/elements/1.1/contributor",
    "description": "http://purl.org/dc/elements/1.1/description",
    "name": "http://xmlns.com/foaf/0.1/name",
    "title": {
      "@id": "http://purl.org/dc/elements/1.1/title"
    },
    "id": "@id",
    "data": "@graph"
  },
  "data": [
    {
      "id": "http://example.org/test#chapter",
      "description": "Fun",
      "title": "Chapter One"
    },
    {
      "@id": "http://example.org/test#jane",
      "authored": "http://example.org/test#chapter",
      "name": "Jane"
    },
    {
      "id": "http://example.org/test#john",
      "name": "John"
    },
    {
      "id": "http://example.org/test#library",
      "contains": {
        "@id": "http://example.org/test#book",
        "contains": "http://example.org/test#chapter",
        "contributor": "Writer",
        "title": "My Book"
      }
    }
  ]
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0017-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.org/test#chapter",
    "http://purl.org/dc/elements/1.1/description": [{"@value": "Fun"}],
    "http://purl.org/dc/elements/1.1/title": [{"@value": "Chapter One"}]
  },
  {
    "@id": "http://example.org/test#jane",
    "http://example.org/vocab#authored": [{
      "@id": "http://example.org/test#chapter"
    }],
    "http://xmlns.com/foaf/0.1/name": [{"@value": "Jane"}]
  },
  {
    "@id": "http://example.org/test#john",
    "http://xmlns.com/foaf/0.1/name": [{"@value": "John"}]
  },
  {
    "@id": "http://example.org/test#library",
    "http://example.org/vocab#contains": [{
      "@id": "http://example.org/test#book",
      "http://example.org/vocab#contains": [{
        "@id": "http://example.org/test#chapter"
      }],
      "http://purl.org/dc/elements/1.1/contributor": [{"@value": "Writer"}],
      "http://purl.org/dc/elements/1.1/title": [{"@value": "My Book"}]
    }]
  }
]"###).expect("Invalid output JSON"));}

// override default @language
//
// override default @language in terms; only language-tag strings
#[tokio::test]
async fn t0018() {
    let input = r###"{
  "@context": {
    "ex": "http://example.org/vocab#",
    "@language": "en",
    "de": { "@id": "ex:german", "@language": "de" },
    "nolang": { "@id": "ex:nolang", "@language": null }
  },
  "@id": "http://example.org/test",
  "ex:test-default": [
    "hello",
    1,
    true
  ],
  "de": [
    "hallo",
    2,
    true
  ],
  "nolang": [
    "no language",
    3,
    false
  ]
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0018-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.org/test",
    "http://example.org/vocab#test-default": [ { "@value": "hello", "@language": "en" }, { "@value": 1 }, { "@value": true } ],
    "http://example.org/vocab#german": [ { "@value": "hallo", "@language": "de" }, { "@value": 2 }, { "@value": true } ],
    "http://example.org/vocab#nolang": [ {"@value": "no language"}, { "@value": 3 }, { "@value": false } ]
  }
]
"###).expect("Invalid output JSON"));}

// remove @value = null
//
// Expanding a value of null removes the value
#[tokio::test]
async fn t0019() {
    let input = r###"{
  "@context": {
    "myproperty": "http://example.com/myproperty"
  },
  "myproperty": { "@value" : null }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0019-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[ ]
"###).expect("Invalid output JSON"));}

// do not remove @graph if not at top-level
//
// @graph used under a node is retained
#[tokio::test]
async fn t0020() {
    let input = r###"{
  "@context": {
    "authored": {
      "@id": "http://example.org/vocab#authored",
      "@type": "@id"
    },
    "contains": {
      "@id": "http://example.org/vocab#contains",
      "@type": "@id"
    },
    "contributor": "http://purl.org/dc/elements/1.1/contributor",
    "description": "http://purl.org/dc/elements/1.1/description",
    "name": "http://xmlns.com/foaf/0.1/name",
    "title": {
      "@id": "http://purl.org/dc/elements/1.1/title"
    }
  },
  "@graph": [
    {
      "@id": "http://example.org/test#jane",
      "name": "Jane",
      "authored": {
        "@graph": [
          {
            "@id": "http://example.org/test#chapter1",
            "description": "Fun",
            "title": "Chapter One"
          },
          {
            "@id": "http://example.org/test#chapter2",
            "description": "More fun",
            "title": "Chapter Two"
          }
        ]
      }
    },
    {
      "@id": "http://example.org/test#john",
      "name": "John"
    },
    {
      "@id": "http://example.org/test#library",
      "contains": {
        "@id": "http://example.org/test#book",
        "contains": "http://example.org/test#chapter",
        "contributor": "Writer",
        "title": "My Book"
      }
    }
  ]
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0020-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.org/test#jane",
    "http://xmlns.com/foaf/0.1/name": [ {"@value": "Jane"} ],
    "http://example.org/vocab#authored": [
      {
        "@graph": [
          {
            "@id": "http://example.org/test#chapter1",
            "http://purl.org/dc/elements/1.1/description": [ {"@value": "Fun"} ],
            "http://purl.org/dc/elements/1.1/title": [ {"@value": "Chapter One"} ]
          },
          {
            "@id": "http://example.org/test#chapter2",
            "http://purl.org/dc/elements/1.1/description": [ {"@value": "More fun"} ],
            "http://purl.org/dc/elements/1.1/title": [ {"@value": "Chapter Two"} ]
          }
        ]
      }
    ]
  },
  {
    "@id": "http://example.org/test#john",
    "http://xmlns.com/foaf/0.1/name": [ {"@value": "John"} ]
  },
  {
    "@id": "http://example.org/test#library",
    "http://example.org/vocab#contains": [
      {
        "@id": "http://example.org/test#book",
        "http://example.org/vocab#contains": [ { "@id": "http://example.org/test#chapter" } ],
        "http://purl.org/dc/elements/1.1/contributor": [ {"@value": "Writer"} ],
        "http://purl.org/dc/elements/1.1/title": [ {"@value": "My Book"} ]
      }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// do not remove @graph at top-level if not only property
//
// @graph used at the top level is retained if there are other properties
#[tokio::test]
async fn t0021() {
    let input = r###"{
  "@context": {
    "authored": {
      "@id": "http://example.org/vocab#authored",
      "@type": "@id"
    },
    "contains": {
      "@id": "http://example.org/vocab#contains",
      "@type": "@id"
    },
    "contributor": "http://purl.org/dc/elements/1.1/contributor",
    "description": "http://purl.org/dc/elements/1.1/description",
    "name": "http://xmlns.com/foaf/0.1/name",
    "title": {
      "@id": "http://purl.org/dc/elements/1.1/title"
    }
  },
  "title": "My first graph",
  "@graph": [
    {
      "@id": "http://example.org/test#jane",
      "name": "Jane",
      "authored": {
        "@graph": [
          {
            "@id": "http://example.org/test#chapter1",
            "description": "Fun",
            "title": "Chapter One"
          },
          {
            "@id": "http://example.org/test#chapter2",
            "description": "More fun",
            "title": "Chapter Two"
          },
          {
            "@id": "http://example.org/test#chapter3",
            "title": "Chapter Three"
          }
        ]
      }
    },
    {
      "@id": "http://example.org/test#john",
      "name": "John"
    },
    {
      "@id": "http://example.org/test#library",
      "contains": {
        "@id": "http://example.org/test#book",
        "contains": "http://example.org/test#chapter",
        "contributor": "Writer",
        "title": "My Book"
      }
    }
  ]
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0021-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "http://purl.org/dc/elements/1.1/title": [ {"@value": "My first graph"} ],
    "@graph": [
      {
        "@id": "http://example.org/test#jane",
        "http://xmlns.com/foaf/0.1/name": [ {"@value": "Jane"} ],
        "http://example.org/vocab#authored": [
          {
            "@graph": [
              {
                "@id": "http://example.org/test#chapter1",
                "http://purl.org/dc/elements/1.1/description": [ {"@value": "Fun"} ],
                "http://purl.org/dc/elements/1.1/title": [ {"@value": "Chapter One"} ]
              },
              {
                "@id": "http://example.org/test#chapter2",
                "http://purl.org/dc/elements/1.1/description": [ {"@value": "More fun"} ],
                "http://purl.org/dc/elements/1.1/title": [ {"@value": "Chapter Two"} ]
              },
              {
                "@id": "http://example.org/test#chapter3",
                "http://purl.org/dc/elements/1.1/title": [ {"@value": "Chapter Three"} ]
              }
            ]
          }
        ]
      },
      {
        "@id": "http://example.org/test#john",
        "http://xmlns.com/foaf/0.1/name": [ {"@value": "John"} ]
      },
      {
        "@id": "http://example.org/test#library",
        "http://example.org/vocab#contains": [
          {
            "@id": "http://example.org/test#book",
            "http://example.org/vocab#contains": [ { "@id": "http://example.org/test#chapter" } ],
            "http://purl.org/dc/elements/1.1/contributor": [ {"@value": "Writer"} ],
            "http://purl.org/dc/elements/1.1/title": [ {"@value": "My Book"} ]
          }
        ]
      }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// expand value with default language
//
// Expanding with a default language applies that language to string values
#[tokio::test]
async fn t0022() {
    let input = r###"{
  "@context": {
    "term": "http://example.com/term",
    "@language": "en"
  },
  "term": "v"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0022-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.com/term": [{"@value": "v", "@language": "en"}]
}]"###).expect("Invalid output JSON"));}

// Expanding list/set with coercion
//
// Expanding lists and sets with properties having coercion coerces list/set values
#[tokio::test]
async fn t0023() {
    let input = r###"{
  "@context": {
    "xsd": "http://www.w3.org/2001/XMLSchema#",
    "idlist": {"@id": "http://example.com/idlist", "@container": "@list", "@type": "@id"},
    "datelist": {"@id": "http://example.com/datelist", "@container": "@list", "@type": "xsd:date"},
    "idset": {"@id": "http://example.com/idset", "@container": "@set", "@type": "@id"},
    "dateset": {"@id": "http://example.com/dateset", "@container": "@set", "@type": "xsd:date"},
    "idprop": {"@id": "http://example.com/idprop", "@type": "@id" },
    "dateprop": {"@id": "http://example.com/dateprop", "@type": "xsd:date" },
    "idprop2": {"@id": "http://example.com/idprop2", "@type": "@id" },
    "dateprop2": {"@id": "http://example.com/dateprop2", "@type": "xsd:date" }
  },
  "idlist": ["http://example.org/id"],
  "datelist": ["2012-04-12"],
  "idprop": {"@list": ["http://example.org/id"]},
  "dateprop": {"@list": ["2012-04-12"]},
  "idset": ["http://example.org/id"],
  "dateset": ["2012-04-12"],
  "idprop2": {"@set": ["http://example.org/id"]},
  "dateprop2": {"@set": ["2012-04-12"]}
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0023-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "http://example.com/idlist": [{"@list": [{"@id": "http://example.org/id"}]}],
    "http://example.com/datelist": [{"@list": [{"@value": "2012-04-12","@type": "http://www.w3.org/2001/XMLSchema#date"}]}],
    "http://example.com/idprop": [{"@list": [{"@id": "http://example.org/id"}]}],
    "http://example.com/dateprop": [{"@list": [{"@value": "2012-04-12","@type": "http://www.w3.org/2001/XMLSchema#date"}]}],
    "http://example.com/idset": [{"@id": "http://example.org/id"}],
    "http://example.com/dateset": [{"@value": "2012-04-12","@type": "http://www.w3.org/2001/XMLSchema#date"}],
    "http://example.com/idprop2": [{"@id": "http://example.org/id"}],
    "http://example.com/dateprop2": [{"@value": "2012-04-12","@type": "http://www.w3.org/2001/XMLSchema#date"}]
  }
]"###).expect("Invalid output JSON"));}

// Multiple contexts
//
// Tests that contexts in an array are merged
#[tokio::test]
async fn t0024() {
    let input = r###"{
  "@context": [
    {
      "name": "http://xmlns.com/foaf/0.1/name",
      "homepage": {"@id": "http://xmlns.com/foaf/0.1/homepage","@type": "@id"}
    },
    {"ical": "http://www.w3.org/2002/12/cal/ical#"}
  ],
  "@id": "http://example.com/speakers#Alice",
  "name": "Alice",
  "homepage": "http://xkcd.com/177/",
  "ical:summary": "Alice Talk",
  "ical:location": "Lyon Convention Centre, Lyon, France"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0024-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@id": "http://example.com/speakers#Alice",
  "http://xmlns.com/foaf/0.1/name": [{"@value": "Alice"}],
  "http://xmlns.com/foaf/0.1/homepage": [{"@id": "http://xkcd.com/177/"}],
  "http://www.w3.org/2002/12/cal/ical#summary": [{"@value": "Alice Talk"}],
  "http://www.w3.org/2002/12/cal/ical#location": [{"@value": "Lyon Convention Centre, Lyon, France"}]
}]"###).expect("Invalid output JSON"));}

// Problematic IRI expansion tests
//
// Expanding different kinds of terms and Compact IRIs
#[tokio::test]
async fn t0025() {
    let input = r###"{
  "@context": {
    "foo": "http://example.com/foo/",
    "foo:bar": "http://example.com/foo/bar",
    "bar": {"@id": "foo:bar", "@type": "@id"},
    "_": "http://example.com/underscore/"
  },
  "@type": [ "foo", "foo:bar", "_" ]
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0025-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@type": [
    "http://example.com/foo/",
    "http://example.com/foo/bar",
    "http://example.com/underscore/"
  ]
}]
"###).expect("Invalid output JSON"));}

// Term definition with @id: @type
//
// Expanding term mapping to @type uses @type syntax
#[tokio::test]
async fn t0026() {
    let input = r###"{
  "@context": {
    "http://www.w3.org/1999/02/22-rdf-syntax-ns#type": {"@id": "@type", "@type": "@id"}
  },
  "@graph": [
    {
      "@id": "http://example.com/a",
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type": "http://example.com/b"
    }, {
      "@id": "http://example.com/c",
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type": [
        "http://example.com/d",
        "http://example.com/e"
      ]
    }, {
      "@id": "http://example.com/f",
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type": "http://example.com/g"
    }
  ]
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0026-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.com/a",
    "@type": [
      "http://example.com/b"
    ]
  },
  {
    "@id": "http://example.com/c",
    "@type": [
      "http://example.com/d",
      "http://example.com/e"
    ]
  },
  {
    "@id": "http://example.com/f",
    "@type": [
      "http://example.com/g"
    ]
  }
]"###).expect("Invalid output JSON"));}

// Duplicate values in @list and @set
//
// Duplicate values in @list and @set are not merged
#[tokio::test]
async fn t0027() {
    let input = r###"{
  "@context": {
    "mylist": {"@id": "http://example.com/mylist", "@container": "@list"},
    "myset": {"@id": "http://example.com/myset", "@container": "@set"}
  },
  "@id": "http://example.org/id",
  "mylist": [1, 2, 2, 3],
  "myset": [1, 2, 2, 3]
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0027-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@id": "http://example.org/id",
  "http://example.com/mylist": [{
    "@list": [
      {"@value": 1},
      {"@value": 2},
      {"@value": 2},
      {"@value": 3}
    ]
  }],
  "http://example.com/myset": [
    {"@value": 1},
    {"@value": 2},
    {"@value": 2},
    {"@value": 3}
  ]
}]
"###).expect("Invalid output JSON"));}

// Use @vocab in properties and @type but not in @id
//
// @vocab is used to compact properties and @type, but is not used for @id
#[tokio::test]
async fn t0028() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/vocab#",
    "date": { "@type": "dateTime" }
  },
  "@id": "example1",
  "@type": "test",
  "date": "2011-01-25T00:00:00Z",
  "embed": {
    "@id": "example2",
    "expandedDate": { "@value": "2012-08-01T00:00:00Z", "@type": "dateTime" }
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0028-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "https://w3c.github.io/json-ld-api/tests/expand/example1",
    "@type": [ "http://example.org/vocab#test" ],
    "http://example.org/vocab#date": [
      {
        "@value": "2011-01-25T00:00:00Z",
        "@type": "http://example.org/vocab#dateTime"
      }
    ],
    "http://example.org/vocab#embed": [
      {
        "@id": "https://w3c.github.io/json-ld-api/tests/expand/example2",
        "http://example.org/vocab#expandedDate": [
          {
            "@value": "2012-08-01T00:00:00Z",
            "@type": "http://example.org/vocab#dateTime"
          }
        ]
      }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// Relative IRIs
//
// @base is used to compact @id; test with different relative IRIs
#[tokio::test]
async fn t0029() {
    let input = r###"{
  "@context": {
    "links": { "@id": "http://www.example.com/link", "@type": "@id", "@container": "@list" }
  },
  "@id": "relativeIris",
  "@type": [
    "link",
    "#fragment-works",
    "?query=works",
    "./",
    "../",
    "../parent",
    "../../../parent-parent-eq-root",
    "../../../../../still-root",
    "../.././.././../../too-many-dots",
    "/absolute",
    "//example.org/scheme-relative"
  ],
  "links": [
    "link",
    "#fragment-works",
    "?query=works",
    "./",
    "../",
    "../parent",
    "../../../parent-parent-eq-root",
    "./../../../useless/../../../still-root",
    "../.././.././../../too-many-dots",
    "/absolute",
    "//example.org/scheme-relative"
  ]
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0029-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "https://w3c.github.io/json-ld-api/tests/expand/relativeIris",
    "@type": [
      "https://w3c.github.io/json-ld-api/tests/expand/link",
      "https://w3c.github.io/json-ld-api/tests/expand/0029-in.jsonld#fragment-works",
      "https://w3c.github.io/json-ld-api/tests/expand/0029-in.jsonld?query=works",
      "https://w3c.github.io/json-ld-api/tests/expand/",
      "https://w3c.github.io/json-ld-api/tests/",
      "https://w3c.github.io/json-ld-api/tests/parent",
      "https://w3c.github.io/parent-parent-eq-root",
      "https://w3c.github.io/still-root",
      "https://w3c.github.io/too-many-dots",
      "https://w3c.github.io/absolute",
      "https://example.org/scheme-relative"
    ],
    "http://www.example.com/link": [
      {
        "@list": [
          {"@id": "https://w3c.github.io/json-ld-api/tests/expand/link"},
          {"@id": "https://w3c.github.io/json-ld-api/tests/expand/0029-in.jsonld#fragment-works"},
          {"@id": "https://w3c.github.io/json-ld-api/tests/expand/0029-in.jsonld?query=works"},
          {"@id": "https://w3c.github.io/json-ld-api/tests/expand/"},
          {"@id": "https://w3c.github.io/json-ld-api/tests/"},
          {"@id": "https://w3c.github.io/json-ld-api/tests/parent"},
          {"@id": "https://w3c.github.io/parent-parent-eq-root"},
          {"@id": "https://w3c.github.io/still-root"},
          {"@id": "https://w3c.github.io/too-many-dots"},
          {"@id": "https://w3c.github.io/absolute"},
          {"@id": "https://example.org/scheme-relative"}
        ]
      }
    ]
  }
]"###).expect("Invalid output JSON"));}

// Language maps
//
// Language Maps expand values to include @language
#[tokio::test]
async fn t0030() {
    let input = r###"{
  "@context": {
    "vocab": "http://example.com/vocab/",
    "label": {
      "@id": "vocab:label",
      "@container": "@language"
    }
  },
  "@id": "http://example.com/queen",
  "label": {
    "en": "The Queen",
    "de": [ "Die Königin", "Ihre Majestät" ]
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0030-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.com/queen",
    "http://example.com/vocab/label":
    [
      {
        "@value": "Die Königin",
        "@language": "de"
      }, {
        "@value": "Ihre Majestät",
        "@language": "de"
      }, {
        "@value": "The Queen",
        "@language": "en"
      }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// type-coercion of native types
//
// Expanding native types with type coercion adds the coerced type to an expanded value representation and retains the native value representation
#[tokio::test]
async fn t0031() {
    let input = r###"{
  "@context": {
    "ex": "http://example.org/vocab#",
    "xsd": "http://www.w3.org/2001/XMLSchema#",
    "ex:integer": { "@type": "xsd:integer" },
    "ex:double": { "@type": "xsd:double" },
    "ex:boolean": { "@type": "xsd:boolean" }
  },
  "@id": "http://example.org/test#example1",
  "ex:integer": 1,
  "ex:double": 123.45,
  "ex:boolean": true
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0031-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.org/test#example1",
    "http://example.org/vocab#integer": [ {
      "@value": 1,
      "@type": "http://www.w3.org/2001/XMLSchema#integer"
    } ],
    "http://example.org/vocab#double": [ {
      "@value": 123.45,
      "@type": "http://www.w3.org/2001/XMLSchema#double"
    } ],
    "http://example.org/vocab#boolean": [ {
      "@value": true,
      "@type": "http://www.w3.org/2001/XMLSchema#boolean"
    } ]
  }
]
"###).expect("Invalid output JSON"));}

// Null term and @vocab
//
// Mapping a term to null decouples it from @vocab
#[tokio::test]
async fn t0032() {
    let input = r###"{
  "@context": {
    "@vocab": "http://xmlns.com/foaf/0.1/",
    "from": null,
    "university": { "@id": null }
  },
  "@id": "http://me.markus-lanthaler.com/",
  "name": "Markus Lanthaler",
  "from": "Italy",
  "university": "TU Graz"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0032-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
   {
      "@id": "http://me.markus-lanthaler.com/",
      "http://xmlns.com/foaf/0.1/name": [
         {
            "@value": "Markus Lanthaler"
         }
      ]
   }
]
"###).expect("Invalid output JSON"));}

// Using @vocab with with type-coercion
//
// Verifies that terms can be defined using @vocab
#[tokio::test]
async fn t0033() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.com/vocab#",
    "homepage": {
      "@type": "@id"
    },
    "created_at": {
      "@type": "http://www.w3.org/2001/XMLSchema#date"
    }
  },
  "name": "Markus Lanthaler",
  "homepage": "http://www.markus-lanthaler.com/",
  "created_at": "2012-10-28"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0033-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
   "http://example.com/vocab#name": [{
      "@value": "Markus Lanthaler"
   }],
   "http://example.com/vocab#homepage": [{
      "@id": "http://www.markus-lanthaler.com/"
   }],
   "http://example.com/vocab#created_at": [{
      "@value": "2012-10-28",
      "@type": "http://www.w3.org/2001/XMLSchema#date"
   }]
}]
"###).expect("Invalid output JSON"));}

// Multiple properties expanding to the same IRI
//
// Verifies multiple values from separate terms are deterministically made multiple values of the IRI associated with the terms
#[tokio::test]
async fn t0034() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.com/vocab/",
    "colliding": "http://example.com/vocab/collidingTerm"
  },
  "@id": "http://example.com/IriCollissions",
  "colliding": [
    "value 1",
    2
  ],
  "collidingTerm": [
    3,
    "four"
  ],
  "http://example.com/vocab/collidingTerm": 5
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0034-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
   "@id": "http://example.com/IriCollissions",
   "http://example.com/vocab/collidingTerm": [
      {
         "@value": "value 1"
      }, {
         "@value": 2
      }, {
         "@value": 3
      }, {
         "@value": "four"
      },
      {
         "@value": 5
      }
   ]
}]
"###).expect("Invalid output JSON"));}

// Language maps with @vocab, default language, and colliding property
//
// Pathological tests of language maps
#[tokio::test]
async fn t0035() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.com/vocab/",
    "@language": "it",
    "label": {
      "@container": "@language"
    }
  },
  "@id": "http://example.com/queen",
  "label": {
    "en": "The Queen",
    "de": [ "Die Königin", "Ihre Majestät" ]
  },
  "http://example.com/vocab/label": [
    "Il re",
    { "@value": "The king", "@language": "en" }
  ]
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0035-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
   "@id": "http://example.com/queen",
   "http://example.com/vocab/label": [
      {
         "@value": "Il re",
         "@language": "it"
      }, {
         "@value": "The king",
         "@language": "en"
      }, {
         "@value": "Die Königin",
         "@language": "de"
      }, {
         "@value": "Ihre Majestät",
         "@language": "de"
      }, {
         "@value": "The Queen",
         "@language": "en"
      }
   ]
}]
"###).expect("Invalid output JSON"));}

// Expanding @index
//
// Expanding index maps for terms defined with @container: @index
#[tokio::test]
async fn t0036() {
    let input = r###"{
  "@context": {
    "property": "http://example.com/property",
    "indexContainer": { "@id": "http://example.com/container", "@container": "@index" }
  },
  "@id": "http://example.org/indexTest",
  "indexContainer": {
    "A": [
      {
        "@id": "http://example.org/nodeWithoutIndexA"
      },
      {
        "@id": "http://example.org/nodeWithIndexA",
        "@index": "this overrides the 'A' index from the container"
      },
      1,
      true,
      false,
      null,
      "simple string A",
      {
        "@value": "typed literal A",
        "@type": "http://example.org/type"
      },
      {
        "@value": "language-tagged string A",
        "@language": "en"
      }
    ],
    "B": "simple string B",
    "C": [
      {
        "@id": "http://example.org/nodeWithoutIndexC"
      },
      {
        "@id": "http://example.org/nodeWithIndexC",
        "@index": "this overrides the 'C' index from the container"
      },
      3,
      true,
      false,
      null,
      "simple string C",
      {
        "@value": "typed literal C",
        "@type": "http://example.org/type"
      },
      {
        "@value": "language-tagged string C",
        "@language": "en"
      }
    ]
  },
  "property": [
    {
      "@id": "http://example.org/nodeWithoutIndexProp"
    },
    {
      "@id": "http://example.org/nodeWithIndexProp",
      "@index": "prop"
    },
    {
      "@value": 3,
      "@index": "prop"
    },
    {
      "@value": true,
      "@index": "prop"
    },
    {
      "@value": false,
      "@index": "prop"
    },
    {
      "@value": null,
      "@index": "prop"
    },
    "simple string no index",
    {
      "@value": "typed literal Prop",
      "@type": "http://example.org/type",
      "@index": "prop"
    },
    {
      "@value": "language-tagged string Prop",
      "@language": "en",
      "@index": "prop"
    }
  ]
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0036-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
   {
      "@id": "http://example.org/indexTest",
      "http://example.com/container": [
         {
            "@id": "http://example.org/nodeWithoutIndexA",
            "@index": "A"
         },
         {
            "@id": "http://example.org/nodeWithIndexA",
            "@index": "this overrides the 'A' index from the container"
         },
         {
            "@value": 1,
            "@index": "A"
         },
         {
            "@value": true,
            "@index": "A"
         },
         {
            "@value": false,
            "@index": "A"
         },
         {
            "@value": "simple string A",
            "@index": "A"
         },
         {
            "@value": "typed literal A",
            "@type": "http://example.org/type",
            "@index": "A"
         },
         {
            "@value": "language-tagged string A",
            "@language": "en",
            "@index": "A"
         },
         {
            "@value": "simple string B",
            "@index": "B"
         },
         {
            "@id": "http://example.org/nodeWithoutIndexC",
            "@index": "C"
         },
         {
            "@id": "http://example.org/nodeWithIndexC",
            "@index": "this overrides the 'C' index from the container"
         },
         {
            "@value": 3,
            "@index": "C"
         },
         {
            "@value": true,
            "@index": "C"
         },
         {
            "@value": false,
            "@index": "C"
         },
         {
            "@value": "simple string C",
            "@index": "C"
         },
         {
            "@value": "typed literal C",
            "@type": "http://example.org/type",
            "@index": "C"
         },
         {
            "@value": "language-tagged string C",
            "@language": "en",
            "@index": "C"
         }
      ],
      "http://example.com/property": [
         {
            "@id": "http://example.org/nodeWithoutIndexProp"
         },
         {
            "@id": "http://example.org/nodeWithIndexProp",
            "@index": "prop"
         },
         {
            "@value": 3,
            "@index": "prop"
         },
         {
            "@value": true,
            "@index": "prop"
         },
         {
            "@value": false,
            "@index": "prop"
         },
         {
            "@value": "simple string no index"
         },
         {
            "@value": "typed literal Prop",
            "@type": "http://example.org/type",
            "@index": "prop"
         },
         {
            "@value": "language-tagged string Prop",
            "@language": "en",
            "@index": "prop"
         }
      ]
   }
]
"###).expect("Invalid output JSON"));}

// Expanding @reverse
//
// Expanding @reverse keeps @reverse
#[tokio::test]
async fn t0037() {
    let input = r###"{
  "@context": {
    "name": "http://xmlns.com/foaf/0.1/name"
  },
  "@id": "http://example.com/people/markus",
  "name": "Markus Lanthaler",
  "@reverse": {
    "http://xmlns.com/foaf/0.1/knows": {
      "@id": "http://example.com/people/dave",
      "name": "Dave Longley"
    }
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0037-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.com/people/markus",
    "@reverse": {
      "http://xmlns.com/foaf/0.1/knows": [
        {
          "@id": "http://example.com/people/dave",
          "http://xmlns.com/foaf/0.1/name": [ { "@value": "Dave Longley" } ]
        }
      ]
    },
    "http://xmlns.com/foaf/0.1/name": [ { "@value": "Markus Lanthaler" } ]
  }
]
"###).expect("Invalid output JSON"));}

// Expanding blank node labels
//
// Blank nodes are not relabeled during expansion
#[tokio::test]
async fn t0038() {
    let input = r###"{
  "@context": {
    "term": "_:term",
    "termId": { "@id": "term", "@type": "@id" }
  },
  "@id": "_:term",
  "@type": "_:term",
  "term": [
    {
      "@id": "_:term",
      "@type": "term"
    },
    {
      "@id": "_:Bx",
      "term": "term"
    },
    "plain value",
    {
      "@id": "_:term"
    }
  ],
  "termId": [
    {
      "@id": "_:term",
      "@type": "term"
    },
    {
      "@id": "_:Cx",
      "term": "termId"
    },
    "term:AppendedToBlankNode",
    "_:termAppendedToBlankNode",
    "relativeIri",
    {
      "@id": "_:term"
    }
  ]
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0038-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "_:term",
    "@type": [
      "_:term"
    ],
    "_:term": [
      {
        "@id": "_:term",
        "@type": [
          "_:term"
        ]
      },
      {
        "@id": "_:Bx",
        "_:term": [
          {
            "@value": "term"
          }
        ]
      },
      {
        "@value": "plain value"
      },
      {
        "@id": "_:term"
      },
      {
        "@id": "_:term",
        "@type": [
          "_:term"
        ]
      },
      {
        "@id": "_:Cx",
        "_:term": [
          {
            "@value": "termId"
          }
        ]
      },
      {
        "@id": "_:termAppendedToBlankNode"
      },
      {
        "@id": "_:termAppendedToBlankNode"
      },
      {
        "@id": "https://w3c.github.io/json-ld-api/tests/expand/relativeIri"
      },
      {
        "@id": "_:term"
      }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// Using terms in a reverse-maps
//
// Terms within @reverse are expanded
#[tokio::test]
async fn t0039() {
    let input = r###"{
  "@context": {
    "name": "http://xmlns.com/foaf/0.1/name",
    "knows": "http://xmlns.com/foaf/0.1/knows"
  },
  "@id": "http://example.com/people/markus",
  "name": "Markus Lanthaler",
  "@reverse": {
    "knows": {
      "@id": "http://example.com/people/dave",
      "name": "Dave Longley"
    }
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0039-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.com/people/markus",
    "@reverse": {
      "http://xmlns.com/foaf/0.1/knows": [
        {
          "@id": "http://example.com/people/dave",
          "http://xmlns.com/foaf/0.1/name": [ { "@value": "Dave Longley" } ]
        }
      ]
    },
    "http://xmlns.com/foaf/0.1/name": [ { "@value": "Markus Lanthaler" } ]
  }
]
"###).expect("Invalid output JSON"));}

// language and index expansion on non-objects
//
// Only invoke language and index map expansion if the value is a JSON object
#[tokio::test]
async fn t0040() {
    let input = r###"{
  "@context": {
    "vocab": "http://example.com/vocab/",
    "label": {
      "@id": "vocab:label",
      "@container": "@language"
    },
    "indexes": {
      "@id": "vocab:index",
      "@container": "@index"
    }
  },
  "@id": "http://example.com/queen",
  "label": [
    "The Queen"
  ],
  "indexes":
  [
    "No",
    "indexes",
    { "@id": "asTheValueIsntAnObject" }
  ]
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0040-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.com/queen",
    "http://example.com/vocab/label":
    [
      {
        "@value": "The Queen"
      }
    ],
    "http://example.com/vocab/index":
    [
      {
        "@value": "No"
      },
      {
        "@value": "indexes"
      },
      {
        "@id": "https://w3c.github.io/json-ld-api/tests/expand/asTheValueIsntAnObject"
      }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// @language: null resets the default language
#[tokio::test]
async fn t0041() {
    let input = r###"{
  "@context": {
    "property": "http://example.com/property",
    "nested": "http://example.com/nested",
    "@language": "en"
  },
  "property": "this is English",
  "nested": {
    "@context": {
      "@language": null
    },
    "property": "and this is a plain string"
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0041-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "http://example.com/property": [ { "@value": "this is English", "@language": "en" } ],
    "http://example.com/nested": [
      {
        "http://example.com/property": [ { "@value": "and this is a plain string" } ]
      }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// Reverse properties
//
// Expanding terms defined as reverse properties uses @reverse in expanded document
#[tokio::test]
async fn t0042() {
    let input = r###"{
  "@context": {
    "name": "http://xmlns.com/foaf/0.1/name",
    "isKnownBy": { "@reverse": "http://xmlns.com/foaf/0.1/knows" }
  },
  "@id": "http://example.com/people/markus",
  "name": "Markus Lanthaler",
  "isKnownBy": {
    "@id": "http://example.com/people/dave",
    "name": "Dave Longley"
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0042-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.com/people/markus",
    "@reverse": {
      "http://xmlns.com/foaf/0.1/knows": [
        {
          "@id": "http://example.com/people/dave",
          "http://xmlns.com/foaf/0.1/name": [ { "@value": "Dave Longley" } ]
        }
      ]
    },
    "http://xmlns.com/foaf/0.1/name": [ { "@value": "Markus Lanthaler" } ]
  }
]
"###).expect("Invalid output JSON"));}

// Using reverse properties inside a @reverse-container
//
// Expanding a reverse property within a @reverse undoes both reversals
#[tokio::test]
async fn t0043() {
    let input = r###"{
  "@context": {
    "name": "http://xmlns.com/foaf/0.1/name",
    "isKnownBy": { "@reverse": "http://xmlns.com/foaf/0.1/knows" }
  },
  "@id": "http://example.com/people/markus",
  "name": "Markus Lanthaler",
  "@reverse": {
    "isKnownBy": [
      {
        "@id": "http://example.com/people/dave",
        "name": "Dave Longley"
      },
      {
        "@id": "http://example.com/people/gregg",
        "name": "Gregg Kellogg"
      }
    ]
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0043-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.com/people/markus",
    "http://xmlns.com/foaf/0.1/knows": [
      {
        "@id": "http://example.com/people/dave",
        "http://xmlns.com/foaf/0.1/name": [ { "@value": "Dave Longley" } ]
      },
      {
        "@id": "http://example.com/people/gregg",
        "http://xmlns.com/foaf/0.1/name": [ { "@value": "Gregg Kellogg" } ]
      }
    ],
    "http://xmlns.com/foaf/0.1/name": [ { "@value": "Markus Lanthaler" } ]
  }
]
"###).expect("Invalid output JSON"));}

// Index maps with language mappings
//
// Ensure index maps use language mapping
#[tokio::test]
async fn t0044() {
    let input = r###"{
  "@context": {
    "property": { "@id": "http://example.com/vocab/property", "@language": "de" },
    "indexMap": { "@id": "http://example.com/vocab/indexMap", "@language": "en", "@container": "@index" }
  },
  "@id": "http://example.com/node",
  "property": [
    {
      "@id": "http://example.com/propertyValueNode",
      "indexMap": {
        "expands to english string": "simple string"
      }
    },
    "einfacher String"
  ]
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0044-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.com/node",
    "http://example.com/vocab/property": [
      {
        "@id": "http://example.com/propertyValueNode",
        "http://example.com/vocab/indexMap": [
          {
            "@value": "simple string",
            "@language": "en",
            "@index": "expands to english string"
          }
        ]
      },
      {
        "@value": "einfacher String",
        "@language": "de"
      }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// Top-level value objects
//
// Expanding top-level value objects causes them to be removed
#[tokio::test]
async fn t0045() {
    let input = r###"{
    "@value": "free-floating value"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0045-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[ ]
"###).expect("Invalid output JSON"));}

// Free-floating nodes
//
// Expanding free-floating nodes causes them to be removed
#[tokio::test]
async fn t0046() {
    let input = r###"{
    "@graph": [
        { "@id": "http://example.com/free-floating-node" },
        { "@value": "free-floating value object" },
        { "@value": "free-floating value language-tagged string",  "@language": "en" },
        { "@value": "free-floating value typed value",  "@type": "http://example.com/type" },
        "free-floating plain string",
        true,
        false,
        null,
        1,
        1.5
    ]
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0046-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[ ]
"###).expect("Invalid output JSON"));}

// Free-floating values in sets and free-floating lists
//
// Free-floating values in sets are removed, free-floating lists are removed completely
#[tokio::test]
async fn t0047() {
    let input = r###"{
    "@context": {
        "property": "http://example.com/property"
    },
    "@graph": [
        {
            "@set": [
                "free-floating strings in set objects are removed",
                {
                    "@id": "http://example.com/free-floating-node"
                },
                {
                    "@id": "http://example.com/node",
                    "property": "nodes with properties are not removed"
                }
            ]
        },
        {
            "@list": [
                "lists are removed even though they represent an invisible linked structure, they have no real meaning",
                {
                    "@id": "http://example.com/node-in-free-floating-list",
                    "property": "everything inside a free-floating list is removed with the list; also nodes with properties"
                }
            ]
        }
    ]
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0047-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
    {
        "@id": "http://example.com/node",
        "http://example.com/property": [
            {
                "@value": "nodes with properties are not removed"
            }
        ]
    }
]
"###).expect("Invalid output JSON"));}

// Terms are ignored in @id
//
// Values of @id are not expanded as terms
#[tokio::test]
async fn t0048() {
    let input = r###"{
  "@context": {
    "term": "http://example.com/terms-are-not-considered-in-id",
    "compact-iris": "http://example.com/compact-iris#",
    "property": "http://example.com/property",
    "@vocab": "http://example.org/vocab-is-not-considered-for-id"
  },
  "@id": "term",
  "property": [
    {
      "@id": "compact-iris:are-considered",
      "property": "@id supports the following values: relative, absolute, and compact IRIs"
    },
    {
      "@id": "../parent-node",
      "property": "relative IRIs get resolved against the document's base IRI"
    }
  ]
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0048-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "https://w3c.github.io/json-ld-api/tests/expand/term",
    "http://example.com/property": [
      {
        "@id": "http://example.com/compact-iris#are-considered",
        "http://example.com/property": [
          { "@value": "@id supports the following values: relative, absolute, and compact IRIs" }
        ]
      },
      {
        "@id": "https://w3c.github.io/json-ld-api/tests/parent-node",
        "http://example.com/property": [
          { "@value": "relative IRIs get resolved against the document's base IRI" }
        ]
      }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// String values of reverse properties
//
// String values of a reverse property with @type: @id are treated as IRIs
#[tokio::test]
async fn t0049() {
    let input = r###"{
  "@context": {
    "name": "http://xmlns.com/foaf/0.1/name",
    "isKnownBy": { "@reverse": "http://xmlns.com/foaf/0.1/knows", "@type": "@id" }
  },
  "@id": "http://example.com/people/markus",
  "name": "Markus Lanthaler",
  "isKnownBy": [
    "http://example.com/people/dave",
    "http://example.com/people/gregg"
  ]
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0049-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.com/people/markus",
    "@reverse": {
      "http://xmlns.com/foaf/0.1/knows": [
        {
          "@id": "http://example.com/people/dave"
        },
        {
          "@id": "http://example.com/people/gregg"
        }
      ]
    },
    "http://xmlns.com/foaf/0.1/name": [ { "@value": "Markus Lanthaler" } ]
  }
]
"###).expect("Invalid output JSON"));}

// Term definitions with prefix separate from prefix definitions
//
// Term definitions using compact IRIs don't inherit the definitions of the prefix
#[tokio::test]
async fn t0050() {
    let input = r###"{
  "@context": {
    "issue": { "@id": "http://example.com/issue/", "@type": "@id" },
    "issue:raisedBy": { "@container": "@set" }
  },
  "issue": "/issue/1",
  "issue:raisedBy": "Markus"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0050-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "http://example.com/issue/": [ { "@id": "https://w3c.github.io/issue/1" } ],
    "http://example.com/issue/raisedBy": [ { "@value": "Markus" } ]
  }
]
"###).expect("Invalid output JSON"));}

// Expansion of keyword aliases in term definitions
//
// Expanding terms which are keyword aliases
#[tokio::test]
async fn t0051() {
    let input = r###"{
  "@context": [
    { "id": "@id" },
    { "url": "id" }
  ],
  "url": "/issue/1",
  "http://example.com/property": "ok"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0051-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
   "http://example.com/property": [{
      "@value": "ok"
   }],
   "@id": "https://w3c.github.io/issue/1"
}]
"###).expect("Invalid output JSON"));}

// @vocab-relative IRIs in term definitions
//
// If @vocab is defined, term definitions are expanded relative to @vocab
#[tokio::test]
async fn t0052() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "property": "vocabRelativeProperty"
  },
  "property": "must expand to http://example.org/vocabRelativeProperty",
  "http://example.org/property": "ok"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0052-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "http://example.org/property": [ { "@value": "ok" } ],
    "http://example.org/vocabRelativeProperty": [ { "@value": "must expand to http://example.org/vocabRelativeProperty" } ]
  }
]
"###).expect("Invalid output JSON"));}

// Expand absolute IRI with @type: @vocab
//
// Expanding values of properties of @type: @vocab does not further expand absolute IRIs
#[tokio::test]
async fn t0053() {
    let input = r###"{
  "@context": {
    "term": {"@id": "http://example.org/term", "@type": "@vocab"}
  },
  "term": "http://example.org/enum"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0053-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/term": [{"@id": "http://example.org/enum"}]
}]
"###).expect("Invalid output JSON"));}

// Expand term with @type: @vocab
//
// Expanding values of properties of @type: @vocab does not expand term values
#[tokio::test]
async fn t0054() {
    let input = r###"{
  "@context": {
    "term": {"@id": "http://example.org/term", "@type": "@vocab"},
    "enum": {"@id": "http://example.org/enum"}
  },
  "term": "enum"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0054-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/term": [{"@id": "http://example.org/enum"}]
}]
"###).expect("Invalid output JSON"));}

// Expand @vocab-relative term with @type: @vocab
//
// Expanding values of properties of @type: @vocab expands relative IRIs using @vocab
#[tokio::test]
async fn t0055() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "term": {"@id": "http://example.org/term", "@type": "@vocab"}
  },
  "term": "enum"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0055-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/term": [{"@id": "http://example.org/enum"}]
}]
"###).expect("Invalid output JSON"));}

// Use terms with @type: @vocab but not with @type: @id
//
// Checks that expansion uses appropriate base depending on term definition having @type @id or @vocab
#[tokio::test]
async fn t0056() {
    let input = r###"{
  "@context": {
    "name": "http://xmlns.com/foaf/0.1/name",
    "homepage": { "@id": "http://xmlns.com/foaf/0.1/homepage", "@type": "@vocab" },
    "link": { "@id": "http://example.com/link", "@type": "@id" },
    "MarkusHomepage": "http://www.markus-lanthaler.com/",
    "relative-iri": "http://example.com/error-if-this-is-used-for-link"
  },
  "@id": "http://me.markus-lanthaler.com/",
  "name": "Markus Lanthaler",
  "homepage": "MarkusHomepage",
  "link": "relative-iri"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0056-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://me.markus-lanthaler.com/",
    "http://xmlns.com/foaf/0.1/homepage": [ { "@id": "http://www.markus-lanthaler.com/" } ],
    "http://example.com/link": [ {  "@id": "https://w3c.github.io/json-ld-api/tests/expand/relative-iri" } ],
    "http://xmlns.com/foaf/0.1/name": [ { "@value": "Markus Lanthaler" } ]
  }
]
"###).expect("Invalid output JSON"));}

// Expand relative IRI with @type: @vocab
//
// Relative values of terms with @type: @vocab expand relative to @vocab
#[tokio::test]
async fn t0057() {
    let input = r###"{
  "@context": {
    "term": { "@id": "http://example.org/term", "@type": "@vocab" }
  },
  "term": "not-a-term-thus-a-relative-IRI"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0057-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "http://example.org/term": [ { "@id": "https://w3c.github.io/json-ld-api/tests/expand/not-a-term-thus-a-relative-IRI" } ]
  }
]
"###).expect("Invalid output JSON"));}

// Expand compact IRI with @type: @vocab
//
// Compact IRIs are expanded normally even if term has @type: @vocab
#[tokio::test]
async fn t0058() {
    let input = r###"{
  "@context": {
    "term": { "@id": "http://example.org/term", "@type": "@vocab" },
    "prefix": "http://example.com/vocab#"
  },
  "term": "prefix:suffix"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0058-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "http://example.org/term": [ { "@id": "http://example.com/vocab#suffix" } ]
  }
]
"###).expect("Invalid output JSON"));}

// Reset @vocab by setting it to null
//
// Setting @vocab to null removes a previous definition
#[tokio::test]
async fn t0059() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/vocab#"
  },
  "@id": "example-with-vocab",
  "@type": "vocab-prefixed",
  "property": "property expanded using @vocab",
  "embed": {
    "@context": {
      "@vocab": null
    },
    "@id": "example-vocab-reset",
    "@type": "document-relative",
    "property": "@vocab reset, property will be dropped"
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0059-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "https://w3c.github.io/json-ld-api/tests/expand/example-with-vocab",
    "@type": [ "http://example.org/vocab#vocab-prefixed" ],
    "http://example.org/vocab#embed": [
      {
        "@id": "https://w3c.github.io/json-ld-api/tests/expand/example-vocab-reset",
        "@type": [ "https://w3c.github.io/json-ld-api/tests/expand/document-relative" ]
      }
    ],
    "http://example.org/vocab#property": [ { "@value": "property expanded using @vocab" } ]
  }
]
"###).expect("Invalid output JSON"));}

// Overwrite document base with @base and reset it again
//
// Setting @base to an IRI and then resetting it to nil
#[tokio::test]
async fn t0060() {
    let input = r###"{
  "@context": {
    "property": "http://example.com/vocab#property"
  },
  "@id": "../document-relative",
  "@type": "#document-relative",
  "property": {
    "@context": {
      "@base": "http://example.org/test/"
    },
    "@id": "../document-base-overwritten",
    "@type": "#document-base-overwritten",
    "property": [
      {
        "@context": null,
        "@id": "../document-relative",
        "@type": "#document-relative",
        "property": "context completely reset, drops property"
      },
      {
        "@context": {
          "@base": null
        },
        "@id": "../document-relative",
        "@type": "#document-relative",
        "property": "only @base is cleared"
      }
    ]
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0060-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "https://w3c.github.io/json-ld-api/tests/document-relative",
    "@type": [ "https://w3c.github.io/json-ld-api/tests/expand/0060-in.jsonld#document-relative" ],
    "http://example.com/vocab#property": [
      {
        "@id": "http://example.org/document-base-overwritten",
        "@type": [ "http://example.org/test/#document-base-overwritten" ],
        "http://example.com/vocab#property": [
          {
            "@id": "https://w3c.github.io/json-ld-api/tests/document-relative",
            "@type": [ "https://w3c.github.io/json-ld-api/tests/expand/0060-in.jsonld#document-relative" ]
          },
          {
            "@id": "../document-relative",
            "@type": [ "#document-relative" ],
            "http://example.com/vocab#property": [ { "@value": "only @base is cleared" } ]
          }
        ]
      }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// Coercing native types to arbitrary datatypes
//
// Expanding native types when coercing to arbitrary datatypes
#[tokio::test]
async fn t0061() {
    let input = r###"{
  "@context": {
    "property": {
      "@id": "http://example.com/property",
      "@type": "http://example.com/datatype"
    }
  },
  "property": [ 1, true, false, 5.1 ]
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0061-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "http://example.com/property": [
      { "@value": 1, "@type": "http://example.com/datatype" },
      { "@value": true, "@type": "http://example.com/datatype" },
      { "@value": false, "@type": "http://example.com/datatype" },
      { "@value": 5.1, "@type": "http://example.com/datatype" }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// Various relative IRIs with with @base
//
// Pathological relative IRIs
#[tokio::test]
async fn t0062() {
    let input = r###"{
  "@context": {
    "@base": "http://example.com/some/deep/directory/and/file#with-a-fragment",
    "links": { "@id": "http://www.example.com/link", "@type": "@id", "@container": "@list" }
  },
  "@id": "relativeIris",
  "@type": [
    "link",
    "#fragment-works",
    "?query=works",
    "./",
    "../",
    "../parent",
    "../../parent-parent-eq-root",
    "../../../../../still-root",
    "../.././.././../../too-many-dots",
    "/absolute",
    "//example.org/scheme-relative"
  ],
  "links": [
    "link",
    "#fragment-works",
    "?query=works",
    "./",
    "../",
    "../parent",
    "../../parent-parent-eq-root",
    "./../../../../../still-root",
    "../.././.././../../too-many-dots",
    "/absolute",
    "//example.org/scheme-relative",
    "//example.org/../scheme-relative",
    "//example.org/.././useless/../../scheme-relative"
  ]
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0062-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.com/some/deep/directory/and/relativeIris",
    "@type": [
        "http://example.com/some/deep/directory/and/link",
        "http://example.com/some/deep/directory/and/file#fragment-works",
        "http://example.com/some/deep/directory/and/file?query=works",
        "http://example.com/some/deep/directory/and/",
        "http://example.com/some/deep/directory/",
        "http://example.com/some/deep/directory/parent",
        "http://example.com/some/deep/parent-parent-eq-root",
        "http://example.com/still-root",
        "http://example.com/too-many-dots",
        "http://example.com/absolute",
        "http://example.org/scheme-relative"
    ],
    "http://www.example.com/link": [ {
      "@list": [
        { "@id": "http://example.com/some/deep/directory/and/link" },
        { "@id": "http://example.com/some/deep/directory/and/file#fragment-works" },
        { "@id": "http://example.com/some/deep/directory/and/file?query=works" },
        { "@id": "http://example.com/some/deep/directory/and/" },
        { "@id": "http://example.com/some/deep/directory/" },
        { "@id": "http://example.com/some/deep/directory/parent" },
        { "@id": "http://example.com/some/deep/parent-parent-eq-root" },
        { "@id": "http://example.com/still-root" },
        { "@id": "http://example.com/too-many-dots" },
        { "@id": "http://example.com/absolute" },
        { "@id": "http://example.org/scheme-relative" },
        { "@id": "http://example.org/scheme-relative" },
        { "@id": "http://example.org/scheme-relative" }
      ]
    } ]
  }
]
"###).expect("Invalid output JSON"));}

// Reverse property and index container
//
// Expaning reverse properties with an index-container
#[tokio::test]
async fn t0063() {
    let input = r###"{
  "@context": {
    "name": "http://xmlns.com/foaf/0.1/name",
    "isKnownBy": { "@reverse": "http://xmlns.com/foaf/0.1/knows", "@container": "@index" }
  },
  "@id": "http://example.com/people/markus",
  "name": "Markus Lanthaler",
  "isKnownBy": {
    "Dave": {
      "@id": "http://example.com/people/dave",
      "name": "Dave Longley"
    },
    "Gregg": {
      "@id": "http://example.com/people/gregg",
      "name": "Gregg Kellogg"
    }
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0063-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.com/people/markus",
    "@reverse": {
      "http://xmlns.com/foaf/0.1/knows": [
        {
          "@id": "http://example.com/people/dave",
          "@index": "Dave",
          "http://xmlns.com/foaf/0.1/name": [ { "@value": "Dave Longley" } ]
        },
        {
          "@id": "http://example.com/people/gregg",
          "@index": "Gregg",
          "http://xmlns.com/foaf/0.1/name": [ { "@value": "Gregg Kellogg" } ]
        }
      ]
    },
    "http://xmlns.com/foaf/0.1/name": [ { "@value": "Markus Lanthaler" } ]
  }
]
"###).expect("Invalid output JSON"));}

// bnode values of reverse properties
//
// Expand reverse property whose values are unlabeled blank nodes
#[tokio::test]
async fn t0064() {
    let input = r###"{
  "@context": {
    "name": "http://xmlns.com/foaf/0.1/name",
    "isKnownBy": { "@reverse": "http://xmlns.com/foaf/0.1/knows" }
  },
  "@id": "http://example.com/people/markus",
  "name": "Markus Lanthaler",
  "isKnownBy": [
    {
      "name": "Dave Longley"
    },
    {
      "name": "Gregg Kellogg"
    }
  ]
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0064-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.com/people/markus",
    "@reverse": {
      "http://xmlns.com/foaf/0.1/knows": [
        {
          "http://xmlns.com/foaf/0.1/name": [ { "@value": "Dave Longley" } ]
        },
        {
          "http://xmlns.com/foaf/0.1/name": [ { "@value": "Gregg Kellogg" } ]
        }
      ]
    },
    "http://xmlns.com/foaf/0.1/name": [ { "@value": "Markus Lanthaler" } ]
  }
]
"###).expect("Invalid output JSON"));}

// Drop unmapped keys in reverse map
//
// Keys that are not mapped to an IRI in a reverse-map are dropped
#[tokio::test]
async fn t0065() {
    let input = r###"{
  "@context": {
    "name": "http://xmlns.com/foaf/0.1/name",
    "knows": "http://xmlns.com/foaf/0.1/knows"
  },
  "@id": "http://example.com/people/markus",
  "name": "Markus Lanthaler",
  "@reverse": {
    "knows": {
      "@id": "http://example.com/people/dave",
      "name": "Dave Longley"
    },
    "relative-iri": {
      "@id": "relative-node",
      "name": "Keys that are not mapped to an IRI in a reverse-map are dropped"
    }
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0065-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.com/people/markus",
    "@reverse": {
      "http://xmlns.com/foaf/0.1/knows": [
        {
          "@id": "http://example.com/people/dave",
          "http://xmlns.com/foaf/0.1/name": [ { "@value": "Dave Longley" } ]
        }
      ]
    },
    "http://xmlns.com/foaf/0.1/name": [ { "@value": "Markus Lanthaler" } ]
  }
]
"###).expect("Invalid output JSON"));}

// Reverse-map keys with @vocab
//
// Expand uses @vocab to expand keys in reverse-maps
#[tokio::test]
async fn t0066() {
    let input = r###"{
  "@context": {
    "name": "http://xmlns.com/foaf/0.1/name",
    "knows": "http://xmlns.com/foaf/0.1/knows",
    "@vocab": "http://example.com/vocab/"
  },
  "@id": "http://example.com/people/markus",
  "name": "Markus Lanthaler",
  "@reverse": {
    "knows": {
      "@id": "http://example.com/people/dave",
      "name": "Dave Longley"
    },
    "noTerm": {
      "@id": "relative-node",
      "name": "Compact keys using @vocab"
    }
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0066-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.com/people/markus",
    "@reverse": {
      "http://xmlns.com/foaf/0.1/knows": [
        {
          "@id": "http://example.com/people/dave",
          "http://xmlns.com/foaf/0.1/name": [ { "@value": "Dave Longley" } ]
        }
      ],
      "http://example.com/vocab/noTerm": [
        {
          "@id": "https://w3c.github.io/json-ld-api/tests/expand/relative-node",
          "http://xmlns.com/foaf/0.1/name": [ { "@value": "Compact keys using @vocab" } ]
        }
      ]
    },
    "http://xmlns.com/foaf/0.1/name": [ { "@value": "Markus Lanthaler" } ]
  }
]
"###).expect("Invalid output JSON"));}

// prefix://suffix not a compact IRI
//
// prefix:suffix values are not interpreted as compact IRIs if suffix begins with two slashes
#[tokio::test]
async fn t0067() {
    let input = r###"{
  "@context": {
    "http": "http://example.com/this-prefix-would-overwrite-all-http-iris"
  },
  "@id": "http://example.org/node1",
  "@type": "http://example.org/type",
  "http://example.org/property": "all these IRIs remain unchanged because they are interpreted as absolute IRIs"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0067-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.org/node1",
    "@type": ["http://example.org/type"],
    "http://example.org/property": [
      { "@value": "all these IRIs remain unchanged because they are interpreted as absolute IRIs" }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// _:suffix values are not a compact IRI
//
// prefix:suffix values are not interpreted as compact IRIs if prefix is an underscore
#[tokio::test]
async fn t0068() {
    let input = r###"{
  "@context": {
    "_": "http://example.com/this-prefix-would-overwrite-all-blank-node-identifiers"
  },
  "@id": "_:node1",
  "@type": "_:type",
  "_:property": "all these IRIs remain unchanged because they are interpreted as blank node identifiers"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0068-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "_:node1",
    "@type": [ "_:type" ],
    "_:property": [
      { "@value": "all these IRIs remain unchanged because they are interpreted as blank node identifiers" }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// Compact IRI as term with type mapping
//
// Redefine compact IRI to define type mapping using the compact IRI itself as value of @id
#[tokio::test]
async fn t0069() {
    let input = r###"{
  "@context": {
    "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
    "rdfs:subClassOf": { "@id": "rdfs:subClassOf", "@type": "@id" }
  },
  "@id": "http://example.com/vocab#class",
  "@type": "rdfs:Class",
  "rdfs:subClassOf": "http://example.com/vocab#someOtherClass"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0069-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.com/vocab#class",
    "@type": [ "http://www.w3.org/2000/01/rdf-schema#Class" ],
    "http://www.w3.org/2000/01/rdf-schema#subClassOf": [
      { "@id": "http://example.com/vocab#someOtherClass"}
    ]
  }
]
"###).expect("Invalid output JSON"));}

// Compact IRI as term defined using equivalent compact IRI
//
// Redefine compact IRI to define type mapping using the compact IRI itself as string value
#[tokio::test]
async fn t0070() {
    let input = r###"{
  "@context": {
    "prefix": "http://www.example.org/vocab#",
    "prefix:foo": "prefix:foo"
  },
  "@id": "http://example.com/vocab#id",
  "@type": "prefix:Class",
  "prefix:foo": "bar"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0070-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.com/vocab#id",
    "@type": [ "http://www.example.org/vocab#Class" ],
    "http://www.example.org/vocab#foo": [
      { "@value": "bar"}
    ]
  }
]
"###).expect("Invalid output JSON"));}

// Redefine terms looking like compact IRIs
//
// Term definitions may look like compact IRIs
#[tokio::test]
async fn t0071() {
    let input = r###"{
  "@context": [
    {
      "v": "http://example.com/vocab#",
      "v:term": "v:somethingElse",
      "v:termId": { "@id": "v:somethingElseId" }
    },
    {
      "v:term": "v:term",
      "v:termId": { "@id": "v:termId" }
    }
  ],
  "v:term": "value of v:term",
  "v:termId": "value of v:termId"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0071-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "http://example.com/vocab#term": [
      { "@value": "value of v:term" }
    ],
    "http://example.com/vocab#termId": [
      { "@value": "value of v:termId" }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// Redefine term using @vocab, not itself
//
// Redefining a term as itself when @vocab is defined uses @vocab, not previous term definition
#[tokio::test]
async fn t0072() {
    let input = r###"{
  "@context": [
    {
      "v": "http://example.com/vocab#",
      "term": "v:somethingElse"
    },
    {
      "@vocab": "http://example.com/anotherVocab#",
      "term": "term"
    }
  ],
  "term": "value of term"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0072-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "http://example.com/anotherVocab#term": [
      { "@value": "value of term" }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// @context not first property
//
// Objects are unordered, so serialized node definition containing @context may have @context at the end of the node definition
#[tokio::test]
async fn t0073() {
    let input = r###"{
  "@id": "ex:node1",
  "owl:sameAs": {
    "@id": "ex:node2",
    "rdfs:label": "Node 2",
    "link": "ex:node3",
    "@context": {
      "rdfs": "http://www.w3.org/2000/01/rdf-schema#"
    }
  },
  "@context": {
    "ex": "http://example.org/",
    "owl": "http://www.w3.org/2002/07/owl#",
    "link": { "@id": "ex:link", "@type": "@id" }
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0073-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.org/node1",
    "http://www.w3.org/2002/07/owl#sameAs": [
      {
        "@id": "http://example.org/node2",
        "http://example.org/link": [
          { "@id": "http://example.org/node3" }
        ],
        "http://www.w3.org/2000/01/rdf-schema#label": [ { "@value": "Node 2" } ]
      }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// @id not first property
//
// Objects are unordered, so serialized node definition containing @id may have @id at the end of the node definition
#[tokio::test]
async fn t0074() {
    let input = r###"{
  "@context": {
    "ex": "http://example.org/",
    "owl": "http://www.w3.org/2002/07/owl#",
    "link": {
      "@id": "ex:link",
      "@type": "@id"
    }
  },
  "owl:sameAs": {
    "@context": {
      "rdfs": "http://www.w3.org/2000/01/rdf-schema#"
    },
    "rdfs:label": "Node 2",
    "link": "ex:node3",
    "@id": "ex:node2"
  },
  "@id": "ex:node1"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0074-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.org/node1",
    "http://www.w3.org/2002/07/owl#sameAs": [
      {
        "@id": "http://example.org/node2",
        "http://example.org/link": [
          { "@id": "http://example.org/node3" }
        ],
        "http://www.w3.org/2000/01/rdf-schema#label": [ { "@value": "Node 2" } ]
      }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// @vocab as blank node identifier
//
// Use @vocab to map all properties to blank node identifiers
#[tokio::test]
async fn t0075() {
    let input = r###"{
  "@context": {
    "@vocab": "_:"
  },
  "@id": "ex:node1",
  "b1": "blank node property 1",
  "b2": "blank node property 1"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0075-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "ex:node1",
    "_:b1": [ { "@value": "blank node property 1" } ],
    "_:b2": [ { "@value": "blank node property 1" } ]
  }
]
"###).expect("Invalid output JSON"));}

// base option overrides document location
//
// Use of the base option overrides the document location
#[tokio::test]
async fn t0076() {
    let input = r###"{
  "@id": "relative-iri",
  "http://prop": "value"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0076-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@id": "http://example/base/relative-iri",
  "http://prop": [{"@value": "value"}]
}]
"###).expect("Invalid output JSON"));}

// expandContext option
//
// Use of the expandContext option to expand the input document
#[tokio::test]
async fn t0077() {
    let input = r###"{
  "@id": "http://example.com/id1",
  "@type": "t1",
  "term1": "v1",
  "term2": {"@value": "v2", "@type": "t2"},
  "term3": {"@value": "v3", "@language": "en"},
  "term4": 4,
  "term5": [50, 51]
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0077-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@id": "http://example.com/id1",
  "@type": ["http://example.com/t1"],
  "http://example.com/term1": [{"@value": "v1"}],
  "http://example.com/term2": [{"@value": "v2", "@type": "http://example.com/t2"}],
  "http://example.com/term3": [{"@value": "v3", "@language": "en"}],
  "http://example.com/term4": [{"@value": 4}],
  "http://example.com/term5": [{"@value": 50}, {"@value": 51}]
}]"###).expect("Invalid output JSON"));}

// multiple reverse properties
//
// Use of multiple reverse properties
#[tokio::test]
async fn t0078() {
    let input = r###"{
  "@context": {
    "name": "http://example.com/vocab#name",
    "children": { "@reverse": "http://example.com/vocab#parent" },
    "pets": { "@reverse": "http://example.com/vocab#owner" }
  },
  "@id": "#homer",
  "name": "Homer",
  "children": [
    {
      "@id": "#bart",
      "name": "Bart"
    },
    {
      "@id": "#lisa",
      "name": "Lisa"
    }
  ],
  "pets": [
    {
      "@id": "#snowball-ii",
      "name": "Snowball II"
    },
    {
      "@id": "#santas-little-helper",
      "name": "Santa's Little Helper"
    }
  ]
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0078-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@id": "https://w3c.github.io/json-ld-api/tests/expand/0078-in.jsonld#homer",
  "@reverse": {
    "http://example.com/vocab#parent": [
      {
        "@id": "https://w3c.github.io/json-ld-api/tests/expand/0078-in.jsonld#bart",
        "http://example.com/vocab#name": [ { "@value": "Bart" } ]
      },
      {
        "@id": "https://w3c.github.io/json-ld-api/tests/expand/0078-in.jsonld#lisa",
        "http://example.com/vocab#name": [ { "@value": "Lisa" } ]
      }
    ],
    "http://example.com/vocab#owner": [
      {
        "@id": "https://w3c.github.io/json-ld-api/tests/expand/0078-in.jsonld#snowball-ii",
        "http://example.com/vocab#name": [ { "@value": "Snowball II" } ]
      },
      {
        "@id": "https://w3c.github.io/json-ld-api/tests/expand/0078-in.jsonld#santas-little-helper",
        "http://example.com/vocab#name": [ { "@value": "Santa's Little Helper" } ]
      }
    ]
  },
  "http://example.com/vocab#name": [ { "@value": "Homer" } ]
}]
"###).expect("Invalid output JSON"));}

// expand @graph container
//
// Use of @graph containers
#[tokio::test]
async fn t0079() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "input": {"@id": "foo:input", "@container": "@graph"},
    "value": "foo:value"
  },
  "input": {
    "value": "x"
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0079-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "foo:input": [{
    "@graph": [{
      "foo:value": [{
        "@value": "x"
      }]
    }]
  }]
}]
"###).expect("Invalid output JSON"));}

// expand [@graph, @set] container
//
// Use of [@graph, @set] containers
#[tokio::test]
async fn t0080() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "input": {"@id": "foo:input", "@container": ["@graph", "@set"]},
    "value": "foo:value"
  },
  "input": [{
    "value": "x"
  }]
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0080-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "foo:input": [{
    "@graph": [{
      "foo:value": [{
        "@value": "x"
      }]
    }]
  }]
}]
"###).expect("Invalid output JSON"));}

// Creates an @graph container if value is a graph
//
// Don't double-expand an already expanded graph
#[tokio::test]
async fn t0081() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "input": {"@container": "@graph"}
  },
  "input": {
    "@graph": {
      "value": "x"
    }
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0081-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/input": [{
    "@graph": [{
      "@graph": [{
        "http://example.org/value": [{"@value": "x"}]
      }]
    }]
  }]
}]"###).expect("Invalid output JSON"));}

// expand [@graph, @index] container
//
// Use of @graph containers with @index
#[tokio::test]
async fn t0082() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "input": {"@container": ["@graph", "@index"]}
  },
  "input": {
    "g1": {"value": "x"}
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0082-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/input": [{
    "@index": "g1",
    "@graph": [{
      "http://example.org/value": [{"@value": "x"}]
    }]
  }]
}]"###).expect("Invalid output JSON"));}

// expand [@graph, @index, @set] container
//
// Use of @graph containers with @index and @set
#[tokio::test]
async fn t0083() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "input": {"@container": ["@graph", "@index", "@set"]}
  },
  "input": {
    "g1": {"value": "x"}
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0083-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/input": [{
    "@index": "g1",
    "@graph": [{
      "http://example.org/value": [{"@value": "x"}]
    }]
  }]
}]"###).expect("Invalid output JSON"));}

// Do not expand [@graph, @index] container if value is a graph
//
// Does not create a new graph object if indexed value is already a graph object
#[tokio::test]
async fn t0084() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "input": {"@container": ["@graph", "@index"]}
  },
  "input": {
    "g1": {
      "@graph": {
        "value": "x"
      }
    }
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0084-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/input": [{
    "@index": "g1",
    "@graph": [{
      "http://example.org/value": [{"@value": "x"}]
    }]
  }]
}]"###).expect("Invalid output JSON"));}

// expand [@graph, @id] container
//
// Use of @graph containers with @id
#[tokio::test]
async fn t0085() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "input": {"@container": ["@graph", "@id"]}
  },
  "input": {
    "http://example.com/g1": {"value": "x"}
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0085-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/input": [{
    "@id": "http://example.com/g1",
    "@graph": [{
      "http://example.org/value": [{"@value": "x"}]
    }]
  }]
}]"###).expect("Invalid output JSON"));}

// expand [@graph, @id, @set] container
//
// Use of @graph containers with @id and @set
#[tokio::test]
async fn t0086() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "input": {"@container": ["@graph", "@id", "@set"]}
  },
  "input": {
    "http://example.com/g1": {"value": "x"}
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0086-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/input": [{
    "@id": "http://example.com/g1",
    "@graph": [{
      "http://example.org/value": [{"@value": "x"}]
    }]
  }]
}]"###).expect("Invalid output JSON"));}

// Do not expand [@graph, @id] container if value is a graph
//
// Does not create a new graph object if indexed value is already a graph object
#[tokio::test]
async fn t0087() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "input": {"@container": ["@graph", "@id"]}
  },
  "input": {
    "http://example.com/g1": {
      "@graph": {
        "value": "x"
      }
    }
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0087-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/input": [{
    "@id": "http://example.com/g1",
    "@graph": [{
      "http://example.org/value": [{"@value": "x"}]
    }]
  }]
}]"###).expect("Invalid output JSON"));}

// Do not expand native values to IRIs
//
// Value Expansion does not expand native values, such as booleans, to a node object
#[tokio::test]
async fn t0088() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "@base": "http://example.com/",
    "coerceId": {"@type": "@id"},
    "coerceVocab": {"@type": "@vocab"}
  },
  "coerceDefault": ["string", true, false, 0, 1],
  "coerceId": ["string", true, false, 0, 1],
  "coerceVocab": ["string", true, false, 0, 1]
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0088-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
 {
   "http://example.org/coerceDefault": [
     {"@value": "string"},
     {"@value": true},
     {"@value": false},
     {"@value": 0},
     {"@value": 1}
   ],
   "http://example.org/coerceId": [
     {"@id": "http://example.com/string"},
     {"@value": true},
     {"@value": false},
     {"@value": 0},
     {"@value": 1}
   ],
   "http://example.org/coerceVocab": [
     {"@id": "http://example.org/string"},
     {"@value": true},
     {"@value": false},
     {"@value": 0},
     {"@value": 1}
   ]
 }
]
"###).expect("Invalid output JSON"));}

// empty @base applied to the base option
//
// Use of an empty @base is applied to the base option
#[tokio::test]
async fn t0089() {
    let input = r###"{
  "@context": {
    "@base": ""
  },
  "@id": "relative-iri",
  "http://prop": "value"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0089-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@id": "http://example/base/relative-iri",
  "http://prop": [{"@value": "value"}]
}]
"###).expect("Invalid output JSON"));}

// relative @base overrides base option and document location
//
// Use of a relative @base overrides base option and document location
#[tokio::test]
async fn t0090() {
    let input = r###"{
  "@context": {
    "@base": ".."
  },
  "@id": "relative-iri",
  "http://prop": "value"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0090-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@id": "http://example/relative-iri",
  "http://prop": [{"@value": "value"}]
}]
"###).expect("Invalid output JSON"));}

// relative and absolute @base overrides base option and document location
//
// Use of a relative and absolute @base overrides base option and document location
#[tokio::test]
async fn t0091() {
    let input = r###"{
  "@context": [{
    "@base": "http://foo.bar/./baz/"
  }, {
    "@base": "example/"
  }],
  "@id": "relative-iri",
  "http://prop": "value"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0091-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@id": "http://foo.bar/baz/example/relative-iri",
  "http://prop": [{"@value": "value"}]
}]
"###).expect("Invalid output JSON"));}

// Various relative IRIs as properties with with @vocab: ''
//
// Pathological relative property IRIs
#[tokio::test]
async fn t0092() {
    let input = r###"{
  "@context": {
    "@base": "http://example.com/some/deep/directory/and/file/",
    "@vocab": ""
  },
  "@id": "relativePropertyIris",
  "link": "link",
  "#fragment-works": "#fragment-works",
  "?query=works": "?query=works",
  "./": "./",
  "../": "../",
  "../parent": "../parent",
  "../../parent-parent-eq-root": "../../parent-parent-eq-root",
  "../../../../../still-root": "../../../../../still-root",
  "../.././.././../../too-many-dots": "../.././.././../../too-many-dots",
  "/absolute": "/absolute",
  "//example.org/scheme-relative": "//example.org/scheme-relative"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0092-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.com/some/deep/directory/and/file/relativePropertyIris",
    "http://example.com/some/deep/directory/and/file/#fragment-works": [{"@value": "#fragment-works"}],
    "http://example.com/some/deep/directory/and/file/../": [{"@value": "../"}],
    "http://example.com/some/deep/directory/and/file/../../../../../still-root": [{"@value": "../../../../../still-root"}],
    "http://example.com/some/deep/directory/and/file/../.././.././../../too-many-dots": [{"@value": "../.././.././../../too-many-dots"}],
    "http://example.com/some/deep/directory/and/file/../../parent-parent-eq-root": [{"@value": "../../parent-parent-eq-root"}],
    "http://example.com/some/deep/directory/and/file/../parent": [{"@value": "../parent"}],
    "http://example.com/some/deep/directory/and/file/./": [{"@value": "./"}],
    "http://example.com/some/deep/directory/and/file///example.org/scheme-relative": [{"@value": "//example.org/scheme-relative"}],
    "http://example.com/some/deep/directory/and/file//absolute": [{"@value": "/absolute"}],
    "http://example.com/some/deep/directory/and/file/?query=works": [{"@value": "?query=works"}],
    "http://example.com/some/deep/directory/and/file/link": [{"@value": "link"}]
  }
]"###).expect("Invalid output JSON"));}

// expand @graph container (multiple objects)
//
// Use of @graph containers
#[tokio::test]
async fn t0093() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "input": {"@id": "foo:input", "@container": "@graph"},
    "value": "foo:value"
  },
  "input": [{
    "value": "x"
  }, {
    "value": "y"
  }]
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0093-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "foo:input": [{
    "@graph": [{
      "foo:value": [{
        "@value": "x"
      }]
    }]
  }, {
    "@graph": [{
      "foo:value": [{
        "@value": "y"
      }]
    }]
  }]
}]"###).expect("Invalid output JSON"));}

// expand [@graph, @set] container (multiple objects)
//
// Use of [@graph, @set] containers
#[tokio::test]
async fn t0094() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "input": {"@id": "foo:input", "@container": ["@graph", "@set"]},
    "value": "foo:value"
  },
  "input": [{
    "value": "x"
  }, {
    "value": "y"
  }]
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0094-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "foo:input": [{
    "@graph": [{
      "foo:value": [{
        "@value": "x"
      }]
    }]
  }, {
    "@graph": [{
      "foo:value": [{
        "@value": "y"
      }]
    }]
  }]
}]"###).expect("Invalid output JSON"));}

// Creates an @graph container if value is a graph (multiple objects)
//
// Double-expand an already expanded graph
#[tokio::test]
async fn t0095() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "input": {"@container": "@graph"}
  },
  "input": [{
    "@graph": {
      "value": "x"
    }
  }, {
    "@graph": {
      "value": "y"
    }
  }]
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0095-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/input": [{
    "@graph": [{
      "@graph": [{"http://example.org/value": [{"@value": "x"}]}]
    }]
  }, {
    "@graph": [{
      "@graph": [{"http://example.org/value": [{"@value": "y"}]}]
    }]
  }]
}]"###).expect("Invalid output JSON"));}

// expand [@graph, @index] container (multiple indexed objects)
//
// Use of @graph containers with @index
#[tokio::test]
async fn t0096() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "input": {"@container": ["@graph", "@index"]}
  },
  "input": {
    "g1": {"value": "x"},
    "g2": {"value": "y"}
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0096-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/input": [{
    "@index": "g1",
    "@graph": [{
      "http://example.org/value": [{"@value": "x"}]
    }]
  }, {
    "@index": "g2",
    "@graph": [{
      "http://example.org/value": [{"@value": "y"}]
    }]
  }]
}]"###).expect("Invalid output JSON"));}

// expand [@graph, @index, @set] container (multiple objects)
//
// Use of @graph containers with @index and @set
#[tokio::test]
async fn t0097() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "input": {"@container": ["@graph", "@index", "@set"]}
  },
  "input": {
    "g1": {"value": "x"},
    "g2": {"value": "y"}
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0097-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/input": [{
    "@index": "g1",
    "@graph": [{
      "http://example.org/value": [{"@value": "x"}]
    }]
  }, {
    "@index": "g2",
    "@graph": [{
      "http://example.org/value": [{"@value": "y"}]
    }]
  }]
}]"###).expect("Invalid output JSON"));}

// Do not expand [@graph, @index] container if value is a graph (multiple objects)
//
// Does not create a new graph object if indexed value is already a graph object
#[tokio::test]
async fn t0098() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "input": {"@container": ["@graph", "@index"]}
  },
  "input": {
    "g1": {"@graph": {"value": "x"}},
    "g2": {"@graph": {"value": "y"}}
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0098-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/input": [{
    "@index": "g1",
    "@graph": [{
      "http://example.org/value": [{"@value": "x"}]
    }]
  }, {
    "@index": "g2",
    "@graph": [{
      "http://example.org/value": [{"@value": "y"}]
    }]
  }]
}]"###).expect("Invalid output JSON"));}

// expand [@graph, @id] container (multiple objects)
//
// Use of @graph containers with @id
#[tokio::test]
async fn t0099() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "input": {"@container": ["@graph", "@id"]}
  },
  "input": {
    "http://example.com/g1": {"value": "x"},
    "http://example.com/g2": {"value": "y"}
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0099-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/input": [{
    "@id": "http://example.com/g1",
    "@graph": [{
      "http://example.org/value": [{"@value": "x"}]
    }]
  }, {
    "@id": "http://example.com/g2",
    "@graph": [{
      "http://example.org/value": [{"@value": "y"}]
    }]
  }]
}]"###).expect("Invalid output JSON"));}

// expand [@graph, @id, @set] container (multiple objects)
//
// Use of @graph containers with @id and @set
#[tokio::test]
async fn t0100() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "input": {"@container": ["@graph", "@id", "@set"]}
  },
  "input": {
    "http://example.com/g1": {"value": "x"},
    "http://example.com/g2": {"value": "y"}
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0100-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/input": [{
    "@id": "http://example.com/g1",
    "@graph": [{
      "http://example.org/value": [{"@value": "x"}]
    }]
  }, {
    "@id": "http://example.com/g2",
    "@graph": [{
      "http://example.org/value": [{"@value": "y"}]
    }]
  }]
}]"###).expect("Invalid output JSON"));}

// Do not expand [@graph, @id] container if value is a graph (multiple objects)
//
// Does not create a new graph object if indexed value is already a graph object
#[tokio::test]
async fn t0101() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "input": {"@container": ["@graph", "@id"]}
  },
  "input": {
    "http://example.com/g1": {
      "@graph": {
        "value": "x"
      }
    },
    "http://example.com/g2": {
      "@graph": {
        "value": "y"
      }
    }
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0101-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/input": [{
    "@id": "http://example.com/g1",
    "@graph": [{
      "http://example.org/value": [{"@value": "x"}]
    }]
  }, {
    "@id": "http://example.com/g2",
    "@graph": [{
      "http://example.org/value": [{"@value": "y"}]
    }]
  }]
}]"###).expect("Invalid output JSON"));}

// Expand @graph container if value is a graph (multiple objects)
//
// Creates a new graph object if indexed value is already a graph object
#[tokio::test]
async fn t0102() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "input": {"@container": "@graph"}
  },
  "input": {
    "@graph": [{
      "value": "x"
    }, {
      "value": "y"
    }]
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0102-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/input": [
    {
      "@graph": [{
        "@graph": [{
          "http://example.org/value": [{"@value": "x"}]
        }, {
          "http://example.org/value": [{"@value": "y"}]
        }]
      }]
    }
  ]
}]
"###).expect("Invalid output JSON"));}

// Expand @graph container if value is a graph (multiple graphs)
//
// Creates a new graph object if indexed value is already a graph object
#[tokio::test]
async fn t0103() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "input": {"@container": "@graph"}
  },
  "input": [{
    "@graph": {
      "value": "x"
    }
  }, {
    "@graph": {
      "value": "y"
    }
  }]
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0103-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/input": [
    {
      "@graph": [{
        "@graph": [{"http://example.org/value": [{"@value": "x"}]}]
      }]
    }, {
      "@graph": [{
        "@graph": [{"http://example.org/value": [{"@value": "y"}]}]
      }]
    }
  ]
}]"###).expect("Invalid output JSON"));}

// Creates an @graph container if value is a graph (mixed graph and object)
//
// Double-expand an already expanded graph
#[tokio::test]
async fn t0104() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "input": {"@container": "@graph"}
  },
  "input": [
    {"@graph": {"value": "x"}},
    {"value": "y"}
  ]
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0104-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/input": [
    {
      "@graph": [{
        "@graph": [{"http://example.org/value": [{"@value": "x"}]}]
      }]
    }, {
      "@graph": [{
          "http://example.org/value": [{"@value": "y"}]
      }]
    }
  ]
}]"###).expect("Invalid output JSON"));}

// Do not expand [@graph, @index] container if value is a graph (mixed graph and object)
//
// Does not create a new graph object if indexed value is already a graph object
#[tokio::test]
async fn t0105() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "input": {"@container": ["@graph", "@index"]}
  },
  "input": {
    "g1": [{"@graph": {"value": "x"}}, {"value": "y"}],
    "g2": [{"@graph": {"value": "a"}}, {"value": "b"}]
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0105-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/input": [{
    "@index": "g1",
    "@graph": [{
      "http://example.org/value": [{"@value": "x"}]
    }]
  }, {
    "@index": "g1",
    "@graph": [{
      "http://example.org/value": [{"@value": "y"}]
    }]
  }, {
    "@index": "g2",
    "@graph": [{
      "http://example.org/value": [{"@value": "a"}]
    }]
  }, {
    "@index": "g2",
    "@graph": [{
      "http://example.org/value": [{"@value": "b"}]
    }]
  }]
}]"###).expect("Invalid output JSON"));}

// Do not expand [@graph, @id] container if value is a graph (mixed graph and object)
//
// Does not create a new graph object if indexed value is already a graph object
#[tokio::test]
async fn t0106() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "input": {"@container": ["@graph", "@id"]}
  },
  "input": {
    "http://example.com/g1": {
      "@graph": {
        "value": "x"
      }
    },
    "http://example.com/g2": {
      "@graph": {
        "value": "y"
      }
    }
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0106-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/input": [{
    "@id": "http://example.com/g1",
    "@graph": [{
      "http://example.org/value": [{"@value": "x"}]
    }]
  }, {
    "@id": "http://example.com/g2",
    "@graph": [{
      "http://example.org/value": [{"@value": "y"}]
    }]
  }]
}]"###).expect("Invalid output JSON"));}

// expand [@graph, @index] container (indexes with multiple objects)
//
// Use of @graph containers with @index
#[tokio::test]
async fn t0107() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "input": {"@container": ["@graph", "@index"]}
  },
  "input": {
    "g1": [{"value": "x"}, {"value": "y"}],
    "g2": [{"value": "a"}, {"value": "b"}]
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0107-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/input": [{
    "@index": "g1",
    "@graph": [{
      "http://example.org/value": [{"@value": "x"}]
    }]
  }, {
    "@index": "g1",
    "@graph": [{
      "http://example.org/value": [{"@value": "y"}]
    }]
  }, {
    "@index": "g2",
    "@graph": [{
      "http://example.org/value": [{"@value": "a"}]
    }]
  }, {
    "@index": "g2",
    "@graph": [{
      "http://example.org/value": [{"@value": "b"}]
    }]
  }]
}]"###).expect("Invalid output JSON"));}

// expand [@graph, @id] container (multiple ids and objects)
//
// Use of @graph containers with @id
#[tokio::test]
async fn t0108() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "input": {"@container": ["@graph", "@id"]}
  },
  "input": {
    "http://example.com/g1": [{"value": "x"}, {"value": "y"}],
    "http://example.com/g2": [{"value": "a"}, {"value": "b"}]
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0108-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/input": [{
    "@id": "http://example.com/g1",
    "@graph": [{
      "http://example.org/value": [{"@value": "x"}]
    }]
  }, {
    "@id": "http://example.com/g1",
    "@graph": [{
      "http://example.org/value": [{"@value": "y"}]
    }]
  }, {
    "@id": "http://example.com/g2",
    "@graph": [{
      "http://example.org/value": [{"@value": "a"}]
    }]
  }, {
    "@id": "http://example.com/g2",
    "@graph": [{
      "http://example.org/value": [{"@value": "b"}]
    }]
  }]
}]"###).expect("Invalid output JSON"));}

// IRI expansion of fragments including ':'
//
// Do not treat as absolute IRIs values that look like compact IRIs if they're not absolute
#[tokio::test]
async fn t0109() {
    let input = r###"{
  "@context": {
    "@base": "https://ex.org/",
    "u": {"@id": "urn:u:", "@type": "@id"}
  },
  "u": ["#Test", "#Test:2"]
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0109-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "urn:u:": [
    {"@id": "https://ex.org/#Test"},
    {"@id": "https://ex.org/#Test:2"}
  ]
}]"###).expect("Invalid output JSON"));}

// Various relative IRIs as properties with with relative @vocab
//
// Pathological relative property IRIs
#[tokio::test]
async fn t0110() {
    let input = r###"{
  "@context": {
    "@base": "http://example.com/some/deep/directory/and/file/",
    "@vocab": "/relative"
  },
  "@id": "relativePropertyIris",
  "link": "link",
  "#fragment-works": "#fragment-works",
  "?query=works": "?query=works",
  "./": "./",
  "../": "../",
  "../parent": "../parent",
  "../../parent-parent-eq-root": "../../parent-parent-eq-root",
  "../../../../../still-root": "../../../../../still-root",
  "../.././.././../../too-many-dots": "../.././.././../../too-many-dots",
  "/absolute": "/absolute",
  "//example.org/scheme-relative": "//example.org/scheme-relative"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0110-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.com/some/deep/directory/and/file/relativePropertyIris",
    "http://example.com/relative#fragment-works": [{"@value": "#fragment-works"}],
    "http://example.com/relative../": [{"@value": "../"}],
    "http://example.com/relative../../../../../still-root": [{"@value": "../../../../../still-root"}],
    "http://example.com/relative../.././.././../../too-many-dots": [{"@value": "../.././.././../../too-many-dots"}],
    "http://example.com/relative../../parent-parent-eq-root": [{"@value": "../../parent-parent-eq-root"}],
    "http://example.com/relative../parent": [{"@value": "../parent"}],
    "http://example.com/relative./": [{"@value": "./"}],
    "http://example.com/relative//example.org/scheme-relative": [{"@value": "//example.org/scheme-relative"}],
    "http://example.com/relative/absolute": [{"@value": "/absolute"}],
    "http://example.com/relative?query=works": [{"@value": "?query=works"}],
    "http://example.com/relativelink": [{"@value": "link"}]
  }
]"###).expect("Invalid output JSON"));}

// Various relative IRIs as properties with with relative @vocab itself relative to an existing vocabulary base
//
// Pathological relative property IRIs
#[tokio::test]
async fn t0111() {
    let input = r###"{
  "@context": [{
    "@version": 1.1,
    "@base": "http://example.com/some/deep/directory/and/file/",
    "@vocab": "http://example.com/vocabulary/"
  }, {
    "@vocab": "./rel2#"
  }],
  "@id": "relativePropertyIris",
  "link": "link",
  "#fragment-works": "#fragment-works",
  "?query=works": "?query=works",
  "./": "./",
  "../": "../",
  "../parent": "../parent",
  "../../parent-parent-eq-root": "../../parent-parent-eq-root",
  "../../../../../still-root": "../../../../../still-root",
  "../.././.././../../too-many-dots": "../.././.././../../too-many-dots",
  "/absolute": "/absolute",
  "//example.org/scheme-relative": "//example.org/scheme-relative"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0111-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.com/some/deep/directory/and/file/relativePropertyIris",
    "http://example.com/vocabulary/./rel2##fragment-works": [{"@value": "#fragment-works"}],
    "http://example.com/vocabulary/./rel2#../": [{"@value": "../"}],
    "http://example.com/vocabulary/./rel2#../../../../../still-root": [{"@value": "../../../../../still-root"}],
    "http://example.com/vocabulary/./rel2#../.././.././../../too-many-dots": [{"@value": "../.././.././../../too-many-dots"}],
    "http://example.com/vocabulary/./rel2#../../parent-parent-eq-root": [{"@value": "../../parent-parent-eq-root"}],
    "http://example.com/vocabulary/./rel2#../parent": [{"@value": "../parent"}],
    "http://example.com/vocabulary/./rel2#./": [{"@value": "./"}],
    "http://example.com/vocabulary/./rel2#//example.org/scheme-relative": [{"@value": "//example.org/scheme-relative"}],
    "http://example.com/vocabulary/./rel2#/absolute": [{"@value": "/absolute"}],
    "http://example.com/vocabulary/./rel2#?query=works": [{"@value": "?query=works"}],
    "http://example.com/vocabulary/./rel2#link": [{"@value": "link"}]
  }
]"###).expect("Invalid output JSON"));}

// Various relative IRIs as properties with with relative @vocab relative to another relative vocabulary base
//
// Pathological relative property IRIs
#[tokio::test]
async fn t0112() {
    let input = r###"{
  "@context": [{
    "@version": 1.1,
    "@base": "http://example.com/some/deep/directory/and/file/",
    "@vocab": "/rel1"
  }, {
    "@vocab": "./rel2#"
  }],
  "@id": "relativePropertyIris",
  "link": "link",
  "#fragment-works": "#fragment-works",
  "?query=works": "?query=works",
  "./": "./",
  "../": "../",
  "../parent": "../parent",
  "../../parent-parent-eq-root": "../../parent-parent-eq-root",
  "../../../../../still-root": "../../../../../still-root",
  "../.././.././../../too-many-dots": "../.././.././../../too-many-dots",
  "/absolute": "/absolute",
  "//example.org/scheme-relative": "//example.org/scheme-relative"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0112-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.com/some/deep/directory/and/file/relativePropertyIris",
    "http://example.com/rel1./rel2##fragment-works": [{"@value": "#fragment-works"}],
    "http://example.com/rel1./rel2#../": [{"@value": "../"}],
    "http://example.com/rel1./rel2#../../../../../still-root": [{"@value": "../../../../../still-root"}],
    "http://example.com/rel1./rel2#../.././.././../../too-many-dots": [{"@value": "../.././.././../../too-many-dots"}],
    "http://example.com/rel1./rel2#../../parent-parent-eq-root": [{"@value": "../../parent-parent-eq-root"}],
    "http://example.com/rel1./rel2#../parent": [{"@value": "../parent"}],
    "http://example.com/rel1./rel2#./": [{"@value": "./"}],
    "http://example.com/rel1./rel2#//example.org/scheme-relative": [{"@value": "//example.org/scheme-relative"}],
    "http://example.com/rel1./rel2#/absolute": [{"@value": "/absolute"}],
    "http://example.com/rel1./rel2#?query=works": [{"@value": "?query=works"}],
    "http://example.com/rel1./rel2#link": [{"@value": "link"}]
  }
]"###).expect("Invalid output JSON"));}

// context with JavaScript Object property names
//
// Expand with context including JavaScript Object property names
#[tokio::test]
async fn t0113() {
    let input = r###"{
  "@context": {
    "valueOf": "http://example.org/valueOf",
    "toString": "http://example.org/toString"
  },
  "valueOf": "first",
  "toString": "second"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0113-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "http://example.org/toString": [
      {
        "@value": "second"
      }
    ],
    "http://example.org/valueOf": [
      {
        "@value": "first"
      }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// Expansion allows multiple properties expanding to @type
//
// An exception for the colliding keywords error is made for @type
#[tokio::test]
async fn t0114() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "type1": "@type",
    "type2": "@type"
  },
  "type1": "Type1",
  "type2": "Type2"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0114-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@type": ["http://example.org/Type1", "http://example.org/Type2"]
}]"###).expect("Invalid output JSON"));}

// Verifies that relative IRIs as properties with @vocab: '' in 1.0 generate an error
//
// Relative property IRIs with relative @vocab in 1.0
#[tokio::test]
async fn t0115() {
    let input = r###"{
  "@context": {
    "@base": "http://example.com/some/deep/directory/and/file/",
    "@vocab": ""
  },
  "@id": "relativePropertyIris",
  "link": "link"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0115-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid vocab mapping");}

// Verifies that relative IRIs as properties with relative @vocab in 1.0 generate an error
//
// Relative property IRIs with relative @vocab in 1.0
#[tokio::test]
async fn t0116() {
    let input = r###"{
  "@context": {
    "@base": "http://example.com/some/deep/directory/and/file/",
    "@vocab": "/relative"
  },
  "@id": "relativePropertyIris",
  "link": "link"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0116-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid vocab mapping");}

// A term starting with a colon can expand to a different IRI
//
// Terms may begin with a colon and not be treated as IRIs.
#[tokio::test]
async fn t0117() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/vocab",
    ":term": {"@type": "@id"}
  },
  ":term": "http://example.org/base"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0117-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/vocab:term": [{"@id": "http://example.org/base"}]
}]"###).expect("Invalid output JSON"));}

// Expanding a value staring with a colon does not treat that value as an IRI
//
// Terms may begin with a colon and not be treated as IRIs.
#[tokio::test]
async fn t0118() {
    let input = r###"{
  "@context": {"@vocab": "http://schema.org/"},
  "@id": "foo:bar-id",
  "@type": "foo:bar-type",
  "foo:bar": "is an absolute iri property",
  "term": "is schema.org/term",
  ":fish": "is schema.org/:fish"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0118-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@id": "foo:bar-id",
  "@type": ["foo:bar-type"],
  "foo:bar": [{"@value": "is an absolute iri property"}],
  "http://schema.org/term": [{"@value": "is schema.org/term"}],
  "http://schema.org/:fish": [{"@value": "is schema.org/:fish"}]
}]"###).expect("Invalid output JSON"));}

// Ignore some terms with @, allow others.
//
// Processors SHOULD generate a warning and MUST ignore terms having the form of a keyword.
#[tokio::test]
async fn t0119() {
    let input = r###"{
  "@context": {
    "@": "http://example.org/vocab/at",
    "@foo.bar": "http://example.org/vocab/foo.bar",
    "@ignoreMe": "http://example.org/vocab/ignoreMe"
  },
  "@": "allowed",
  "@foo.bar": "allowed",
  "@ignoreMe": "ignored"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0119-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/vocab/at": [{"@value": "allowed"}],
  "http://example.org/vocab/foo.bar": [{"@value": "allowed"}]
}]"###).expect("Invalid output JSON"));}

// Ignore some values of @id with @, allow others.
//
// Processors SHOULD generate a warning and MUST ignore values of @id having the form of a keyword.
#[tokio::test]
async fn t0120() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.orb/vocab/",
    "at": {"@id": "@"},
    "foo.bar": {"@id": "@foo.bar"},
    "ignoreMe": {"@id": "@ignoreMe"}
  },
  "at": "allowed",
  "foo.bar": "allowed",
  "ignoreMe": "resolves to @vocab"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0120-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.orb/vocab/@": [{"@value": "allowed"}],
  "http://example.orb/vocab/@foo.bar": [{"@value": "allowed"}],
  "http://example.orb/vocab/ignoreMe": [{"@value": "resolves to @vocab"}]
}]"###).expect("Invalid output JSON"));}

// Ignore some values of @reverse with @, allow others.
//
// Processors SHOULD generate a warning and MUST ignore values of @reverse having the form of a keyword.
#[tokio::test]
async fn t0121() {
    let input = r###"{
  "@context": {
    "@base": "http://example.org/",
    "@vocab": "http://example.org/vocab/",
    "at": {"@reverse": "@"},
    "foo.bar": {"@reverse": "@foo.bar"}
  },
  "@id": "foo",
  "at": {"@id": "allowed"},
  "foo.bar": {"@id": "allowed"}
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0121-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@id": "http://example.org/foo",
  "@reverse": {
    "http://example.org/vocab/@": [{"@id": "http://example.org/allowed"}],
    "http://example.org/vocab/@foo.bar": [{"@id": "http://example.org/allowed"}]
  }
}]
"###).expect("Invalid output JSON"));}

// Ignore some IRIs when that start with @ when expanding.
//
// Processors SHOULD generate a warning and MUST ignore IRIs having the form of a keyword.
#[tokio::test]
async fn t0122() {
    let input = r###"{
  "@context": {
    "@base": "http://example.org/"
  },
  "http://example.org/vocab/at": {"@id": "@"},
  "http://example.org/vocab/foo.bar": {"@id": "@foo.bar"},
  "http://example.org/vocab/ignoreme": {"@id": "@ignoreMe"}
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0122-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/vocab/at": [{"@id": "http://example.org/@"}],
  "http://example.org/vocab/foo.bar": [{"@id": "http://example.org/@foo.bar"}],
  "http://example.org/vocab/ignoreme": [{"@id": null}]
}]"###).expect("Invalid output JSON"));}

// Value objects including invalid literal datatype IRIs are rejected
//
// Processors MUST validate datatype IRIs.
#[tokio::test]
async fn t0123() {
    let input = r###"{
  "@id": "http://example.com/foo",
  "http://example.com/bar": {"@value": "bar", "@type": "http://example.com/baz z"}
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0123-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid typed value");}

// compact IRI as @vocab
//
// Verifies that @vocab defined as a compact IRI expands properly
#[tokio::test]
async fn t0124() {
    let input = r###"{
  "@context": [
    {
      "@version": 1.1,
      "ex": {
        "@id": "http://example.org/",
        "@prefix": true
      }
    },
    {
      "@vocab": "ex:ns/"
    }
  ],
  "foo": "bar"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0124-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "http://example.org/ns/foo": [
      {
        "@value": "bar"
      }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// term as @vocab
//
// Verifies that @vocab defined as a term expands properly
#[tokio::test]
async fn t0125() {
    let input = r###"{
  "@context": [
    {
      "@version": 1.1,
      "ex": {
        "@id": "http://example.org/",
        "@prefix": true
      }
    },
    {
      "@vocab": "ex"
    }
  ],
  "foo": "bar"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0125-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "http://example.org/foo": [
      {
        "@value": "bar"
      }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// A scoped context may include itself recursively (direct)
//
// Verifies that no exception is raised on expansion when processing a scoped context referencing itself directly
#[tokio::test]
async fn t0126() {
    let input = r###"{
  "@context": "0126-context.jsonld",
  "@id": "ex:id",
  "prop": {
    "value": "v"
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0126-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "ex:id",
    "ex:prop": [
      {
        "ex:value": [
          {
            "@value": "v"
          }
        ]
      }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// A scoped context may include itself recursively (indirect)
//
// Verifies that no exception is raised on expansion when processing a scoped context referencing itself indirectly
#[tokio::test]
async fn t0127() {
    let input = r###"{
  "@context": "0127-context-1.jsonld",
  "@id": "ex:id",
  "prop": {
    "value": "v"
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0127-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "ex:id",
    "ex:prop": [
      {
        "ex:value": [
          {
            "@value": "v"
          }
        ]
      }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// Two scoped context may include a shared context
//
// Verifies that no exception is raised on expansion when processing two scoped contexts referencing a shared context
#[tokio::test]
async fn t0128() {
    let input = r###"{
  "@context": [
    "0128-context-1.jsonld",
    "0128-context-2.jsonld"
  ],
  "@id": "ex:id",
  "prop": {
    "value": "v"
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0128-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "ex:id",
    "ex:prop": [
      {
        "ex:value": [
          {
            "@value": "v"
          }
        ]
      }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// Base without trailing slash, without path
//
// Verify URI resolution relative to base (without trailing slash, without path) according to RFC 3986
#[tokio::test]
async fn t0129() {
    let input = r###"{
  "@context" : {"@base": "http://example"},
  "@id": "relative-iri",
  "http://prop": "value"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0129-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@id": "http://example/relative-iri",
  "http://prop": [{"@value": "value"}]
}]
"###).expect("Invalid output JSON"));}

// Base without trailing slash, with path
//
// Verify URI resolution relative to base (without trailing slash, with path) according to RFC 3986
#[tokio::test]
async fn t0130() {
    let input = r###"{
  "@context" : {"@base": "http://example/base"},
  "@id": "relative-iri",
  "http://prop": "value"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/0130-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@id": "http://example/relative-iri",
  "http://prop": [{"@value": "value"}]
}]
"###).expect("Invalid output JSON"));}

// adding new term
//
// Expansion using a scoped context uses term scope for selecting proper term
#[tokio::test]
async fn tc001() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example/",
    "foo": {"@context": {"bar": "http://example.org/bar"}}
  },
  "foo": {
    "bar": "baz"
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/c001-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "http://example/foo": [{"http://example.org/bar": [{"@value": "baz"}]}]
  }
]"###).expect("Invalid output JSON"));}

// overriding a term
//
// Expansion using a scoped context uses term scope for selecting proper term
#[tokio::test]
async fn tc002() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example/",
    "foo": {"@context": {"bar": {"@type": "@id"}}},
    "bar": {"@type": "http://www.w3.org/2001/XMLSchema#string"}
  },
  "foo": {
    "bar": "http://example/baz"
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/c002-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "http://example/foo": [{"http://example/bar": [{"@id": "http://example/baz"}]}]
  }
]"###).expect("Invalid output JSON"));}

// property and value with different terms mapping to the same expanded property
//
// Expansion using a scoped context uses term scope for selecting proper term
#[tokio::test]
async fn tc003() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example/",
    "foo": {"@context": {"Bar": {"@id": "bar"}}}
  },
  "foo": {
    "Bar": "baz"
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/c003-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "http://example/foo": [{
      "http://example/bar": [
        {"@value": "baz"}
      ]}
    ]
  }
]"###).expect("Invalid output JSON"));}

// deep @context affects nested nodes
//
// Expansion using a scoped context uses term scope for selecting proper term
#[tokio::test]
async fn tc004() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example/",
    "foo": {"@context": {"baz": {"@type": "@vocab"}}}
  },
  "foo": {
    "bar": {
      "baz": "buzz"
    }
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/c004-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "http://example/foo": [{
      "http://example/bar": [{
        "http://example/baz": [{"@id": "http://example/buzz"}]
      }]
    }]
  }
]"###).expect("Invalid output JSON"));}

// scoped context layers on intemediate contexts
//
// Expansion using a scoped context uses term scope for selecting proper term
#[tokio::test]
async fn tc005() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example/",
    "b": {"@context": {"c": "http://example.org/c"}}
  },
  "a": {
    "@context": {"@vocab": "http://example.com/"},
    "b": {
      "a": "A in example.com",
      "c": "C in example.org"
    },
    "c": "C in example.com"
  },
  "c": "C in example"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/c005-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example/a": [{
    "http://example.com/c": [{"@value": "C in example.com"}],
    "http://example/b": [{
      "http://example.com/a": [{"@value": "A in example.com"}],
      "http://example.org/c": [{"@value": "C in example.org"}]
    }]
  }],
  "http://example/c": [{"@value": "C in example"}]
}]"###).expect("Invalid output JSON"));}

// adding new term
//
// scoped context on @type
#[tokio::test]
async fn tc006() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example/",
    "Foo": {"@context": {"bar": "http://example.org/bar"}}
  },
  "a": {"@type": "Foo", "bar": "baz"}
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/c006-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "http://example/a": [{
      "@type": ["http://example/Foo"],
      "http://example.org/bar": [{"@value": "baz"}]
    }]
  }
]"###).expect("Invalid output JSON"));}

// overriding a term
//
// scoped context on @type
#[tokio::test]
async fn tc007() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example/",
    "Foo": {"@context": {"bar": {"@type": "@id"}}},
    "bar": {"@type": "http://www.w3.org/2001/XMLSchema#string"}
  },
  "a": {"@type": "Foo", "bar": "http://example/baz"}
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/c007-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "http://example/a": [{
      "@type": ["http://example/Foo"],
      "http://example/bar": [{"@id": "http://example/baz"}]
    }]
  }
]"###).expect("Invalid output JSON"));}

// alias of @type
//
// scoped context on @type
#[tokio::test]
async fn tc008() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example/",
    "type": "@type",
    "Foo": {"@context": {"bar": "http://example.org/bar"}}
  },
  "a": {"type": "Foo", "bar": "baz"}
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/c008-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "http://example/a": [{
      "@type": ["http://example/Foo"],
      "http://example.org/bar": [{"@value": "baz"}]
    }]
  }
]"###).expect("Invalid output JSON"));}

// deep @type-scoped @context does NOT affect nested nodes
//
// scoped context on @type
#[tokio::test]
async fn tc009() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example/",
    "Foo": {"@context": {"baz": {"@type": "@vocab"}}}
  },
  "@type": "Foo",
  "bar": {"baz": "buzz"}
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/c009-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@type": ["http://example/Foo"],
    "http://example/bar": [{
      "http://example/baz": [{"@value": "buzz"}]
    }]
  }
]
"###).expect("Invalid output JSON"));}

// scoped context layers on intemediate contexts
//
// scoped context on @type
#[tokio::test]
async fn tc010() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example/",
    "B": {"@context": {"c": "http://example.org/c"}}
  },
  "a": {
    "@context": {"@vocab": "http://example.com/"},
    "@type": "B",
    "a": "A in example.com",
    "c": "C in example.org"
  },
  "c": "C in example"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/c010-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example/a": [{
    "@type": ["http://example/B"],
    "http://example.com/a": [{"@value": "A in example.com"}],
    "http://example.org/c": [{"@value": "C in example.org"}]
  }],
  "http://example/c": [{"@value": "C in example"}]
}]"###).expect("Invalid output JSON"));}

// orders @type terms when applying scoped contexts
//
// scoped context on @type
#[tokio::test]
async fn tc011() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example/",
    "t1": {"@context": {"foo": {"@id": "http://example.com/foo"}}},
    "t2": {"@context": {"foo": {"@id": "http://example.org/foo", "@type": "@id"}}}
  },
  "@type": ["t2", "t1"],
  "foo": "urn:bar"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/c011-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@type": ["http://example/t2", "http://example/t1"],
  "http://example.org/foo": [
    {"@id": "urn:bar"}
  ]
}]"###).expect("Invalid output JSON"));}

// deep property-term scoped @context in @type-scoped @context affects nested nodes
//
// scoped context on @type
#[tokio::test]
async fn tc012() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example/",
    "Foo": {
      "@context": {
        "bar": {
          "@context": {
            "baz": {"@type": "@vocab"}
          }
        }
      }
    }
  },
  "@type": "Foo",
  "bar": {"baz": "buzz"}
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/c012-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@type": ["http://example/Foo"],
    "http://example/bar": [{
      "http://example/baz": [{"@id": "http://example/buzz"}]
    }]
  }
]
"###).expect("Invalid output JSON"));}

// type maps use scoped context from type index and not scoped context from containing
//
// scoped context on @type
#[tokio::test]
async fn tc013() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example/",
    "prop": {"@container": "@index"},
    "foo": "http://example/base-foo",
    "Outer": {
      "@context": {
        "prop": {
          "@id": "http://example/outer-prop",
          "@container": "@type"
        }
      }
    },
    "Inner": {"@context": {"foo": "http://example/inner-foo"}}
  },
  "@type": "Outer",
  "prop": {
    "Inner": {
      "prop": {
        "index": {
          "@id": "http://example/inner-with-index",
          "foo": "inner-foo"
        }
      }
    }
  },
  "foo": "base-foo"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/c013-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
 "@type": ["http://example/Outer"],
 "http://example/base-foo": [{"@value": "base-foo"}],
 "http://example/outer-prop": [{
   "@type": ["http://example/Inner"],
   "http://example/prop": [{
     "@id": "http://example/inner-with-index",
     "@index": "index",
     "http://example/inner-foo": [{"@value": "inner-foo"}]
   }]
 }]
}]"###).expect("Invalid output JSON"));}

// type-scoped context nullification
//
// type-scoped context nullification
#[tokio::test]
async fn tc014() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example/",
    "foo": "http://example/foo",
    "Type": {
      "@context": [
        null
      ]
    }
  },
  "foo": "will-exist",
  "p": {
    "@type": "Type",
    "foo": "will-not-exist"
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/c014-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example/foo": [{
    "@value": "will-exist"
  }],
  "http://example/p": [{
    "@type": ["http://example/Type"]
  }]
}]
"###).expect("Invalid output JSON"));}

// type-scoped base
//
// type-scoped base
#[tokio::test]
async fn tc015() {
    let input = r###"{
  "@context": {
    "@base": "http://example/base-base",
    "@vocab": "http://example/",
    "foo": "http://example/foo",
    "Type": {
      "@context": {
        "@base": "http://example/typed-base"
      }
    }
  },
  "@id": "#base-id",
  "p": {
    "@id": "#typed-id",
    "@type": "Type",
    "subjectReference": {
      "@id": "#subject-reference-id"
    },
    "nestedNode": {
      "@id": "#nested-id",
      "foo": "bar"
    }
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/c015-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@id": "http://example/base-base#base-id",
  "http://example/p": [{
    "@id": "http://example/typed-base#typed-id",
    "@type": ["http://example/Type"],
    "http://example/subjectReference": [{
      "@id": "http://example/typed-base#subject-reference-id"
    }],
    "http://example/nestedNode": [{
      "@id": "http://example/base-base#nested-id",
      "http://example/foo": [{
        "@value": "bar"
      }]
    }]
  }]
}]
"###).expect("Invalid output JSON"));}

// type-scoped vocab
//
// type-scoped vocab
#[tokio::test]
async fn tc016() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "Type": {
      "@context": {
        "@vocab": "http://example.com/"
      }
    }
  },
  "foo": "org",
  "p": {
    "@type": "Type",
    "foo": "com",
    "nested": {
      "nested-prop": "org"
    }
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/c016-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/foo": [{
    "@value": "org"
  }],
  "http://example.org/p": [{
    "@type": ["http://example.org/Type"],
    "http://example.com/foo": [{
      "@value": "com"
    }],
    "http://example.com/nested": [{
      "http://example.org/nested-prop": [{
        "@value": "org"
      }]
    }]
  }]
}]
"###).expect("Invalid output JSON"));}

// multiple type-scoped contexts are properly reverted
//
// multiple type-scoped contexts are property reverted
#[tokio::test]
async fn tc017() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example/",
    "Bar": {
      "@context": [
        {
          "prop": "http://example/bar-prop"
        }
      ]
    },
    "Foo": {
      "@context": [
        {
          "prop": "http://example/foo-prop"
        }
      ]
    }
  },
  "@type": ["Foo", "Bar"],
  "prop": "foo",
  "nested": {
    "prop": "vocab"
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/c017-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@type": [
    "http://example/Foo",
    "http://example/Bar"
  ],
  "http://example/foo-prop": [{
    "@value": "foo"
  }],
  "http://example/nested": [{
    "http://example/prop": [{
      "@value": "vocab"
    }]
  }]
}]
"###).expect("Invalid output JSON"));}

// multiple type-scoped types resolved against previous context
//
// multiple type-scoped types resolved against previous context
#[tokio::test]
async fn tc018() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example/",
    "Bar": {
      "@context": [
        null,
        {
          "prop": "http://example/bar-prop"
        }
      ]
    },
    "Foo": {
      "@context": [
        null,
        {
          "prop": "http://example/foo-prop"
        }
      ]
    }
  },
  "@type": ["Foo", "Bar"],
  "prop": "foo",
  "nested": {
    "prop": "will-not-exist"
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/c018-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@type": [
    "http://example/Foo",
    "http://example/Bar"
  ],
  "http://example/foo-prop": [
    {
      "@value": "foo"
    }
  ]
}]
"###).expect("Invalid output JSON"));}

// type-scoped context with multiple property scoped terms
//
// type-scoped context with multiple property scoped terms
#[tokio::test]
async fn tc019() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example/",
    "prop": "http://example/base-prop",
    "Type": {
      "@context": {
        "foo": {
          "@context": {
            "prop": "http://example/foo-prop"
          }
        },
        "bar": {
          "@context": {
            "prop": "http://example/bar-prop"
          }
        }
      }
    }
  },
  "@type": "Type",
  "foo": {
    "prop": "foo"
  },
  "bar": {
    "prop": "bar"
  },
  "baz": {
    "prop": "baz"
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/c019-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@type": [
    "http://example/Type"
  ],
  "http://example/foo": [{
    "http://example/foo-prop": [
      {
        "@value": "foo"
      }
    ]
  }],
  "http://example/bar": [{
    "http://example/bar-prop": [
      {
        "@value": "bar"
      }
    ]
  }],
  "http://example/baz": [{
    "http://example/base-prop": [
      {
        "@value": "baz"
      }
    ]
  }]
}]
"###).expect("Invalid output JSON"));}

// type-scoped value
//
// type-scoped value
#[tokio::test]
async fn tc020() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example/",
    "type": "@type",
    "Type": {
      "@context": {
        "value": "@value"
      }
    }
  },
  "type": "Type",
  "v": {
    "value": "value",
    "type": "value-type"
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/c020-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@type": ["http://example/Type"],
  "http://example/v": [{
    "@type": "http://example/value-type",
    "@value": "value"
  }]
}]
"###).expect("Invalid output JSON"));}

// type-scoped value mix
//
// type-scoped value mix
#[tokio::test]
async fn tc021() {
    let input = r###"{
  "@context": {
    "@vocab": "ex:",
    "type": "@type",
    "prop": "ex:untyped",
    "Type": {
      "@context": {
        "prop": "ex:typed",
        "value": "@value"
      }
    }
  },
  "prop": {
    "type": "Type",
    "prop": [
      "v1",
      {
        "value": "v2"
      },
      {
        "@value": "v3"
      },
      {
        "prop": [
          "v4",
          {
            "type": "Type",
            "prop": "v5"
          }
        ]
      }
    ]
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/c021-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "ex:untyped": [{
    "@type": ["ex:Type"],
    "ex:typed": [{
      "@value": "v1"
    }, {
      "@value": "v2"
    }, {
      "@value": "v3"
    }, {
      "ex:untyped": [{
        "@value": "v4"
      }, {
        "@type": ["ex:Type"],
        "ex:typed": [{"@value": "v5"}]
      }]
    }]
  }]
}]
"###).expect("Invalid output JSON"));}

// type-scoped property-scoped contexts including @type:@vocab
//
// type-scoped property-scoped contexts including @type:@vocab
#[tokio::test]
async fn tc022() {
    let input = r###"{
  "@context": {
    "@vocab": "ex:",
    "Type": {
      "@context": {
        "foo": {
          "@id": "ex:foo",
          "@type": "@vocab",
          "@context": {
            "@version": 1.1,
            "Foo": "ex:Foo",
            "Bar": "ex:Bar"
          }
        }
      }
    }
  },
  "@type": "Type",
  "foo": "Bar"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/c022-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@type": ["ex:Type"],
  "ex:foo": [{"@id": "ex:Bar"}]
}]
"###).expect("Invalid output JSON"));}

// composed type-scoped property-scoped contexts including @type:@vocab
//
// composed type-scoped property-scoped contexts including @type:@vocab
#[tokio::test]
async fn tc023() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "Outer": {
      "@id": "ex:Outer",
      "@context": {
        "nested": "ex:nested"
      }
    },
    "Inner": {
      "@id": "ex:Inner",
      "@context": {
        "@version": 1.1,
        "foo": {
          "@id": "ex:foo",
          "@type": "@vocab",
          "@context": {
            "Foo": "ex:Foo"
          }
        }
      }
    }
  },
  "@type": "Outer",
  "nested": {
    "@type": "Inner",
    "foo": "Foo"
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/c023-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@type": ["ex:Outer"],
  "ex:nested": [{
    "@type": ["ex:Inner"],
    "ex:foo": [{"@id": "ex:Foo"}]
  }]
}]
"###).expect("Invalid output JSON"));}

// type-scoped + property-scoped + values evaluates against previous context
//
// type-scoped + property-scoped + values evaluates against previous context
#[tokio::test]
async fn tc024() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "Outer": {
      "@id": "ex:Outer",
      "@context": {
        "nested": "ex:nested"
      }
    },
    "Inner": {
      "@id": "ex:Inner",
      "@context": {
        "@version": 1.1,
        "foo": {
          "@id": "ex:foo",
          "@container": "@set",
          "@type": "ex:Number",
          "@context": {
            "value": "@value"
          }
        },
        "bar": {
          "@id": "ex:bar",
          "@container": "@set",
          "@type": "@id",
          "@context": {
            "@base": "http://example/"
          }
        }
      }
    }
  },
  "@type": "Outer",
  "nested": {
    "@type": "Inner",
    "foo": [{"value": "1"}, "2"],
    "bar": [{"@id": "a"}, "b"]
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/c024-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@type": ["ex:Outer"],
  "ex:nested": [{
    "@type": ["ex:Inner"],
    "ex:foo": [
      {"@value": "1"},
      {"@type": "ex:Number", "@value": "2"}
    ],
    "ex:bar": [
      {"@id": "http://example/a"},
      {"@id": "http://example/b"}
    ]
  }]
}]
"###).expect("Invalid output JSON"));}

// type-scoped + graph container
//
// type-scoped + graph container
#[tokio::test]
async fn tc025() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "type": "@type",
    "Outer": {
      "@id": "ex:Outer",
      "@context": {
        "nested": {
          "@id": "ex:nested",
          "@type": "@id",
          "@container": "@graph"
        }
      }
    },
    "Inner": {
      "@id": "ex:Inner",
      "@context": {
        "foo": "ex:foo"
      }
    }
  },
  "type": "Outer",
  "nested": {
    "type": "Inner",
    "foo": "bar"
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/c025-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@type": ["ex:Outer"],
  "ex:nested": [{
    "@graph": [{
      "@type": ["ex:Inner"],
      "ex:foo": [{"@value": "bar"}]
    }]
  }]
}]
"###).expect("Invalid output JSON"));}

// @propagate: true on type-scoped context
//
// type-scoped context with @propagate: true survive node-objects
#[tokio::test]
async fn tc026() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@vocab": "http://example/",
    "Foo": {
      "@context": {
        "@propagate": true,
        "baz": {"@type": "@vocab"}
      }
    }
  },
  "@type": "Foo",
  "bar": {"baz": "buzz"}
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/c026-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@type": ["http://example/Foo"],
    "http://example/bar": [{
      "http://example/baz": [{"@id": "http://example/buzz"}]
    }]
  }
]
"###).expect("Invalid output JSON"));}

// @propagate: false on property-scoped context
//
// property-scoped context with @propagate: false do not survive node-objects
#[tokio::test]
async fn tc027() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@vocab": "http://example/",
    "bar": {
      "@context": {
        "@propagate": false,
        "baz": {"@type": "@id"}
      }
    }
  },
  "bar": {"baz": {"baz": "buzz"}}
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/c027-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
 "http://example/bar": [{
   "http://example/baz": [{
     "http://example/baz": [{"@value": "buzz"}]
   }]
 }]
}]"###).expect("Invalid output JSON"));}

// @propagate: false on embedded context
//
// embedded context with @propagate: false do not survive node-objects
#[tokio::test]
async fn tc028() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@vocab": "http://example/"
  },
  "bar": {
    "@context": {
      "@propagate": false,
      "baz": {"@type": "@vocab"}
    },
    "baz": {
      "baz": "buzz"
    }
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/c028-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
 "http://example/bar": [{
   "http://example/baz": [{
     "http://example/baz": [{"@value": "buzz"}]
   }]
 }]
}]"###).expect("Invalid output JSON"));}

// @propagate is invalid in 1.0
//
// @propagate is invalid in 1.0
#[tokio::test]
async fn tc029() {
    let input = r###"{
  "@context": {
    "@propagate": true
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/c029-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid context entry");}

// @propagate must be boolean valued
//
// @propagate must be boolean valued
#[tokio::test]
async fn tc030() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@propagate": "not boolean"
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/c030-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid @propagate value");}

// @context resolutions respects relative URLs.
//
// URL resolution follows RFC3986
#[tokio::test]
async fn tc031() {
    let input = r###"{
  "@context": [
    {"@base": "http://example.org/a/"},
    "c031/c031-context.jsonld"
  ],
  "outer": {
    "inner": "ab"
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/c031-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "http://a.example/out": [{
      "http://a.example/in": [{
        "@value":"ab"
      }]
    }]
  }
]
"###).expect("Invalid output JSON"));}

// Unused embedded context with error.
//
// An embedded context which is never used should still be checked.
#[tokio::test]
async fn tc032() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "t1": {
      "@id": "ex:t1",
      "@context": {
        "t2": {
          "@context": {"type": null}
        }
      }
    }
  },
  "t1": "something"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/c032-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid scoped context");}

// Unused context with an embedded context error.
//
// An unused context with an embedded context should still be checked.
#[tokio::test]
async fn tc033() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "t1": {
      "@id": "ex:t1",
      "@context": {
        "t2": {
          "@context": {"type": null}
        }
      }
    }
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/c033-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid scoped context");}

// Remote scoped context.
//
// Scoped contexts may be externally loaded.
#[tokio::test]
async fn tc034() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example/",
    "foo": {"@context": "c034-context.jsonld"}
  },
  "foo": {
    "bar": "baz"
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/c034-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "http://example/foo": [{"http://example.org/bar": [{"@value": "baz"}]}]
  }
]"###).expect("Invalid output JSON"));}

// Term scoping with embedded contexts.
//
// Terms should make use of @vocab relative to the scope in which the term was defined.
#[tokio::test]
async fn tc035() {
    let input = r###"{
  "@context": {
    "@vocab": "http://vocab.org/",
    "prop1": {}
  },
  "@id": "ex:outer",
  "foo": {
    "@context": {
      "@vocab": "http://vocab.override.org/"
    },
    "@id": "ex:inner",
    "prop1": "baz1",
    "prop2": "baz2"
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/c035-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "ex:outer",
    "http://vocab.org/foo": [
      {
        "@id": "ex:inner",
        "http://vocab.org/prop1": [
          {
            "@value": "baz1"
          }
        ],
        "http://vocab.override.org/prop2": [
          {
            "@value": "baz2"
          }
        ]
      }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// Expand string using default and term directions
//
// Strings are coerced to have @direction based on default and term direction.
#[tokio::test]
async fn tdi01() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@direction": "rtl",
    "ex": "http://example.org/vocab#",
    "ex:ltr": { "@direction": "ltr" },
    "ex:none": { "@direction": null }
  },
  "ex:rtl": "rtl",
  "ex:ltr": "ltr",
  "ex:none": "no direction"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/di01-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "http://example.org/vocab#rtl": [{"@value": "rtl", "@direction": "rtl"}],
    "http://example.org/vocab#ltr": [{"@value": "ltr", "@direction": "ltr"}],
    "http://example.org/vocab#none": [{"@value": "no direction"}]
  }
]"###).expect("Invalid output JSON"));}

// Expand string using default and term directions and languages
//
// Strings are coerced to have @direction based on default and term direction.
#[tokio::test]
async fn tdi02() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@language": "en",
    "@direction": "rtl",
    "ex": "http://example.org/vocab#",
    "ex:ltr": { "@direction": "ltr" },
    "ex:none": { "@direction": null },
    "ex:german": { "@language": "de" },
    "ex:nolang": { "@language": null },
    "ex:german_ltr": { "@language": "de", "@direction": "ltr" },
    "ex:nolang_ltr": { "@language": null, "@direction": "ltr" },
    "ex:none_none": { "@language": null, "@direction": null },
    "ex:german_none": { "@language": "de", "@direction": null }
  },
  "ex:rtl": "rtl en",
  "ex:ltr": "ltr en",
  "ex:none": "no direction en",
  "ex:german": "german rtl",
  "ex:nolang": "no language rtl",
  "ex:german_ltr": "german ltr",
  "ex:nolang_ltr": "no language ltr",
  "ex:none_none": "no language or direction",
  "ex:german_none": "german no direction"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/di02-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "http://example.org/vocab#rtl": [{"@value": "rtl en", "@language": "en", "@direction": "rtl"}],
    "http://example.org/vocab#ltr": [{"@value": "ltr en", "@language": "en", "@direction": "ltr"}],
    "http://example.org/vocab#none": [{"@value": "no direction en", "@language": "en"}],
    "http://example.org/vocab#german": [{"@value": "german rtl", "@language": "de", "@direction": "rtl"}],
    "http://example.org/vocab#nolang": [{"@value": "no language rtl", "@direction": "rtl"}],
    "http://example.org/vocab#german_ltr": [{"@value": "german ltr", "@language": "de", "@direction": "ltr"}],
    "http://example.org/vocab#nolang_ltr": [{"@value": "no language ltr", "@direction": "ltr"}],
    "http://example.org/vocab#none_none": [{"@value": "no language or direction"}],
    "http://example.org/vocab#german_none": [{"@value": "german no direction", "@language": "de"}]
  }
]"###).expect("Invalid output JSON"));}

// expand list values with @direction
//
// List values where the term has @direction are used in expansion.
#[tokio::test]
async fn tdi03() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "foo_ltr": {"@id": "http://example.com/foo", "@container": "@list", "@direction": "ltr"},
    "foo_rtl": {"@id": "http://example.com/foo", "@container": "@list", "@direction": "rtl"}
  },
  "foo_ltr": ["en"],
  "foo_rtl": ["ar"]
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/di03-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.com/foo": [
    {"@list": [{"@value": "en", "@direction": "ltr"}]},
    {"@list": [{"@value": "ar", "@direction": "rtl"}]}
  ]
}]"###).expect("Invalid output JSON"));}

// simple language map with term direction
//
// Term selection with language maps and @direction.
#[tokio::test]
async fn tdi04() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@direction": "ltr",
    "vocab": "http://example.com/vocab/",
    "label": {
      "@id": "vocab:label",
      "@container": "@language"
    }
  },
  "@id": "http://example.com/queen",
  "label": {
    "en": "The Queen",
    "de": [ "Die Königin", "Ihre Majestät" ]
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/di04-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.com/queen",
    "http://example.com/vocab/label": [
      {"@value": "Die Königin", "@language": "de", "@direction": "ltr"},
      {"@value": "Ihre Majestät", "@language": "de", "@direction": "ltr"},
      {"@value": "The Queen", "@language": "en", "@direction": "ltr"}
    ]
  }
]"###).expect("Invalid output JSON"));}

// simple language mapwith overriding term direction
//
// Term selection with language maps and @direction.
#[tokio::test]
async fn tdi05() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "vocab": "http://example.com/vocab/",
    "label": {
      "@id": "vocab:label",
      "@direction": "ltr",
      "@container": "@language"
    }
  },
  "@id": "http://example.com/queen",
  "label": {
    "en": "The Queen",
    "de": [ "Die Königin", "Ihre Majestät" ]
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/di05-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.com/queen",
    "http://example.com/vocab/label": [
      {"@value": "Die Königin", "@language": "de", "@direction": "ltr"},
      {"@value": "Ihre Majestät", "@language": "de", "@direction": "ltr"},
      {"@value": "The Queen", "@language": "en", "@direction": "ltr"}
    ]
  }
]"###).expect("Invalid output JSON"));}

// simple language mapwith overriding null direction
//
// Term selection with language maps and @direction.
#[tokio::test]
async fn tdi06() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "vocab": "http://example.com/vocab/",
    "@direction": "rtl",
    "label": {
      "@id": "vocab:label",
      "@direction": "ltr",
      "@container": "@language"
    }
  },
  "@id": "http://example.com/queen",
  "label": {
    "en": "The Queen",
    "de": [ "Die Königin", "Ihre Majestät" ]
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/di06-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.com/queen",
    "http://example.com/vocab/label": [
      {"@value": "Die Königin", "@language": "de", "@direction": "ltr"},
      {"@value": "Ihre Majestät", "@language": "de", "@direction": "ltr"},
      {"@value": "The Queen", "@language": "en", "@direction": "ltr"}
    ]
  }
]"###).expect("Invalid output JSON"));}

// simple language map with mismatching term direction
//
// Term selection with language maps and @direction.
#[tokio::test]
async fn tdi07() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "vocab": "http://example.com/vocab/",
    "@direction": "rtl",
    "label": {
      "@id": "vocab:label",
      "@direction": null,
      "@container": "@language"
    }
  },
  "@id": "http://example.com/queen",
  "label": {
    "en": "The Queen",
    "de": [ "Die Königin", "Ihre Majestät" ]
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/di07-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.com/queen",
    "http://example.com/vocab/label": [
      {"@value": "Die Königin", "@language": "de"},
      {"@value": "Ihre Majestät", "@language": "de"},
      {"@value": "The Queen", "@language": "en"}
    ]
  }
]"###).expect("Invalid output JSON"));}

// @direction must be one of ltr or rtl
//
// Generate an error if @direction has illegal value.
#[tokio::test]
async fn tdi08() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@direction": "foo"
  },
  "vocab:term": "bar"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/di08-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid base direction");}

// @direction is incompatible with @type
//
// Value objects can have either @type but not @language or @direction.
#[tokio::test]
async fn tdi09() {
    let input = r###"{
  "ex:p": {
    "@value": "v",
    "@type": "ex:t",
    "@direction": "rtl"
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/di09-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid value object");}

// Invalid keyword in term definition
//
// Verifies that an exception is raised on expansion when a invalid term definition is found
#[tokio::test]
async fn tec01() {
    let input = r###"{
  "@context": {
    "term": {"@id": "http://example/", "@index": true}
  },
  "@id": "http://example/test#example"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/ec01-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid term definition");}

// Term definition on @type with empty map
//
// Verifies that an exception is raised if @type is defined as a term with an empty map
#[tokio::test]
async fn tec02() {
    let input = r###"{
  "@context": {
    "@type": {}
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/ec02-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "keyword redefinition");}

// Invalid container mapping
//
// Verifies that an exception is raised on expansion when a invalid container mapping is found
#[tokio::test]
async fn tem01() {
    let input = r###"{
  "@context": {
    "term": {"@id": "http://example/term", "@container": "@context"}
  },
  "@id": "http://example/test#example"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/em01-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid container mapping");}

// @nest MUST NOT have a string value
//
// container: @nest
#[tokio::test]
async fn ten01() {
    let input = r###"{
  "@context": {"@vocab": "http://example.org/"},
  "@nest": "This should generate an error"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/en01-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid @nest value");}

// @nest MUST NOT have a boolen value
//
// Transparent Nesting
#[tokio::test]
async fn ten02() {
    let input = r###"{
  "@context": {"@vocab": "http://example.org/"},
  "@nest": true
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/en02-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid @nest value");}

// @nest MUST NOT have a numeric value
//
// Transparent Nesting
#[tokio::test]
async fn ten03() {
    let input = r###"{
  "@context": {"@vocab": "http://example.org/"},
  "@nest": 1
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/en03-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid @nest value");}

// @nest MUST NOT have a value object value
//
// Transparent Nesting
#[tokio::test]
async fn ten04() {
    let input = r###"{
  "@context": {"@vocab": "http://example.org/"},
  "@nest": {"@value": "This should generate an error"}
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/en04-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid @nest value");}

// does not allow a keyword other than @nest for the value of @nest
//
// Transparent Nesting
#[tokio::test]
async fn ten05() {
    let input = r###"{
  "@context": {
    "term": {"@id": "http://example/term", "@nest": "@id"}
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/en05-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid @nest value");}

// does not allow @nest with @reverse
//
// Transparent Nesting
#[tokio::test]
async fn ten06() {
    let input = r###"{
  "@context": {
    "term": {"@reverse": "http://example/term", "@nest": "@nest"}
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/en06-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid reverse property");}

// processingMode json-ld-1.0 conflicts with @version: 1.1
//
// If processingMode is explicitly json-ld-1.0, it will conflict with 1.1 features.
#[tokio::test]
async fn tep02() {
    let input = r###"{
  "@context": {
    "@version": 1.1
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/ep02-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "processing mode conflict");}

// @version must be 1.1
//
// If @version is specified, it must be 1.1
#[tokio::test]
async fn tep03() {
    let input = r###"{
  "@context": {
    "@version": 1.0
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/ep03-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid @version value");}

// Keywords cannot be aliased to other keywords
//
// Verifies that an exception is raised on expansion when processing an invalid context aliasing a keyword to another keyword
#[tokio::test]
async fn ter01() {
    let input = r###"{
  "@context": {
    "@type": "@id"
  },
  "@type": "http://example.org/type"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er01-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "keyword redefinition");}

// A context may not include itself recursively (direct)
//
// Verifies that an exception is raised on expansion when processing a context referencing itself
#[tokio::test]
async fn ter02() {
    let input = r###"{
  "@context": "er02-in.jsonld",
  "@id": "http://example/test#example"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er02-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "recursive context inclusion");}

// A context may not include itself recursively (indirect)
//
// Verifies that an exception is raised on expansion when processing a context referencing itself indirectly
#[tokio::test]
async fn ter03() {
    let input = r###"{
  "@context": "er03-in.jsonld",
  "@id": "http://example/test#example"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er03-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "recursive context inclusion");}

// Error dereferencing a remote context
//
// Verifies that an exception is raised on expansion when a context dereference results in an error
#[tokio::test]
async fn ter04() {
    let input = r###"{
  "@context": "tag:non-dereferencable-iri",
  "@id": "http://example/test#example"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er04-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "loading remote context failed");}

// Invalid remote context
//
// Verifies that an exception is raised on expansion when a remote context is not an object containing @context
#[tokio::test]
async fn ter05() {
    let input = r###"[{
  "@context": "er05-in.jsonld",
  "@id": "http://example/test#example"
}]
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er05-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid remote context");}

// Invalid local context
//
// Verifies that an exception is raised on expansion when a context is not a string or object
#[tokio::test]
async fn ter06() {
    let input = r###"{
  "@context": true,
  "@id": "http://example/test#example"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er06-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid local context");}

// Invalid base IRI
//
// Verifies that an exception is raised on expansion when a context contains an invalid @base
#[tokio::test]
async fn ter07() {
    let input = r###"{
  "@context": {"@base": true},
  "@id": "http://example/test#example"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er07-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid base IRI");}

// Invalid vocab mapping
//
// Verifies that an exception is raised on expansion when a context contains an invalid @vocab mapping
#[tokio::test]
async fn ter08() {
    let input = r###"{
  "@context": {"@vocab": true},
  "@id": "http://example/test#example"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er08-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid vocab mapping");}

// Invalid default language
//
// Verifies that an exception is raised on expansion when a context contains an invalid @language
#[tokio::test]
async fn ter09() {
    let input = r###"{
  "@context": {"@language": true},
  "@id": "http://example/test#example"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er09-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid default language");}

// Cyclic IRI mapping
//
// Verifies that an exception is raised on expansion when a cyclic IRI mapping is found
#[tokio::test]
async fn ter10() {
    let input = r###"{
  "@context": {
    "term": {"@id": "term:term"}
  },
  "@id": "http://example/test#example"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er10-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "cyclic IRI mapping");}

// Invalid term definition
//
// Verifies that an exception is raised on expansion when a invalid term definition is found
#[tokio::test]
async fn ter11() {
    let input = r###"{
  "@context": {
    "term": true
  },
  "@id": "http://example/test#example"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er11-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid term definition");}

// Invalid type mapping (not a string)
//
// Verifies that an exception is raised on expansion when a invalid type mapping is found
#[tokio::test]
async fn ter12() {
    let input = r###"{
  "@context": {
    "term": {"@id": "http://example/term", "@type": true}
  },
  "@id": "http://example/test#example"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er12-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid type mapping");}

// Invalid type mapping (not absolute IRI)
//
// Verifies that an exception is raised on expansion when a invalid type mapping is found
#[tokio::test]
async fn ter13() {
    let input = r###"{
  "@context": {
    "term": {"@id": "http://example/term", "@type": "_:not-an-iri"}
  },
  "@id": "http://example/test#example"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er13-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid type mapping");}

// Invalid reverse property (contains @id)
//
// Verifies that an exception is raised on expansion when a invalid reverse property is found
#[tokio::test]
async fn ter14() {
    let input = r###"{
  "@context": {
    "term": {"@id": "http://example/term", "@reverse": "http://example/reverse"}
  },
  "@id": "http://example/test#example"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er14-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid reverse property");}

// Invalid IRI mapping (@reverse not a string)
//
// Verifies that an exception is raised on expansion when a invalid IRI mapping is found
#[tokio::test]
async fn ter15() {
    let input = r###"{
  "@context": {
    "term": {"@reverse": true}
  },
  "@id": "http://example/test#example"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er15-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid IRI mapping");}

// Invalid reverse property (invalid @container)
//
// Verifies that an exception is raised on expansion when a invalid reverse property is found
#[tokio::test]
async fn ter17() {
    let input = r###"{
  "@context": {
    "term": {"@reverse": "http://example/reverse", "@container": "@list"}
  },
  "@id": "http://example/test#example"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er17-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid reverse property");}

// Invalid IRI mapping (@id not a string)
//
// Verifies that an exception is raised on expansion when a invalid IRI mapping is found
#[tokio::test]
async fn ter18() {
    let input = r###"{
  "@context": {
    "term": {"@id": true}
  },
  "@id": "http://example/test#example"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er18-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid IRI mapping");}

// Invalid keyword alias (@context)
//
// Verifies that an exception is raised on expansion when a invalid keyword alias is found
#[tokio::test]
async fn ter19() {
    let input = r###"{
  "@context": {
    "term": {"@id": "@context"}
  },
  "@id": "http://example/test#example"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er19-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid keyword alias");}

// Invalid IRI mapping (no vocab mapping)
//
// Verifies that an exception is raised on expansion when a invalid IRI mapping is found
#[tokio::test]
async fn ter20() {
    let input = r###"{
  "@context": {
    "term": {"@container": "@set"}
  },
  "@id": "http://example/test#example"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er20-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid IRI mapping");}

// Invalid container mapping
//
// Verifies that an exception is raised on expansion when a invalid container mapping is found
#[tokio::test]
async fn ter21() {
    let input = r###"{
  "@context": {
    "term": {"@id": "http://example/term", "@container": "@id"}
  },
  "@id": "http://example/test#example"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er21-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid container mapping");}

// Invalid language mapping
//
// Verifies that an exception is raised on expansion when a invalid language mapping is found
#[tokio::test]
async fn ter22() {
    let input = r###"{
  "@context": {
    "term": {"@id": "http://example/term", "@language": true}
  },
  "@id": "http://example/test#example"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er22-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid language mapping");}

// Invalid IRI mapping (relative IRI in @type)
//
// Verifies that an exception is raised on expansion when a invalid type mapping is found
#[tokio::test]
async fn ter23() {
    let input = r###"{
  "@context": {
    "term": {"@id": "http://example/term", "@type": "relative/iri"}
  },
  "@id": "http://example/test#example"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er23-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid type mapping");}

// List of lists (from array)
//
// Verifies that an exception is raised in Expansion when a list of lists is found
#[tokio::test]
async fn ter24() {
    let input = r###"{
  "@context": {"foo": {"@id": "http://example.com/foo", "@container": "@list"}},
  "foo": [{"@list": ["baz"]}]
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er24-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "list of lists");}

// Invalid reverse property map
//
// Verifies that an exception is raised in Expansion when a invalid reverse property map is found
#[tokio::test]
async fn ter25() {
    let input = r###"{
  "@id": "http://example/foo",
  "@reverse": {
    "@id": "http://example/bar"
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er25-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid reverse property map");}

// Colliding keywords
//
// Verifies that an exception is raised in Expansion when colliding keywords are found
#[tokio::test]
async fn ter26() {
    let input = r###"{
  "@context": {
    "id": "@id",
    "ID": "@id"
  },
  "id": "http://example/foo",
  "ID": "http://example/bar"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er26-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "colliding keywords");}

// Invalid @id value
//
// Verifies that an exception is raised in Expansion when an invalid @id value is found
#[tokio::test]
async fn ter27() {
    let input = r###"{
  "@id": true
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er27-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid @id value");}

// Invalid type value
//
// Verifies that an exception is raised in Expansion when an invalid type value is found
#[tokio::test]
async fn ter28() {
    let input = r###"{
  "@type": true
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er28-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid type value");}

// Invalid value object value
//
// Verifies that an exception is raised in Expansion when an invalid value object value is found
#[tokio::test]
async fn ter29() {
    let input = r###"{
  "http://example/prop": {"@value": ["foo"]}
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er29-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid value object value");}

// Invalid language-tagged string
//
// Verifies that an exception is raised in Expansion when an invalid language-tagged string value is found
#[tokio::test]
async fn ter30() {
    let input = r###"{
  "http://example/prop": {"@value": "foo", "@language": true}
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er30-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid language-tagged string");}

// Invalid @index value
//
// Verifies that an exception is raised in Expansion when an invalid @index value value is found
#[tokio::test]
async fn ter31() {
    let input = r###"{
  "http://example.com/vocab/indexMap": {
    "@value": "simple string",
    "@language": "en",
    "@index": true
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er31-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid @index value");}

// List of lists (from array)
//
// Verifies that an exception is raised in Expansion when a list of lists is found
#[tokio::test]
async fn ter32() {
    let input = r###"{
  "http://example.com/foo": {"@list": [{"@list": ["baz"]}]}
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er32-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "list of lists");}

// Invalid @reverse value
//
// Verifies that an exception is raised in Expansion when an invalid @reverse value is found
#[tokio::test]
async fn ter33() {
    let input = r###"{
  "http://example/prop": {
    "@reverse": true
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er33-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid @reverse value");}

// Invalid reverse property value (in @reverse)
//
// Verifies that an exception is raised in Expansion when an invalid reverse property value is found
#[tokio::test]
async fn ter34() {
    let input = r###"{
  "@context": {
    "name": "http://xmlns.com/foaf/0.1/name"
  },
  "@id": "http://example.com/people/markus",
  "name": "Markus Lanthaler",
  "@reverse": {
    "http://xmlns.com/foaf/0.1/knows": "Dave Longley"
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er34-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid reverse property value");}

// Invalid language map value
//
// Verifies that an exception is raised in Expansion when an invalid language map value is found
#[tokio::test]
async fn ter35() {
    let input = r###"{
  "@context": {
    "vocab": "http://example.com/vocab/",
    "label": {
      "@id": "vocab:label",
      "@container": "@language"
    }
  },
  "@id": "http://example.com/queen",
  "label": {
    "en": true
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er35-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid language map value");}

// Invalid reverse property value (through coercion)
//
// Verifies that an exception is raised in Expansion when an invalid reverse property value is found
#[tokio::test]
async fn ter36() {
    let input = r###"{
  "@context": {
    "term": {"@reverse": "http://example/reverse"}
  },
  "@id": "http://example/foo",
  "term": {"@list": ["http://example/bar"]}
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er36-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid reverse property value");}

// Invalid value object (unexpected keyword)
//
// Verifies that an exception is raised in Expansion when an invalid value object is found
#[tokio::test]
async fn ter37() {
    let input = r###"{
  "http://example/foo": {"@value": "bar", "@id": "http://example/baz"}
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er37-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid value object");}

// Invalid value object (@type and @language)
//
// Verifies that an exception is raised in Expansion when an invalid value object is found
#[tokio::test]
async fn ter38() {
    let input = r###"{
  "http://example/foo": {"@value": "bar", "@language": "en", "@type": "http://example/type"}
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er38-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid value object");}

// Invalid language-tagged value
//
// Verifies that an exception is raised in Expansion when an invalid language-tagged value is found
#[tokio::test]
async fn ter39() {
    let input = r###"{
  "http://example/foo": {"@value": true, "@language": "en"}
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er39-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid language-tagged value");}

// Invalid typed value
//
// Verifies that an exception is raised in Expansion when an invalid typed value is found
#[tokio::test]
async fn ter40() {
    let input = r###"{
  "http://example/foo": {"@value": "bar", "@type": "_:dt"}
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er40-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid typed value");}

// Invalid set or list object
//
// Verifies that an exception is raised in Expansion when an invalid set or list object is found
#[tokio::test]
async fn ter41() {
    let input = r###"{
  "http://example/prop": {"@list": ["foo"], "@id": "http://example/bar"}
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er41-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid set or list object");}

// Keywords may not be redefined in 1.0
//
// Verifies that an exception is raised on expansion when processing an invalid context attempting to define @container on a keyword
#[tokio::test]
async fn ter42() {
    let input = r###"{
  "@context": {
    "@type": {"@container": "@set"}
  },
  "@type": "http://example.org/type"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er42-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "keyword redefinition");}

// Term definition with @id: @type
//
// Expanding term mapping to @type uses @type syntax now illegal
#[tokio::test]
async fn ter43() {
    let input = r###"{
  "@context": {
    "http://www.w3.org/1999/02/22-rdf-syntax-ns#type": {"@id": "@type", "@type": "@id"}
  },
  "@graph": [
    {
      "@id": "http://example.com/a",
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type": "http://example.com/b"
    }, {
      "@id": "http://example.com/c",
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type": [
        "http://example.com/d",
        "http://example.com/e"
      ]
    }, {
      "@id": "http://example.com/f",
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type": "http://example.com/g"
    }
  ]
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er43-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid IRI mapping");}

// Redefine terms looking like compact IRIs
//
// Term definitions may look like compact IRIs, but must be consistent.
#[tokio::test]
async fn ter44() {
    let input = r###"{
  "@context": [
    {
      "v": "http://example.com/vocab#",
      "v:term": "v:somethingElse",
      "v:termId": { "@id": "v:somethingElseId" }
    }
  ],
  "v:term": "value of v:term",
  "v:termId": "value of v:termId"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er44-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid IRI mapping");}

// Invalid term as relative IRI
//
// Verifies that a relative IRI cannot be used as a term.
#[tokio::test]
async fn ter48() {
    let input = r###"{
  "@context": {
    "./something": "http://example.com/vocab#somethingElse"
  },
  "./something": "something"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er48-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid IRI mapping");}

// A relative IRI cannot be used as a prefix
//
// Verifies that a relative IRI cannot be used as a term.
#[tokio::test]
async fn ter49() {
    let input = r###"{
  "@context": {
    "@vocab": "http:/example.org",
    "./something": {"@type": "@id", "@prefix": true}
  },
  "./something": "something"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er49-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid term definition");}

// Invalid reverse id
//
// Verifies that an exception is raised in Expansion when an invalid IRI is used for @reverse.
#[tokio::test]
async fn ter50() {
    let input = r###"{
  "@context": {
    "rev": {"@reverse": "not an IRI"}
  },
  "@id": "http://example.org/foo",
  "rev": {"@id": "http://example.org/bar"}
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er50-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid IRI mapping");}

// Invalid value object value using a value alias
//
// Verifies that an exception is raised in Expansion when an invalid value object value is found using a value alias
#[tokio::test]
async fn ter51() {
    let input = r###"{
  "@context": {"value": "@value"},
  "http://example/prop": {"value": ["foo"]}
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er51-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid value object value");}

// Definition for the empty term
//
// Verifies that an exception is raised on expansion when a context contains a definition for the empty term
#[tokio::test]
async fn ter52() {
    let input = r###"{
  "@context": {
    "term": {"@id": "http://example.org/term"},
    "": {"@id": "http://example.org/empty"}
  },
  "@id": "http://example/test#example"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er52-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid term definition");}

// Invalid prefix value
//
// Verifies that an exception is raised on expansion when a context contains an invalid @prefix value
#[tokio::test]
async fn ter53() {
    let input = r###"{
  "@context": {
    "term": {"@id": "http://example.org/term", "@prefix": 10}
  },
  "@id": "http://example/test#example"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/er53-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid @prefix value");}

// Using an array value for @context is illegal in JSON-LD 1.0
//
// Verifies that an exception is raised on expansion when a invalid container mapping is found
#[tokio::test]
async fn tes01() {
    let input = r###"{
  "@context": {
    "term": {"@id": "http://example/term", "@container": ["@set"]}
  },
  "@id": "http://example/test#example",
  "term": "foo"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/es01-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid container mapping");}

// Mapping @container: [@list, @set] is invalid
//
// Testing legal combinations of @set with other container values
#[tokio::test]
async fn tes02() {
    let input = r###"{
  "@context": {
    "term": {"@id": "http://example/term", "@container": ["@list", "@set"]}
  },
  "@id": "http://example/test#example",
  "term": "foo"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/es02-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid container mapping");}

// Basic Included array
//
// Tests included blocks.
#[tokio::test]
async fn tin01() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@vocab": "http://example.org/"
  },
  "prop": "value",
  "@included": [{
    "prop": "value2"
  }]
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/in01-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/prop": [{"@value": "value"}],
  "@included": [{
    "http://example.org/prop": [{"@value": "value2"}]
  }]
}]"###).expect("Invalid output JSON"));}

// Basic Included object
//
// Tests included blocks.
#[tokio::test]
async fn tin02() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@vocab": "http://example.org/"
  },
  "prop": "value",
  "@included": {
    "prop": "value2"
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/in02-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/prop": [{"@value": "value"}],
  "@included": [{
    "http://example.org/prop": [{"@value": "value2"}]
  }]
}]"###).expect("Invalid output JSON"));}

// Multiple properties mapping to @included are folded together
//
// Tests included blocks.
#[tokio::test]
async fn tin03() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@vocab": "http://example.org/",
    "included1": "@included",
    "included2": "@included"
  },
  "included1": {"prop": "value1"},
  "included2": {"prop": "value2"}
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/in03-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@included": [
    {"http://example.org/prop": [{"@value": "value1"}]},
    {"http://example.org/prop": [{"@value": "value2"}]}
  ]
}]"###).expect("Invalid output JSON"));}

// Included containing @included
//
// Tests included blocks.
#[tokio::test]
async fn tin04() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@vocab": "http://example.org/"
  },
  "prop": "value",
  "@included": {
    "prop": "value2",
    "@included": {
      "prop": "value3"
    }
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/in04-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/prop": [{"@value": "value"}],
  "@included": [{
    "http://example.org/prop": [{"@value": "value2"}],
    "@included": [{
      "http://example.org/prop": [{"@value": "value3"}]
    }]
  }]
}]"###).expect("Invalid output JSON"));}

// Property value with @included
//
// Tests included blocks.
#[tokio::test]
async fn tin05() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@vocab": "http://example.org/"
  },
  "prop": {
    "@type": "Foo",
    "@included": {
      "@type": "Bar"
    }
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/in05-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/prop": [{
    "@type": ["http://example.org/Foo"],
    "@included": [{
      "@type": ["http://example.org/Bar"]
    }]
  }]
}]"###).expect("Invalid output JSON"));}

// json.api example
//
// Tests included blocks.
#[tokio::test]
async fn tin06() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@vocab": "http://example.org/vocab#",
    "@base": "http://example.org/base/",
    "id": "@id",
    "type": "@type",
    "data": "@nest",
    "attributes": "@nest",
    "links": "@nest",
    "relationships": "@nest",
    "included": "@included",
    "self": {"@type": "@id"},
    "related": {"@type": "@id"},
    "comments": {
      "@context": {
        "data": null
      }
    }
  },
  "data": [{
    "type": "articles",
    "id": "1",
    "attributes": {
      "title": "JSON:API paints my bikeshed!"
    },
    "links": {
      "self": "http://example.com/articles/1"
    },
    "relationships": {
      "author": {
        "links": {
          "self": "http://example.com/articles/1/relationships/author",
          "related": "http://example.com/articles/1/author"
        },
        "data": { "type": "people", "id": "9" }
      },
      "comments": {
        "links": {
          "self": "http://example.com/articles/1/relationships/comments",
          "related": "http://example.com/articles/1/comments"
        },
        "data": [
          { "type": "comments", "id": "5" },
          { "type": "comments", "id": "12" }
        ]
      }
    }
  }],
  "included": [{
    "type": "people",
    "id": "9",
    "attributes": {
      "first-name": "Dan",
      "last-name": "Gebhardt",
      "twitter": "dgeb"
    },
    "links": {
      "self": "http://example.com/people/9"
    }
  }, {
    "type": "comments",
    "id": "5",
    "attributes": {
      "body": "First!"
    },
    "relationships": {
      "author": {
        "data": { "type": "people", "id": "2" }
      }
    },
    "links": {
      "self": "http://example.com/comments/5"
    }
  }, {
    "type": "comments",
    "id": "12",
    "attributes": {
      "body": "I like XML better"
    },
    "relationships": {
      "author": {
        "data": { "type": "people", "id": "9" }
      }
    },
    "links": {
      "self": "http://example.com/comments/12"
    }
  }]
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/in06-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@id": "http://example.org/base/1",
  "@type": ["http://example.org/vocab#articles"],
  "http://example.org/vocab#title": [{"@value": "JSON:API paints my bikeshed!"}],
  "http://example.org/vocab#self": [{"@id": "http://example.com/articles/1"}],
  "http://example.org/vocab#author": [{
    "@id": "http://example.org/base/9",
    "@type": ["http://example.org/vocab#people"],
    "http://example.org/vocab#self": [{"@id": "http://example.com/articles/1/relationships/author"}],
    "http://example.org/vocab#related": [{"@id": "http://example.com/articles/1/author"}]
  }],
  "http://example.org/vocab#comments": [{
    "http://example.org/vocab#self": [{"@id": "http://example.com/articles/1/relationships/comments"}],
    "http://example.org/vocab#related": [{"@id": "http://example.com/articles/1/comments"}]
  }],
  "@included": [{
    "@id": "http://example.org/base/9",
    "@type": ["http://example.org/vocab#people"],
    "http://example.org/vocab#first-name": [{"@value": "Dan"}],
    "http://example.org/vocab#last-name": [{"@value": "Gebhardt"}],
    "http://example.org/vocab#twitter": [{"@value": "dgeb"}],
    "http://example.org/vocab#self": [{"@id": "http://example.com/people/9"}]
  }, {
    "@id": "http://example.org/base/5",
    "@type": ["http://example.org/vocab#comments"],
    "http://example.org/vocab#body": [{"@value": "First!"}],
    "http://example.org/vocab#author": [{
      "@id": "http://example.org/base/2",
      "@type": ["http://example.org/vocab#people"]
    }],
    "http://example.org/vocab#self": [{"@id": "http://example.com/comments/5"}]
  }, {
    "@id": "http://example.org/base/12",
    "@type": ["http://example.org/vocab#comments"],
    "http://example.org/vocab#body": [{"@value": "I like XML better"}],
    "http://example.org/vocab#author": [{
      "@id": "http://example.org/base/9",
      "@type": ["http://example.org/vocab#people"]
    }],
    "http://example.org/vocab#self": [{"@id": "http://example.com/comments/12"}]
  }]
}]"###).expect("Invalid output JSON"));}

// Error if @included value is a string
//
// Tests included blocks.
#[tokio::test]
async fn tin07() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@vocab": "http://example.org/"
  },
  "@included": "string"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/in07-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid @included value");}

// Error if @included value is a value object
//
// Tests included blocks.
#[tokio::test]
async fn tin08() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@vocab": "http://example.org/"
  },
  "@included": {"@value": "value"}
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/in08-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid @included value");}

// Error if @included value is a list object
//
// Tests included blocks.
#[tokio::test]
async fn tin09() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@vocab": "http://example.org/"
  },
  "@included": {"@list": ["value"]}
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/in09-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid @included value");}

// Expand JSON literal (boolean true)
//
// Tests expanding property with @type @json to a JSON literal (boolean true).
#[tokio::test]
async fn tjs01() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "e": {"@id": "http://example.org/vocab#bool", "@type": "@json"}
  },
  "e": true
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/js01-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/vocab#bool": [{"@value": true, "@type": "@json"}]
}]"###).expect("Invalid output JSON"));}

// Expand JSON literal (boolean false)
//
// Tests expanding property with @type @json to a JSON literal (boolean false).
#[tokio::test]
async fn tjs02() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "e": {"@id": "http://example.org/vocab#bool", "@type": "@json"}
  },
  "e": false
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/js02-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/vocab#bool": [{"@value": false, "@type": "@json"}]
}]"###).expect("Invalid output JSON"));}

// Expand JSON literal (double)
//
// Tests expanding property with @type @json to a JSON literal (double).
#[tokio::test]
async fn tjs03() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "e": {"@id": "http://example.org/vocab#double", "@type": "@json"}
  },
  "e": 1.23
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/js03-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/vocab#double": [{"@value": 1.23, "@type": "@json"}]
}]"###).expect("Invalid output JSON"));}

// Expand JSON literal (double-zero)
//
// Tests expanding property with @type @json to a JSON literal (double-zero).
#[tokio::test]
async fn tjs04() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "e": {"@id": "http://example.org/vocab#double", "@type": "@json"}
  },
  "e": 0.0e0
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/js04-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/vocab#double": [{"@value": 0.0e0, "@type": "@json"}]
}]"###).expect("Invalid output JSON"));}

// Expand JSON literal (integer)
//
// Tests expanding property with @type @json to a JSON literal (integer).
#[tokio::test]
async fn tjs05() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "e": {"@id": "http://example.org/vocab#integer", "@type": "@json"}
  },
  "e": 123
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/js05-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/vocab#integer": [{"@value": 123, "@type": "@json"}]
}]"###).expect("Invalid output JSON"));}

// Expand JSON literal (object)
//
// Tests expanding property with @type @json to a JSON literal (object).
#[tokio::test]
async fn tjs06() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "e": {"@id": "http://example.org/vocab#object", "@type": "@json"}
  },
  "e": {"foo": "bar"}
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/js06-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/vocab#object": [{"@value": {"foo": "bar"}, "@type": "@json"}]
}]"###).expect("Invalid output JSON"));}

// Expand JSON literal (array)
//
// Tests expanding property with @type @json to a JSON literal (array).
#[tokio::test]
async fn tjs07() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "e": {"@id": "http://example.org/vocab#array", "@type": "@json"}
  },
  "e": [{"foo": "bar"}]
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/js07-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/vocab#array": [{"@value": [{"foo": "bar"}], "@type": "@json"}]
}]"###).expect("Invalid output JSON"));}

// Expand JSON literal with array canonicalization
//
// Tests expanding JSON literal with array canonicalization.
#[tokio::test]
async fn tjs08() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "e": {"@id": "http://example.org/vocab#c14n", "@type": "@json"}
  },
  "e": [
    56,
    {
      "d": true,
      "10": null,
      "1": [ ]
    }
  ]
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/js08-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "http://example.org/vocab#c14n": [
      {
        "@type": "@json",
        "@value": [
          56,
          {
            "d": true,
            "10": null,
            "1": []
          }
        ]
      }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// Transform JSON literal with string canonicalization
//
// Tests expanding JSON literal with string canonicalization.
#[tokio::test]
async fn tjs09() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "e": {"@id": "http://example.org/vocab#c14n", "@type": "@json"}
  },
  "e": {
    "peach": "This sorting order",
    "péché": "is wrong according to French",
    "pêche": "but canonicalization MUST",
    "sin":   "ignore locale"
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/js09-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "http://example.org/vocab#c14n": [
      {
        "@type": "@json",
        "@value": {
          "peach": "This sorting order",
          "péché": "is wrong according to French",
          "pêche": "but canonicalization MUST",
          "sin":   "ignore locale"
        }
      }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// Expand JSON literal with structural canonicalization
//
// Tests expanding JSON literal with structural canonicalization.
#[tokio::test]
async fn tjs10() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "e": {"@id": "http://example.org/vocab#c14n", "@type": "@json"}
  },
  "e": {
    "1": {"f": {"f": "hi","F": 5} ," ": 56.0},
    "10": { },
    "": "empty",
    "a": { },
    "111": [ {"e": "yes","E": "no" } ],
    "A": { }
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/js10-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "http://example.org/vocab#c14n": [
      {
        "@type": "@json",
        "@value": {
          "1": {"f": {"f": "hi","F": 5} ," ": 56.0},
          "10": { },
          "": "empty",
          "a": { },
          "111": [ {"e": "yes","E": "no" } ],
          "A": { }
        }
      }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// Expand JSON literal with unicode canonicalization
//
// Tests expanding JSON literal with unicode canonicalization.
#[tokio::test]
async fn tjs11() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "e": {"@id": "http://example.org/vocab#c14n", "@type": "@json"}
  },
  "e": {
    "Unnormalized Unicode":"A\u030a"
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/js11-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "http://example.org/vocab#c14n": [
      {
        "@type": "@json",
        "@value": {
          "Unnormalized Unicode":"A\u030a"
        }
      }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// Expand JSON literal with value canonicalization
//
// Tests expanding JSON literal with value canonicalization.
#[tokio::test]
async fn tjs12() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "e": {"@id": "http://example.org/vocab#c14n", "@type": "@json"}
  },
  "e": {
    "numbers": [333333333.33333329, 1E30, 4.50, 2e-3, 0.000000000000000000000000001],
    "string": "\u20ac$\u000F\u000aA'\u0042\u0022\u005c\\\"\/",
    "literals": [null, true, false]
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/js12-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "http://example.org/vocab#c14n": [
      {
        "@type": "@json",
        "@value": {
          "numbers": [333333333.33333329, 1E30, 4.50, 2e-3, 0.000000000000000000000000001],
          "string": "\u20ac$\u000F\u000aA'\u0042\u0022\u005c\\\"\/",
          "literals": [null, true, false]
        }
      }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// Expand JSON literal with wierd canonicalization
//
// Tests expanding JSON literal with wierd canonicalization.
#[tokio::test]
async fn tjs13() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "e": {"@id": "http://example.org/vocab#c14n", "@type": "@json"}
  },
  "e": {
    "\u20ac": "Euro Sign",
    "\r": "Carriage Return",
    "\u000a": "Newline",
    "1": "One",
    "\u0080": "Control\u007f",
    "\ud83d\ude02": "Smiley",
    "\u00f6": "Latin Small Letter O With Diaeresis",
    "</script>": "Browser Challenge"
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/js13-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "http://example.org/vocab#c14n": [
      {
        "@type": "@json",
        "@value": {
          "\u20ac": "Euro Sign",
          "\r": "Carriage Return",
          "\u000a": "Newline",
          "1": "One",
          "\u0080": "Control\u007f",
          "\ud83d\ude02": "Smiley",
          "\u00f6": "Latin Small Letter O With Diaeresis",
          "</script>": "Browser Challenge"
        }
      }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// Expand JSON literal without expanding contents
//
// Tests expanding JSON literal does not expand terms inside json.
#[tokio::test]
async fn tjs14() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "e": {"@id": "http://example.org/vocab#array", "@type": "@json"}
  },
  "e": [{"e": "bar"}]
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/js14-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/vocab#array": [{"@value": [{"e": "bar"}], "@type": "@json"}]
}]"###).expect("Invalid output JSON"));}

// Expand JSON literal aleady in expanded form
//
// Tests expanding JSON literal in expanded form.
#[tokio::test]
async fn tjs15() {
    let input = r###"{
  "http://example.org/vocab#object": [{"@value": {"foo": "bar"}, "@type": "@json"}]
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/js15-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/vocab#object": [{"@value": {"foo": "bar"}, "@type": "@json"}]
}]"###).expect("Invalid output JSON"));}

// Expand JSON literal aleady in expanded form with aliased keys
//
// Tests expanding JSON literal in expanded form with aliased keys in value object.
#[tokio::test]
async fn tjs16() {
    let input = r###"{
  "@context": {"@version": 1.1, "value": "@value", "type": "@type", "json": "@json"},
  "http://example.org/vocab#object": [{"value": {"foo": "bar"}, "type": "json"}]
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/js16-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/vocab#object": [{"@value": {"foo": "bar"}, "@type": "@json"}]
}]"###).expect("Invalid output JSON"));}

// Expand JSON literal (string)
//
// Tests expanding property with @type @json to a JSON literal (string).
#[tokio::test]
async fn tjs17() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "e": {"@id": "http://example.org/vocab#string", "@type": "@json"}
  },
  "e": "string"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/js17-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/vocab#string": [{
    "@value": "string",
    "@type": "@json"
  }]
}]"###).expect("Invalid output JSON"));}

// Expand JSON literal (null)
//
// Tests expanding property with @type @json to a JSON literal (null).
#[tokio::test]
async fn tjs18() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "e": {"@id": "http://example.org/vocab#null", "@type": "@json"}
  },
  "e": null
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/js18-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/vocab#null": [{
    "@value": null,
    "@type": "@json"
  }]
}]"###).expect("Invalid output JSON"));}

// Expand JSON literal with aliased @type
//
// Tests expanding JSON literal with aliased @type.
#[tokio::test]
async fn tjs19() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "type": "@type"
  },
  "ex:foo": {
    "type": "@json",
    "@value": {
      "test": 1
    }
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/js19-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "ex:foo": [{
    "@type": "@json",
    "@value": {
      "test": 1
    }
  }]
}]
"###).expect("Invalid output JSON"));}

// Expand JSON literal with aliased @value
//
// Tests expanding JSON literal with aliased @value.
#[tokio::test]
async fn tjs20() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "value": "@value"
  },
  "ex:foo": {
    "@type": "@json",
    "value": {
      "test": 1
    }
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/js20-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "ex:foo": [{
    "@type": "@json",
    "@value": {
      "test": 1
    }
  }]
}]
"###).expect("Invalid output JSON"));}

// Expand JSON literal with @context
//
// Tests expanding JSON literal with a @context.
#[tokio::test]
async fn tjs21() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "ex:foo": {
      "@type": "@json"
    }
  },
  "ex:foo": {
    "@context": "ex:not:a:context",
    "test": 1
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/js21-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "ex:foo": [{
    "@type": "@json",
    "@value": {
      "@context": "ex:not:a:context",
      "test": 1
    }
  }]
}]
"###).expect("Invalid output JSON"));}

// Expand JSON literal (null) aleady in expanded form.
//
// Tests expanding property with @type @json to a JSON literal (null).
#[tokio::test]
async fn tjs22() {
    let input = r###"{
  "http://example.org/vocab#null": {"@value": null, "@type": "@json"}
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/js22-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/vocab#null": [{
    "@value": null,
    "@type": "@json"
  }]
}]"###).expect("Invalid output JSON"));}

// Expand JSON literal (empty array).
//
// Tests expanding property with @type @json to a JSON literal (empty array).
#[tokio::test]
async fn tjs23() {
    let input = r###"{
  "http://example.org/vocab#null": {"@value": [], "@type": "@json"}
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/js23-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/vocab#null": [{
    "@value": [],
    "@type": "@json"
  }]
}]"###).expect("Invalid output JSON"));}

// Language map with null value
//
// A language map may have a null value, which is ignored
#[tokio::test]
async fn tl001() {
    let input = r###"{
  "@context": {
    "vocab": "http://example.com/vocab/",
    "label": {
      "@id": "vocab:label",
      "@container": "@language"
    }
  },
  "@id": "http://example.com/queen",
  "label": {
    "en": null,
    "de": [ "Die KÃ¶nigin", null ]
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/l001-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.com/queen",
    "http://example.com/vocab/label":
    [
      {
        "@value": "Die KÃ¶nigin",
        "@language": "de"
      }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// @list containing @list
//
// List of lists
#[tokio::test]
async fn tli01() {
    let input = r###"{
  "http://example.com/foo": {"@list": [{"@list": ["baz"]}]}
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/li01-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.com/foo": [{"@list": [{"@list": [{"@value": "baz"}]}]}]
}]"###).expect("Invalid output JSON"));}

// @list containing empty @list
//
// List of lists
#[tokio::test]
async fn tli02() {
    let input = r###"{
  "http://example.com/foo": {"@list": [{"@list": []}]}
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/li02-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.com/foo": [{"@list": [{"@list": []}]}]
}]"###).expect("Invalid output JSON"));}

// @list containing @list (with coercion)
//
// List of lists
#[tokio::test]
async fn tli03() {
    let input = r###"{
  "@context": {"foo": {"@id": "http://example.com/foo", "@container": "@list"}},
  "foo": [{"@list": ["baz"]}]
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/li03-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.com/foo": [{"@list": [{"@list": [{"@value": "baz"}]}]}]
}]"###).expect("Invalid output JSON"));}

// @list containing empty @list (with coercion)
//
// List of lists
#[tokio::test]
async fn tli04() {
    let input = r###"{
  "@context": {"foo": {"@id": "http://example.com/foo", "@container": "@list"}},
  "foo": [{"@list": []}]
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/li04-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.com/foo": [{"@list": [{"@list": []}]}]
}]"###).expect("Invalid output JSON"));}

// coerced @list containing an array
//
// List of lists
#[tokio::test]
async fn tli05() {
    let input = r###"{
  "@context": {"foo": {"@id": "http://example.com/foo", "@container": "@list"}},
  "foo": [["baz"]]
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/li05-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.com/foo": [{"@list": [{"@list": [{"@value": "baz"}]}]}]
}]"###).expect("Invalid output JSON"));}

// coerced @list containing an empty array
//
// List of lists
#[tokio::test]
async fn tli06() {
    let input = r###"{
  "@context": {"foo": {"@id": "http://example.com/foo", "@container": "@list"}},
  "foo": [[]]
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/li06-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.com/foo": [{"@list": [{"@list": []}]}]
}]"###).expect("Invalid output JSON"));}

// coerced @list containing deep arrays
//
// List of lists
#[tokio::test]
async fn tli07() {
    let input = r###"{
  "@context": {"foo": {"@id": "http://example.com/foo", "@container": "@list"}},
  "foo": [[["baz"]]]
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/li07-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.com/foo": [{"@list": [{"@list": [{"@list": [{"@value": "baz"}]}]}]}]
}]"###).expect("Invalid output JSON"));}

// coerced @list containing deep empty arrays
//
// List of lists
#[tokio::test]
async fn tli08() {
    let input = r###"{
  "@context": {"foo": {"@id": "http://example.com/foo", "@container": "@list"}},
  "foo": [[[]]]
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/li08-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.com/foo": [{"@list": [{"@list": [{"@list": []}]}]}]
}]"###).expect("Invalid output JSON"));}

// coerced @list containing multiple lists
//
// List of lists
#[tokio::test]
async fn tli09() {
    let input = r###"{
  "@context": {"foo": {"@id": "http://example.com/foo", "@container": "@list"}},
  "foo": [["a"], ["b"]]
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/li09-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.com/foo": [{"@list": [
    {"@list": [{"@value": "a"}]},
    {"@list": [{"@value": "b"}]}
  ]}]
}]"###).expect("Invalid output JSON"));}

// coerced @list containing mixed list values
//
// List of lists
#[tokio::test]
async fn tli10() {
    let input = r###"{
  "@context": {"foo": {"@id": "http://example.com/foo", "@container": "@list"}},
  "foo": [["a"], "b"]
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/li10-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.com/foo": [{"@list": [
    {"@list": [{"@value": "a"}]},
    {"@value": "b"}
  ]}]
}]"###).expect("Invalid output JSON"));}

// Adds @id to object not having an @id
//
// Expansion using @container: @id
#[tokio::test]
async fn tm001() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example/",
    "idmap": {"@container": "@id"}
  },
  "idmap": {
    "http://example.org/foo": {"label": "Object with @id <foo>"},
    "_:bar": {"label": "Object with @id _:bar"}
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/m001-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example/idmap": [
    {"http://example/label": [{"@value": "Object with @id _:bar"}], "@id": "_:bar"},
    {"http://example/label": [{"@value": "Object with @id <foo>"}], "@id": "http://example.org/foo"}
  ]
}]"###).expect("Invalid output JSON"));}

// Retains @id in object already having an @id
//
// Expansion using @container: @id
#[tokio::test]
async fn tm002() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example/",
    "idmap": {"@container": "@id"}
  },
  "idmap": {
    "http://example.org/foo": {"@id": "http://example.org/bar", "label": "Object with @id <foo>"},
    "_:bar": {"@id": "_:foo", "label": "Object with @id _:bar"}
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/m002-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example/idmap": [
    {"@id": "_:foo", "http://example/label": [{"@value": "Object with @id _:bar"}]},
    {"@id": "http://example.org/bar", "http://example/label": [{"@value": "Object with @id <foo>"}]}
  ]
}]"###).expect("Invalid output JSON"));}

// Adds @type to object not having an @type
//
// Expansion using @container: @type
#[tokio::test]
async fn tm003() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example/",
    "typemap": {"@container": "@type"}
  },
  "typemap": {
    "http://example.org/foo": {"label": "Object with @type <foo>"},
    "_:bar": {"label": "Object with @type _:bar"}
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/m003-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example/typemap": [
    {"http://example/label": [{"@value": "Object with @type _:bar"}], "@type": ["_:bar"]},
    {"http://example/label": [{"@value": "Object with @type <foo>"}], "@type": ["http://example.org/foo"]}
  ]
}]"###).expect("Invalid output JSON"));}

// Prepends @type in object already having an @type
//
// Expansion using @container: @type
#[tokio::test]
async fn tm004() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example/",
    "typemap": {"@container": "@type"}
  },
  "typemap": {
    "http://example.org/foo": {"@type": "http://example.org/bar", "label": "Object with @type <foo>"},
    "_:bar": {"@type": "_:foo", "label": "Object with @type _:bar"}
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/m004-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example/typemap": [
    {
      "@type": ["_:bar", "_:foo"],
      "http://example/label": [{"@value": "Object with @type _:bar"}]
    },
    {
      "@type": ["http://example.org/foo", "http://example.org/bar"],
      "http://example/label": [{"@value": "Object with @type <foo>"}]
    }
  ]
}]"###).expect("Invalid output JSON"));}

// Adds expanded @id to object
//
// Expansion using @container: @id
#[tokio::test]
async fn tm005() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example/",
    "idmap": {"@container": "@id"}
  },
  "idmap": {
    "foo": {"label": "Object with @id <foo>"}
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/m005-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example/idmap": [
    {"http://example/label": [{"@value": "Object with @id <foo>"}], "@id": "http://example.org/foo"}
  ]
}]"###).expect("Invalid output JSON"));}

// Adds vocabulary expanded @type to object
//
// Expansion using @container: @type
#[tokio::test]
async fn tm006() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example/",
    "typemap": {"@container": "@type"}
  },
  "typemap": {
    "Foo": {"label": "Object with @type <foo>"}
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/m006-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example/typemap": [
    {"http://example/label": [{"@value": "Object with @type <foo>"}], "@type": ["http://example/Foo"]}
  ]
}]"###).expect("Invalid output JSON"));}

// Adds document expanded @type to object
//
// Expansion using @container: @type
#[tokio::test]
async fn tm007() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example/",
    "typemap": {"@container": "@type"},
    "label": "http://example/label"
  },
  "typemap": {
    "Foo": {"label": "Object with @type <foo>"}
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/m007-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example/typemap": [
    {"http://example/label": [{"@value": "Object with @type <foo>"}], "@type": ["http://example/Foo"]}
  ]
}]"###).expect("Invalid output JSON"));}

// When type is in a type map
//
// scoped context on @type
#[tokio::test]
async fn tm008() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example/",
    "typemap": {"@container": "@type"},
    "Type": {"@context": {"a": "http://example.org/a"}}
  },
  "typemap": {
    "Type": {"a": "Object with @type <Type>"}
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/m008-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example/typemap": [
    {"http://example.org/a": [{"@value": "Object with @type <Type>"}], "@type": ["http://example/Type"]}
  ]
}]"###).expect("Invalid output JSON"));}

// language map with @none
//
// index on @language
#[tokio::test]
async fn tm009() {
    let input = r###"{
  "@context": {
    "vocab": "http://example.com/vocab/",
    "label": {
      "@id": "vocab:label",
      "@container": "@language"
    }
  },
  "@id": "http://example.com/queen",
  "label": {
    "en": "The Queen",
    "de": [ "Die Königin", "Ihre Majestät" ],
    "@none": "The Queen"
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/m009-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.com/queen",
    "http://example.com/vocab/label": [
      {"@value": "The Queen"},
      {"@value": "Die Königin", "@language": "de"},
      {"@value": "Ihre Majestät", "@language": "de"},
      {"@value": "The Queen", "@language": "en"}
    ]
  }
]"###).expect("Invalid output JSON"));}

// language map with alias of @none
//
// index on @language
#[tokio::test]
async fn tm010() {
    let input = r###"{
  "@context": {
    "vocab": "http://example.com/vocab/",
    "label": {
      "@id": "vocab:label",
      "@container": "@language"
    },
    "none": "@none"
  },
  "@id": "http://example.com/queen",
  "label": {
    "en": "The Queen",
    "de": [ "Die Königin", "Ihre Majestät" ],
    "none": "The Queen"
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/m010-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.com/queen",
    "http://example.com/vocab/label": [
      {"@value": "Die Königin", "@language": "de"},
      {"@value": "Ihre Majestät", "@language": "de"},
      {"@value": "The Queen", "@language": "en"},
      {"@value": "The Queen"}
    ]
  }
]"###).expect("Invalid output JSON"));}

// id map with @none
//
// index on @id
#[tokio::test]
async fn tm011() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example/",
    "idmap": {"@container": "@id"},
    "none": "@none"
  },
  "idmap": {
    "@none": {"label": "Object with no @id"},
    "none": {"label": "Another object with no @id"}
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/m011-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example/idmap": [
    {"http://example/label": [{"@value": "Object with no @id"}]},
    {"http://example/label": [{"@value": "Another object with no @id"}]}
  ]
}]"###).expect("Invalid output JSON"));}

// type map with alias of @none
//
// index on @type
#[tokio::test]
async fn tm012() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example/",
    "typemap": {"@container": "@type"},
    "none": "@none"
  },
  "typemap": {
    "@none": {"label": "Object with no @type"},
    "none": {"label": "Another object with no @type"}
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/m012-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example/typemap": [
    {"http://example/label": [{"@value": "Object with no @type"}]},
    {"http://example/label": [{"@value": "Another object with no @type"}]}
  ]
}]"###).expect("Invalid output JSON"));}

// graph index map with @none
//
// index on @graph and @index
#[tokio::test]
async fn tm013() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "input": {"@container": ["@graph", "@index"]}
  },
  "input": {
    "@none": {"value": "x"}
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/m013-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/input": [{
    "@graph": [{
      "http://example.org/value": [{"@value": "x"}]
    }]
  }]
}]"###).expect("Invalid output JSON"));}

// graph index map with alias @none
//
// index on @graph and @index
#[tokio::test]
async fn tm014() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "input": {"@container": ["@graph", "@index"]},
    "none": "@none"
  },
  "input": {
    "none": {"value": "x"}
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/m014-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/input": [{
    "@graph": [{
      "http://example.org/value": [{"@value": "x"}]
    }]
  }]
}]"###).expect("Invalid output JSON"));}

// graph id index map with aliased @none
//
// index on @graph and @id with @none
#[tokio::test]
async fn tm015() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "input": {"@container": ["@graph", "@id"]}
  },
  "input": {
    "@none": {"value": "x"}
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/m015-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/input": [{
    "@graph": [{
      "http://example.org/value": [{"@value": "x"}]
    }]
  }]
}]"###).expect("Invalid output JSON"));}

// graph id index map with aliased @none
//
// index on @graph and @id with @none
#[tokio::test]
async fn tm016() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "input": {"@container": ["@graph", "@id"]},
    "none": "@none"
  },
  "input": {
    "none": {"value": "x"}
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/m016-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/input": [{
    "@graph": [{
      "http://example.org/value": [{"@value": "x"}]
    }]
  }]
}]"###).expect("Invalid output JSON"));}

// string value of type map expands to node reference
//
// index on @type
#[tokio::test]
async fn tm017() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@vocab": "http://example.org/ns/",
    "@base": "http://example.org/base/",
    "foo": { "@container": "@type" }
  },
  "foo": {"bar": "baz"}
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/m017-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/ns/foo": [{
    "@id": "http://example.org/base/baz",
    "@type": [ "http://example.org/ns/bar" ]
  }]
}]"###).expect("Invalid output JSON"));}

// string value of type map expands to node reference with @type: @id
//
// index on @type
#[tokio::test]
async fn tm018() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@vocab": "http://example.org/ns/",
    "@base": "http://example.org/base/",
    "foo": { "@type": "@id", "@container": "@type" }
  },
  "foo": {"bar": "baz"}
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/m018-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/ns/foo": [{
    "@id": "http://example.org/base/baz",
    "@type": [ "http://example.org/ns/bar" ]
  }]
}]"###).expect("Invalid output JSON"));}

// string value of type map expands to node reference with @type: @vocab
//
// index on @type
#[tokio::test]
async fn tm019() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@vocab": "http://example.org/ns/",
    "@base": "http://example.org/base/",
    "foo": { "@type": "@vocab", "@container": "@type" }
  },
  "foo": {"bar": "baz"}
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/m019-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/ns/foo": [{
    "@id": "http://example.org/ns/baz",
    "@type": [ "http://example.org/ns/bar" ]
  }]
}]"###).expect("Invalid output JSON"));}

// string value of type map must not be a literal
//
// index on @type
#[tokio::test]
async fn tm020() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@vocab": "http://example.org/ns/",
    "@base": "http://example.org/base/",
    "foo": { "@type": "literal", "@container": "@type" }
  },
  "foo": {"bar": "baz"}
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/m020-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid type mapping");}

// Expands input using @nest
//
// Expansion using @nest
#[tokio::test]
async fn tn001() {
    let input = r###"{
  "@context": {"@vocab": "http://example.org/"},
  "p1": "v1",
  "@nest": {
    "p2": "v2"
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/n001-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/p1": [{"@value": "v1"}],
  "http://example.org/p2": [{"@value": "v2"}]
}]"###).expect("Invalid output JSON"));}

// Expands input using aliased @nest
//
// Expansion using @nest
#[tokio::test]
async fn tn002() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "nest": "@nest"
  },
  "p1": "v1",
  "nest": {
    "p2": "v2"
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/n002-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/p1": [{"@value": "v1"}],
  "http://example.org/p2": [{"@value": "v2"}]
}]"###).expect("Invalid output JSON"));}

// Appends nested values when property at base and nested
//
// Expansion using @nest
#[tokio::test]
async fn tn003() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "nest": "@nest"
  },
  "p1": "v1",
  "nest": {
    "p2": "v3"
  },
  "p2": "v2"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/n003-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/p1": [{"@value": "v1"}],
  "http://example.org/p2": [
    {"@value": "v2"},
    {"@value": "v3"}
  ]
}]"###).expect("Invalid output JSON"));}

// Appends nested values from all @nest aliases
//
// Expansion using @nest
#[tokio::test]
async fn tn004() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "nest1": "@nest",
    "nest2": "@nest"
  },
  "p1": "v1",
  "nest2": {
    "p2": "v4"
  },
  "p2": "v2",
  "nest1": {
    "p2": "v3"
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/n004-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/p1": [{"@value": "v1"}],
  "http://example.org/p2": [
    {"@value": "v2"},
    {"@value": "v3"},
    {"@value": "v4"}
  ]
}]"###).expect("Invalid output JSON"));}

// Nested nested containers
//
// Expansion using @nest
#[tokio::test]
async fn tn005() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/"
  },
  "p1": "v1",
  "@nest": {
    "p2": "v3",
    "@nest": {
      "p2": "v4"
    }
  },
  "p2": "v2"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/n005-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/p1": [{"@value": "v1"}],
  "http://example.org/p2": [
    {"@value": "v2"},
    {"@value": "v3"},
    {"@value": "v4"}
  ]
}]"###).expect("Invalid output JSON"));}

// Arrays of nested values
//
// Expansion using @nest
#[tokio::test]
async fn tn006() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "nest": "@nest"
  },
  "p1": "v1",
  "nest": {
    "p2": ["v4", "v5"]
  },
  "p2": ["v2", "v3"]
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/n006-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/p1": [{"@value": "v1"}],
  "http://example.org/p2": [
    {"@value": "v2"},
    {"@value": "v3"},
    {"@value": "v4"},
    {"@value": "v5"}
  ]
}]"###).expect("Invalid output JSON"));}

// A nest of arrays
//
// Expansion using @nest
#[tokio::test]
async fn tn007() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "nest": "@nest"
  },
  "p1": "v1",
  "nest": [{
    "p2": "v4"
  }, {
    "p2": "v5"
  }],
  "p2": ["v2", "v3"]
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/n007-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/p1": [{"@value": "v1"}],
  "http://example.org/p2": [
    {"@value": "v2"},
    {"@value": "v3"},
    {"@value": "v4"},
    {"@value": "v5"}
  ]
}]"###).expect("Invalid output JSON"));}

// Multiple keys may mapping to @type when nesting
//
// Expansion using @nest
#[tokio::test]
async fn tn008() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "nest": "@nest"
  },
  "p1": "v1",
  "nest": [
    {"@type": "T1"},
    {"@type": "T2"}
  ]
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/n008-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
 "@type": ["http://example.org/T1", "http://example.org/T2"],
 "http://example.org/p1": [{"@value": "v1"}]
}]"###).expect("Invalid output JSON"));}

// @version may be specified after first context
//
// If processing mode is not set through API, it is set by the first context containing @version.
#[tokio::test]
async fn tp001() {
    let input = r###"{
  "@context": [
    {"@vocab": "http://example/"},
    {"@version": 1.1, "a": {"@type": "@id"}}
  ],
  "a": "http://example.org/foo"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/p001-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
 "http://example/a": [{"@id": "http://example.org/foo"}]
}]"###).expect("Invalid output JSON"));}

// @version setting [1.0, 1.1, 1.0]
//
// If processing mode is not set through API, it is set by the first context containing @version.
#[tokio::test]
async fn tp002() {
    let input = r###"{
  "@context": [
    {"@vocab": "http://example/"},
    {"@version": 1.1, "a": {"@type": "@id"}},
    {"b": {"@type": "@id"}}
  ],
  "a": "http://example.org/foo",
  "b": "http://example.org/bar"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/p002-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
 "http://example/a": [{"@id": "http://example.org/foo"}],
 "http://example/b": [{"@id": "http://example.org/bar"}]
}]"###).expect("Invalid output JSON"));}

// @version setting [1.1, 1.0]
//
// If processing mode is not set through API, it is set by the first context containing @version.
#[tokio::test]
async fn tp003() {
    let input = r###"{
  "@context": [
    {"@version": 1.1, "a": {"@id": "http://example/a", "@type": "@id"}},
    {"@vocab": "http://example/", "b": {"@type": "@id"}}
  ],
  "a": "http://example.org/foo",
  "b": "http://example.org/bar"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/p003-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
 "http://example/a": [{"@id": "http://example.org/foo"}],
 "http://example/b": [{"@id": "http://example.org/bar"}]
}]"###).expect("Invalid output JSON"));}

// @version setting [1.1, 1.0, 1.1]
//
// If processing mode is not set through API, it is set by the first context containing @version.
#[tokio::test]
async fn tp004() {
    let input = r###"{
  "@context": [
    {"@version": 1.1, "a": {"@id": "http://example/a", "@type": "@id"}},
    {"@vocab": "http://example/"},
    {"@version": 1.1, "b": {"@type": "@id"}}
  ],
  "a": "http://example.org/foo",
  "b": "http://example.org/bar"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/p004-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
 "http://example/a": [{"@id": "http://example.org/foo"}],
 "http://example/b": [{"@id": "http://example.org/bar"}]
}]"###).expect("Invalid output JSON"));}

// error if @version is json-ld-1.0 for property-valued index
//
// Expanding index maps where index is a property.
#[tokio::test]
async fn tpi01() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.com/",
    "container": {"@container": "@index", "@index": "prop"}
  },
  "@id": "http://example.com/annotationsTest",
  "container": {
    "en": "The Queen",
    "de": [ "Die Königin", "Ihre Majestät" ]
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pi01-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid term definition");}

// error if @container does not include @index for property-valued index
//
// Expanding index maps where index is a property.
#[tokio::test]
async fn tpi02() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@vocab": "http://example.com/",
    "container": {"@index": "prop"}
  },
  "@id": "http://example.com/annotationsTest",
  "container": {
    "en": "The Queen",
    "de": [ "Die Königin", "Ihre Majestät" ]
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pi02-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid term definition");}

// error if @index is a keyword for property-valued index
//
// Expanding index maps where index is a property.
#[tokio::test]
async fn tpi03() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@vocab": "http://example.com/",
    "container": {
      "@id": "http://example.com/container",
      "@container": "@index",
      "@index": "@index"
    }
  },
  "@id": "http://example.com/annotationsTest",
  "container": {
    "en": "The Queen",
    "de": [ "Die Königin", "Ihre Majestät" ]
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pi03-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid term definition");}

// error if @index is not a string for property-valued index
//
// Expanding index maps where index is a property.
#[tokio::test]
async fn tpi04() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@vocab": "http://example.com/",
    "container": {
      "@id": "http://example.com/container",
      "@container": "@index",
      "@index": true
    }
  },
  "@id": "http://example.com/annotationsTest",
  "container": {
    "en": "The Queen",
    "de": [ "Die Königin", "Ihre Majestät" ]
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pi04-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid term definition");}

// error if attempting to add property to value object for property-valued index
//
// Expanding index maps where index is a property.
#[tokio::test]
async fn tpi05() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@vocab": "http://example.com/",
    "container": {
      "@id": "http://example.com/container",
      "@container": "@index",
      "@index": "prop"
    }
  },
  "@id": "http://example.com/annotationsTest",
  "container": {
    "en": "The Queen",
    "de": [ "Die Königin", "Ihre Majestät" ]
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pi05-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid value object");}

// property-valued index expands to property value, instead of @index (value)
//
// Expanding index maps where index is a property.
#[tokio::test]
async fn tpi06() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@base": "http://example.com/",
    "@vocab": "http://example.com/",
    "author": {"@type": "@id", "@container": "@index", "@index": "prop"}
  },
  "@id": "article",
  "author": {
    "regular": "person/1",
    "guest": ["person/2", "person/3"]
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pi06-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@id": "http://example.com/article",
  "http://example.com/author": [
    {"@id": "http://example.com/person/2", "http://example.com/prop": [{"@value": "guest"}]},
    {"@id": "http://example.com/person/3", "http://example.com/prop": [{"@value": "guest"}]},
    {"@id": "http://example.com/person/1", "http://example.com/prop": [{"@value": "regular"}]}
  ]
}]"###).expect("Invalid output JSON"));}

// property-valued index appends to property value, instead of @index (value)
//
// Expanding index maps where index is a property.
#[tokio::test]
async fn tpi07() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@base": "http://example.com/",
    "@vocab": "http://example.com/",
    "author": {"@type": "@id", "@container": "@index", "@index": "prop"}
  },
  "@id": "article",
  "author": {
    "regular": {"@id": "person/1", "http://example.com/prop": "foo"},
    "guest": [
      {"@id": "person/2", "prop": "foo"},
      {"@id": "person/3", "prop": "foo"}
    ]
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pi07-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@id": "http://example.com/article",
  "http://example.com/author": [
    {"@id": "http://example.com/person/2", "http://example.com/prop": [{"@value": "guest"}, {"@value": "foo"}]},
    {"@id": "http://example.com/person/3", "http://example.com/prop": [{"@value": "guest"}, {"@value": "foo"}]},
    {"@id": "http://example.com/person/1", "http://example.com/prop": [{"@value": "regular"}, {"@value": "foo"}]}
  ]
}]"###).expect("Invalid output JSON"));}

// property-valued index expands to property value, instead of @index (node)
//
// Expanding index maps where index is a property.
#[tokio::test]
async fn tpi08() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@base": "http://example.com/",
    "@vocab": "http://example.com/",
    "author": {"@type": "@id", "@container": "@index", "@index": "prop"},
    "prop": {"@type": "@vocab"}
  },
  "@id": "http://example.com/article",
  "author": {
    "regular": "person/1",
    "guest": ["person/2", "person/3"]
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pi08-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@id": "http://example.com/article",
  "http://example.com/author": [
    {"@id": "http://example.com/person/2", "http://example.com/prop": [{"@id": "http://example.com/guest"}]},
    {"@id": "http://example.com/person/3", "http://example.com/prop": [{"@id": "http://example.com/guest"}]},
    {"@id": "http://example.com/person/1", "http://example.com/prop": [{"@id": "http://example.com/regular"}]}
  ]
}]"###).expect("Invalid output JSON"));}

// property-valued index appends to property value, instead of @index (node)
//
// Expanding index maps where index is a property.
#[tokio::test]
async fn tpi09() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@base": "http://example.com/",
    "@vocab": "http://example.com/",
    "author": {"@type": "@id", "@container": "@index", "@index": "prop"},
    "prop": {"@type": "@vocab"}
  },
  "@id": "http://example.com/article",
  "author": {
    "regular": {"@id": "person/1", "prop": "foo"},
    "guest": [
      {"@id": "person/2", "prop": "foo"},
      {"@id": "person/3", "prop": "foo"}
    ]
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pi09-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@id": "http://example.com/article",
  "http://example.com/author": [
    {"@id": "http://example.com/person/2", "http://example.com/prop": [{"@id": "http://example.com/guest"}, {"@id": "http://example.com/foo"}]},
    {"@id": "http://example.com/person/3", "http://example.com/prop": [{"@id": "http://example.com/guest"}, {"@id": "http://example.com/foo"}]},
    {"@id": "http://example.com/person/1", "http://example.com/prop": [{"@id": "http://example.com/regular"}, {"@id": "http://example.com/foo"}]}
  ]
}]"###).expect("Invalid output JSON"));}

// property-valued index does not output property for @none
//
// Expanding index maps where index is a property.
#[tokio::test]
async fn tpi10() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@base": "http://example.com/",
    "@vocab": "http://example.com/",
    "author": {"@type": "@id", "@container": "@index", "@index": "prop"},
    "prop": {"@type": "@vocab"}
  },
  "@id": "http://example.com/article",
  "author": {
    "@none": {"@id": "person/1"},
    "guest": [
      {"@id": "person/2"},
      {"@id": "person/3"}
    ]
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pi10-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@id": "http://example.com/article",
  "http://example.com/author": [
    {"@id": "http://example.com/person/1"},
    {"@id": "http://example.com/person/2", "http://example.com/prop": [{"@id": "http://example.com/guest"}]},
    {"@id": "http://example.com/person/3", "http://example.com/prop": [{"@id": "http://example.com/guest"}]}
  ]
}]"###).expect("Invalid output JSON"));}

// property-valued index adds property to graph object
//
// Expanding index maps where index is a property.
#[tokio::test]
async fn tpi11() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@vocab": "http://example.org/",
    "input": {"@container": ["@graph", "@index"], "@index": "prop"}
  },
  "input": {
    "g1": {"value": "x"}
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pi11-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/input": [{
    "http://example.org/prop": [{"@value": "g1"}],
    "@graph": [{
      "http://example.org/value": [{"@value": "x"}]
    }]
  }]
}]"###).expect("Invalid output JSON"));}

// Protect a term
//
// Check error when overriding a protected term.
#[tokio::test]
async fn tpr01() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.com/",
    "@version": 1.1,
    "protected": {
      "@protected": true
    }
  },
  "protected": {
    "@context": {
      "protected": "http://example.com/something-else"
    },
    "protected": "error / property http://example.com/protected"
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr01-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "protected term redefinition");}

// Set a term to not be protected
//
// A term with @protected: false is not protected.
#[tokio::test]
async fn tpr02() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.com/",
    "@version": 1.1,
    "protected": {
      "@protected": true
    },
    "unprotected": {
      "@protected": false
    }
  },
  "protected": true,
  "unprotected": true,
  "scope": {
    "@context": {
      "unprotected": "http://example.com/something-else"
    },
    "unprotected": "property http://example.com/something-else"
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr02-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "http://example.com/protected": [{"@value": true}],
    "http://example.com/unprotected": [{"@value": true}],
    "http://example.com/scope": [{
      "http://example.com/something-else": [{"@value": "property http://example.com/something-else"}]
    }]
  }
]
"###).expect("Invalid output JSON"));}

// Protect all terms in context
//
// A protected context protects all term definitions.
#[tokio::test]
async fn tpr03() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.com/",
    "@version": 1.1,
    "@protected": true,
    "protected1": {
      "@id": "http://example.com/protected1"
    },
    "protected2": {
      "@id": "http://example.com/protected2"
    }
  },
  "protected1": {
    "@context": {
      "protected1": "http://example.com/something-else",
      "protected2": "http://example.com/something-else"
    },
    "protected1": "error / property http://example.com/protected1",
    "protected2": "error / property http://example.com/protected2"
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr03-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "protected term redefinition");}

// Do not protect term with @protected: false
//
// A protected context does not protect terms with @protected: false.
#[tokio::test]
async fn tpr04() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.com/",
    "@version": 1.1,
    "@protected": true,
    "protected": {
      "@id": "http://example.com/protected"
    },
    "unprotected": {
      "@id": "http://example.com/unprotected", "@protected": false
    }
  },
  "protected": {
    "@context": {
      "protected": "http://example.com/something-else1",
      "unprotected": "http://example.com/something-else2"
    },
    "protected": "error / property http://example.com/protected",
    "unprotected": "property http://example.com/something-else2"
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr04-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "protected term redefinition");}

// Clear active context with protected terms from an embedded context
//
// The Active context be set to null from an embedded context.
#[tokio::test]
async fn tpr05() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.com/",
    "@version": 1.1,
    "@protected": true,
    "protected": {"@language": null}
  },
  "protected": {
    "@context": [
      null,
      {
        "@vocab": "http://something-else/"
      }
    ],
    "protected": "error / property http://example.com/protected"
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr05-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid context nullification");}

// Clear active context of protected terms from a term.
//
// The Active context may be set to null from a scoped context of a term.
#[tokio::test]
async fn tpr06() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.com/",
    "@version": 1.1,
    "@protected": true,
    "protected": {
      "@type": "@id"
    },
    "unprotected": {
      "@protected": false,
      "@context": null
    }
  },
  "unprotected": {
    "protected": "not expanded, as protected is not a defined term"
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr06-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "http://example.com/unprotected": [{}]
  }
]
"###).expect("Invalid output JSON"));}

// Term with protected scoped context.
//
// A scoped context can protect terms.
#[tokio::test]
async fn tpr08() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.com/",
    "@version": 1.1,
    "protected": {
      "@protected": false
    },
    "scope1": {
      "@protected": false,
      "@context": {
        "protected": {
          "@id": "http://example.com/something-else"
        }
      }
    },
    "scope2": {
      "@protected": true,
      "@context": {
        "protected": {
          "@protected": true
        }
      }
    }
  },
  "protected": false,
  "scope1": {
    "@context": {
      "protected": "http://example.com/another-thing"
    },
    "protected": "property http://example.com/another-thing"
  },
  "scope2": {
    "@context": {
      "protected": "http://example.com/another-thing"
    },
    "protected": "error / property http://example.com/protected"
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr08-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "protected term redefinition");}

// Attempt to redefine term in other protected context.
//
// A protected term cannot redefine another protected term.
#[tokio::test]
async fn tpr09() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@protected": true,
    "protected1": "http://example.org/protected1",
    "protected2": "http://example.org/protected2"
  },
  "protected2": {
    "@context": {
      "protected1": "http://example.org/something-else"
    },
    "protected1": "error / property http://example.org/protected1"
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr09-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "protected term redefinition");}

// Simple protected and unprotected terms.
//
// Simple protected and unprotected terms.
#[tokio::test]
async fn tpr10() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "protected": {
      "@id": "ex:protected",
      "@protected": true
    },
    "unprotected": "ex:unprotected"
  },
  "protected": "p === ex:protected",
  "unprotected": {
    "protected": "p === ex:protected"
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr10-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "ex:protected": [
      {
        "@value": "p === ex:protected"
      }
    ],
    "ex:unprotected": [
      {
        "ex:protected": [
          {
            "@value": "p === ex:protected"
          }
        ]
      }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// Fail to override protected term.
//
// Fail to override protected term.
#[tokio::test]
async fn tpr11() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "protected": {
      "@id": "ex:protected",
      "@protected": true
    },
    "unprotected": "ex:unprotected"
  },
  "protected": "p === ex:protected",
  "unprotected": {
    "@context": {
      "protected": "ex:protected2"
    },
    "protected": "p === ex:protected"
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr11-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "protected term redefinition");}

// Scoped context fail to override protected term.
//
// Scoped context fail to override protected term.
#[tokio::test]
async fn tpr12() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@protected": true,
    "protected1": "ex:protected1",
    "protected2": "ex:protected2"
  },
  "protected1": "p === ex:protected1",
  "protected2": {
    "@context": {
      "protected1": "ex:protected1:error"
    },
    "protected1": "error / p === ex:protected1"
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr12-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "protected term redefinition");}

// Override unprotected term.
//
// Override unprotected term.
#[tokio::test]
async fn tpr13() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@protected": true,
    "protected": "ex:protected",
    "unprotected": {
      "@id": "ex:unprotected1",
      "@protected": false
    }
  },
  "protected": {
    "@context": {
      "unprotected": "ex:unprotected2"
    },
    "unprotected": "p === ex:unprotected2"
  },
  "unprotected": "p === ex:unprotected1"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr13-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "ex:protected": [
      {
        "ex:unprotected2": [
          {
            "@value": "p === ex:unprotected2"
          }
        ]
      }
    ],
    "ex:unprotected1": [
      {
        "@value": "p === ex:unprotected1"
      }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// Clear protection with null context.
//
// Clear protection with null context.
#[tokio::test]
async fn tpr14() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@protected": true,
    "protected1": "ex:protected1",
    "protected2": {
      "@id": "ex:protected2",
      "@context": null
    }
  },
  "protected1": "p === ex:protected1",
  "protected2": {
    "@context": {
      "protected1": "ex:protected3"
    },
    "protected1": "p === ex:protected3"
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr14-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "ex:protected1": [
      {
        "@value": "p === ex:protected1"
      }
    ],
    "ex:protected2": [
      {
        "ex:protected3": [
          {
            "@value": "p === ex:protected3"
          }
        ]
      }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// Clear protection with array with null context
//
// Clear protection with array with null context
#[tokio::test]
async fn tpr15() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@protected": true,
    "protected1": "ex:protected1",
    "protected2": {
      "@id": "ex:protected2",
      "@context": [
        null
      ]
    }
  },
  "protected1": "p === ex:protected1",
  "protected2": {
    "@context": {
      "protected1": "ex:protected3"
    },
    "protected1": "p === ex:protected3"
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr15-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "ex:protected1": [
      {
        "@value": "p === ex:protected1"
      }
    ],
    "ex:protected2": [
      {
        "ex:protected3": [
          {
            "@value": "p === ex:protected3"
          }
        ]
      }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// Override protected terms after null.
//
// Override protected terms after null.
#[tokio::test]
async fn tpr16() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@protected": true,
    "protected1": "ex:protected1",
    "protected2": {
      "@id": "ex:protected2",
      "@context": [
        null,
        {
          "protected1": "ex:protected3",
          "unprotected": "ex:unprotected2"
        }
      ]
    },
    "unprotected": {
      "@protected": false,
      "@id": "ex:unprotected1"
    }
  },
  "protected1": "p === ex:protected1",
  "protected2": {
    "@context": {
      "protected1": "ex:protected3"
    },
    "protected1": "p === ex:protected3",
    "unprotected": "p === ex:unprotected2"
  },
  "unprotected": "p === ex:unprotected1"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr16-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "ex:protected1": [
      {
        "@value": "p === ex:protected1"
      }
    ],
    "ex:protected2": [
      {
        "ex:protected3": [
          {
            "@value": "p === ex:protected3"
          }
        ],
        "ex:unprotected2": [
          {
            "@value": "p === ex:unprotected2"
          }
        ]
      }
    ],
    "ex:unprotected1": [
      {
        "@value": "p === ex:unprotected1"
      }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// Fail to override protected terms with type.
//
// Fail to override protected terms with type.
#[tokio::test]
async fn tpr17() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@protected": true,
    "protected1": "ex:protected1",
    "protected2": "ex:protected2",
    "Protected": {
      "@id": "ex:Protected",
      "@context": [
        null
      ]
    }
  },
  "protected1": "p === protected1",
  "protected2": {
    "@context": {
      "unprotected": "ex:unprotected"
    },
    "@type": "Protected",
    "unprotected": "error / omitted"
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr17-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid context nullification");}

// Fail to override protected terms with type+null+ctx.
//
// Fail to override protected terms with type+null+ctx.
#[tokio::test]
async fn tpr18() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@protected": true,
    "protected": "ex:protected1",
    "Protected": {
      "@id": "ex:Protected",
      "@context": [
        null,
        {
          "protected": "ex:protected2"
        }
      ]
    }
  },
  "@type": "Protected",
  "protected": "error / p === ex:protected1"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr18-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid context nullification");}

// Mix of protected and unprotected terms.
//
// Mix of protected and unprotected terms.
#[tokio::test]
async fn tpr19() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@protected": true,
    "protected1": "ex:protected1",
    "protected2": {
      "@id": "ex:protected2",
      "@context": [
        null,
        {
          "protected1": "ex:protected3"
        }
      ]
    },
    "unprotected": {
      "@protected": false,
      "@id": "ex:unprotected1"
    }
  },
  "protected1": "p === ex:protected1",
  "protected2": {
    "@context": {
      "protected1": "ex:protected3"
    },
    "protected1": "p === ex:protected3",
    "unprotected": "p === ex:unprotected2"
  },
  "unprotected": "p === ex:unprotected1"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr19-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "ex:protected1": [
      {
        "@value": "p === ex:protected1"
      }
    ],
    "ex:protected2": [
      {
        "ex:protected3": [
          {
            "@value": "p === ex:protected3"
          }
        ]
      }
    ],
    "ex:unprotected1": [
      {
        "@value": "p === ex:unprotected1"
      }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// Fail with mix of protected and unprotected terms with type+null+ctx.
//
// Fail with mix of protected and unprotected terms with type+null+ctx.
#[tokio::test]
async fn tpr20() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@protected": true,
    "protected1": "ex:protected1",
    "Protected": {
      "@id": "ex:Protected",
      "@context": [
        null,
        {
          "protected1": "ex:protected2",
          "unprotected": "ex:unprotected2"
        }
      ]
    },
    "unprotected": {
      "@protected": false,
      "@id": "ex:unprotected1"
    }
  },
  "@type": "Protected",
  "protected1": "error / p === ex:protected1",
  "unprotected": "p === ex:unprotected2"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr20-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid context nullification");}

// Fail with mix of protected and unprotected terms with type+null.
//
// Fail with mix of protected and unprotected terms with type+null.
#[tokio::test]
async fn tpr21() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@protected": true,
    "protected": "ex:protected",
    "Protected": {
      "@id": "ex:Protected",
      "@context": [
        null
      ]
    },
    "unprotected": {
      "@protected": false,
      "@id": "ex:unprotected"
    }
  },
  "@type": "Protected",
  "protected": "error / p === ex:protected",
  "unprotected": "missing"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr21-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid context nullification");}

// Check legal overriding of type-scoped protected term from nested node.
//
// Check legal overriding of type-scoped protected term from nested node.
#[tokio::test]
async fn tpr22() {
    let input = r###"{
  "@context": [{
    "@version": 1.1,
    "@protected": true,
    "@vocab": "http://example.com/",
    "Parent": {"@context": {"@protected": true, "foo": {"@type": "@id"}}}
  }, {
    "@version": 1.1,
    "@protected": true,
    "Child": {"@context": {"@protected": true, "foo": {"@type": "@id"}}}
  }],
  "@type": "Parent",
  "foo": {
    "@type": "Child",
    "foo": "http://example.com/test"
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr22-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@type": [
      "http://example.com/Parent"
    ],
    "http://example.com/foo": [
      {
        "@type": [
          "http://example.com/Child"
        ],
        "http://example.com/foo": [
          {
            "@id": "http://example.com/test"
          }
        ]
      }
    ]
  }
]

"###).expect("Invalid output JSON"));}

// Allows redefinition of protected alias term with same definition.
//
// Allows redefinition of protected alias term with same definition.
#[tokio::test]
async fn tpr23() {
    let input = r###"{
  "@context": [{
    "@version": 1.1,
    "@protected": true,
    "id": "@id",
    "type": "@type"
  }, {
    "@version": 1.1,
    "@protected": true,
    "id": "@id",
    "type": "@type"
  }],
  "id": "http://example/id",
  "type": "http://example/type"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr23-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example/id",
    "@type": [
      "http://example/type"
    ]
  }
]

"###).expect("Invalid output JSON"));}

// Allows redefinition of protected prefix term with same definition.
//
// Allows redefinition of protected prefix term with same definition.
#[tokio::test]
async fn tpr24() {
    let input = r###"{
  "@context": [{
    "@version": 1.1,
    "@protected": true,
    "foo": "http://example/foo#"
  }, {
    "@version": 1.1,
    "@protected": true,
    "foo": "http://example/foo#"
  }],
  "foo:bar": "foobar"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr24-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "http://example/foo#bar": [{
      "@value": "foobar"
    }]
  }
]

"###).expect("Invalid output JSON"));}

// Allows redefinition of terms with scoped contexts using same definitions.
//
// Allows redefinition of terms with scoped contexts using same definitions.
#[tokio::test]
async fn tpr25() {
    let input = r###"{
  "@context": [{
    "@version": 1.1,
    "@protected": true,
    "id": "@id",
    "type": "@type",
    "foo": {
      "@id": "http://example/foo",
      "@container": ["@graph", "@set"]
    },
    "bar": {
      "@id": "http://example/bar",
      "@type": "@id",
      "@context": {
        "@version": 1.1,
        "@protected": true,
        "bar-1": {
          "@id": "http://example/bar-1",
          "@context": {
            "@version": 1.1,
            "@protected": true,
            "bar-2": "http://example/bar-2",
            "Foo": {
              "@id": "http://example/Foo",
              "@context": {
                "@version": 1.1,
                "@protected": true,
                "bar-2": "http://example/bar-2"
              }
            }
          }
        }
      }
    },
    "Bar": {
      "@id": "http://example/Bar",
      "@context": {
        "@version": 1.1,
        "@protected": true,
        "bar-1": {
          "@id": "http://example/bar-1",
          "@context": {
            "@version": 1.1,
            "@protected": true,
            "bar-2": "http://example/bar-2",
            "Foo": {
              "@id": "http://example/Foo",
              "@context": {
                "@version": 1.1,
                "@protected": true,
                "bar-2": "http://example/bar-2"
              }
            }
          }
        }
      }
    },
    "Foo": {
      "@id": "http://example/Foo",
      "@context": {
        "@version": 1.1,
        "@protected": true,
        "bar-2": "http://example/bar-2"
      }
    }
  }, {
    "@version": 1.1,
    "@protected": true,
    "id": "@id",
    "type": "@type",
    "foo": {
      "@id": "http://example/foo",
      "@container": ["@graph", "@set"]
    },
    "bar": {
      "@id": "http://example/bar",
      "@type": "@id",
      "@context": {
        "@version": 1.1,
        "@protected": true,
        "bar-1": {
          "@id": "http://example/bar-1",
          "@context": {
            "@version": 1.1,
            "@protected": true,
            "bar-2": "http://example/bar-2",
            "Foo": {
              "@id": "http://example/Foo",
              "@context": {
                "@version": 1.1,
                "@protected": true,
                "bar-2": "http://example/bar-2"
              }
            }
          }
        }
      }
    },
    "Bar": {
      "@id": "http://example/Bar",
      "@context": {
        "@version": 1.1,
        "@protected": true,
        "bar-1": {
          "@id": "http://example/bar-1",
          "@context": {
            "@version": 1.1,
            "@protected": true,
            "bar-2": "http://example/bar-2",
            "Foo": {
              "@id": "http://example/Foo",
              "@context": {
                "@version": 1.1,
                "@protected": true,
                "bar-2": "http://example/bar-2"
              }
            }
          }
        }
      }
    },
    "Foo": {
      "@id": "http://example/Foo",
      "@context": {
        "@version": 1.1,
        "@protected": true,
        "bar-2": "http://example/bar-2"
      }
    }
  }],
  "type": "Bar",
  "foo": [{
    "bar": "http://example/"
  }],
  "bar-1": {
    "bar-2": {
      "type": "Foo",
      "bar-2": "bar-2"
    }
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr25-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@type": [
      "http://example/Bar"
    ],
    "http://example/foo": [{
      "@graph": [{
        "http://example/bar": [{
          "@id": "http://example/"
        }]
      }]
    }],
    "http://example/bar-1": [{
      "http://example/bar-2": [{
        "@type": ["http://example/Foo"],
        "http://example/bar-2": [{
          "@value": "bar-2"
        }]
      }]
    }]
  }
]

"###).expect("Invalid output JSON"));}

// Fails on redefinition of terms with scoped contexts using different definitions.
//
// Fails on redefinition of terms with scoped contexts using different definitions.
#[tokio::test]
async fn tpr26() {
    let input = r###"{
  "@context": [{
    "@version": 1.1,
    "@protected": true,
    "id": "@id",
    "type": "@type",
    "foo": {
      "@id": "http://example/foo",
      "@container": ["@graph", "@set"]
    },
    "bar": {
      "@id": "http://example/bar",
      "@type": "@id",
      "@context": {
        "@version": 1.1,
        "@protected": true,
        "bar-1": {
          "@id": "http://example/bar-1",
          "@context": {
            "@version": 1.1,
            "@protected": true,
            "bar-2": "http://example/bar-2",
            "Foo": {
              "@id": "http://example/Foo",
              "@context": {
                "@version": 1.1,
                "@protected": true,
                "bar-2": "http://example/bar-2"
              }
            }
          }
        }
      }
    },
    "Bar": {
      "@id": "http://example/Bar",
      "@context": {
        "@version": 1.1,
        "@protected": true,
        "bar-1": {
          "@id": "http://example/bar-1",
          "@context": {
            "@version": 1.1,
            "@protected": true,
            "bar-2": "http://example/bar-2",
            "Foo": {
              "@id": "http://example/Foo",
              "@context": {
                "@version": 1.1,
                "@protected": true,
                "bar-2": "http://example/bar-2"
              }
            }
          }
        }
      }
    },
    "Foo": {
      "@id": "http://example/Foo",
      "@context": {
        "@version": 1.1,
        "@protected": true,
        "bar-2": "http://example/bar-2"
      }
    }
  }, {
    "@version": 1.1,
    "@protected": true,
    "id": "@id",
    "type": "@type",
    "foo": {
      "@id": "http://example/foo",
      "@container": ["@graph", "@set"]
    },
    "bar": {
      "@id": "http://example/bar",
      "@type": "@id",
      "@context": {
        "@version": 1.1,
        "@protected": true,
        "bar-1": {
          "@id": "http://example/bar-1",
          "@context": {
            "@version": 1.1,
            "@protected": true,
            "bar-2": "http://example/bar-2",
            "Foo": {
              "@id": "http://example/Foo",
              "@context": {
                "@version": 1.1,
                "@protected": true,
                "bar-2": "http://example/bar-2"
              }
            }
          }
        }
      }
    },
    "Bar": {
      "@id": "http://example/Bar",
      "@context": {
        "@version": 1.1,
        "@protected": true,
        "bar-1": {
          "@id": "http://example/bar-1",
          "@context": {
            "@version": 1.1,
            "@protected": true,
            "bar-2": "http://example/bar-2",
            "Foo": {
              "@id": "http://example/Foo",
              "@context": {
                "@version": 1.1,
                "@protected": true,
                "bar-2": "http://example/bar-2"
              }
            }
          }
        }
      }
    },
    "Foo": {
      "@id": "http://example/Foo"
    }
  }],
  "type": "Bar",
  "foo": [{
    "bar": "http://example/"
  }],
  "bar-1": {
    "bar-2": {
      "type": "Foo",
      "bar-2": "bar-2"
    }
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr26-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "protected term redefinition");}

// Allows redefinition of protected alias term with same definition modulo protected flag.
//
// Allows redefinition of protected alias term with same definition modulo protected flag.
#[tokio::test]
async fn tpr27() {
    let input = r###"{
  "@context": [{
    "@version": 1.1,
    "@protected": true,
    "id": "@id",
    "type": "@type"
  }, {
    "@version": 1.1,
    "id": "@id",
    "type": "@type"
  }],
  "id": "http://example/id",
  "type": "http://example/type"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr27-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example/id",
    "@type": [
      "http://example/type"
    ]
  }
]

"###).expect("Invalid output JSON"));}

// Fails if trying to redefine a protected null term.
//
// A protected term with a null IRI mapping cannot be redefined.
#[tokio::test]
async fn tpr28() {
    let input = r###"{
  "@context": [{
    "@version": 1.1,
    "@protected": true,
    "term": null
  }, {
    "@version": 1.1,
    "term": {"@id": "http://example.com/term"}
  }],
  "term": "undefined"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr28-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "protected term redefinition");}

// Does not expand a Compact IRI using a non-prefix term.
//
// Expansion of Compact IRIs considers if the term can be used as a prefix.
#[tokio::test]
async fn tpr29() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "tag": {"@id": "http://example.org/ns/tag/", "@prefix": false}
  },
  "tag:champin.net,2019:prop": "This is not treated as a Compact IRI",
  "tag": "tricky"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr29-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "http://example.org/ns/tag/": [
      {
        "@value": "tricky"
      }
    ],
    "tag:champin.net,2019:prop": [
      {
        "@value": "This is not treated as a Compact IRI"
      }
    ]
  }
]
"###).expect("Invalid output JSON"));}

// Keywords may be protected.
//
// Keywords may not be redefined other than to protect them.
#[tokio::test]
async fn tpr30() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "id": {"@id": "@id", "@protected": true},
    "type": {"@id" : "@type", "@container": "@set", "@protected" : true},
    "@type": {"@container": "@set", "@protected": true}
  },
  "id": "http://example.com/1",
  "type": "http://example.org/ns/Foo",
  "@type": "http://example.org/ns/Bar"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr30-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@id": "http://example.com/1",
    "@type": [
      "http://example.org/ns/Bar",
      "http://example.org/ns/Foo"
    ]
  }
]"###).expect("Invalid output JSON"));}

// Protected keyword aliases cannot be overridden.
//
// Keywords may not be redefined other than to protect them.
#[tokio::test]
async fn tpr31() {
    let input = r###"{
  "@context": [{
    "@version": 1.1,
    "id": {"@id": "@id", "@protected": true},
    "type": {"@id" : "@type", "@container": "@set", "@protected" : true},
    "@type": {"@container": "@set", "@protected": true}
  }, {
    "@version": 1.1,
    "id": "http://example.com/id"
  }],
  "id": "http://example.com/1",
  "type": ["http://example.org/ns/Foo"]
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr31-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "protected term redefinition");}

// Protected @type cannot be overridden.
//
// Keywords may not be redefined other than to protect them.
#[tokio::test]
async fn tpr32() {
    let input = r###"{
  "@context": [{
    "@version": 1.1,
    "id": {"@id": "@id", "@protected": true},
    "type": {"@id" : "@type", "@container": "@set", "@protected" : true},
    "@type": {"@container": "@set", "@protected": true}
  }, {
    "@version": 1.1,
    "@type": {"@protected": true}
  }],
  "id": "http://example.com/1",
  "type": ["http://example.org/ns/Foo"]
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr32-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "protected term redefinition");}

// Fails if trying to declare a keyword alias as prefix.
//
// Keyword aliases can not be used as prefixes.
#[tokio::test]
async fn tpr33() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "foo": {
      "@id": "@type",
      "@prefix": true
    }
  },
  "foo:bar": "http://example.org/baz"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr33-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid term definition");}

// Ignores a non-keyword term starting with '@'
//
// Terms in the form of a keyword, which are not keywords, are ignored.
#[tokio::test]
async fn tpr34() {
    let input = r###"{
  "@context": {
    "@ignoreMe": "http://example.org/should-ignore"
  },
  "@type": "http://example.com/IgnoreTest",
  "@ignoreMe": "should not be here"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr34-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@type": ["http://example.com/IgnoreTest"]
}]"###).expect("Invalid output JSON"));}

// Ignores a non-keyword term starting with '@' (with @vocab)
//
// Terms in the form of a keyword, which are not keywords, are ignored.
#[tokio::test]
async fn tpr35() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "@ignoreMe": "http://example.org/should-ignore"
  },
  "@type": "http://example.com/IgnoreTest",
  "@ignoreMe": "should not be here"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr35-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@type": ["http://example.com/IgnoreTest"]
}]"###).expect("Invalid output JSON"));}

// Ignores a term mapping to a value in the form of a keyword.
//
// Terms in the form of a keyword, which are not keywords, are ignored.
#[tokio::test]
async fn tpr36() {
    let input = r###"{
  "@context": {
    "ignoreMe": "@ignoreMe"
  },
  "@type": "http://example.com/IgnoreTest",
  "ignoreMe": "should not be here"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr36-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@type": ["http://example.com/IgnoreTest"]
}]"###).expect("Invalid output JSON"));}

// Ignores a term mapping to a value in the form of a keyword (with @vocab).
//
// Terms in the form of a keyword, which are not keywords, are ignored.
#[tokio::test]
async fn tpr37() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "ignoreMe": "@ignoreMe"
  },
  "@type": "http://example.com/IgnoreTest",
  "ignoreMe": "vocabulary relative"
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr37-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@type": ["http://example.com/IgnoreTest"],
  "http://example.org/ignoreMe": [{"@value": "vocabulary relative"}]
}]"###).expect("Invalid output JSON"));}

// Ignores a term mapping to a value in the form of a keyword (@reverse).
//
// Terms in the form of a keyword, which are not keywords, are ignored.
#[tokio::test]
async fn tpr38() {
    let input = r###"{
  "@context": {
    "ignoreMe": {"@reverse": "@ignoreMe"}
  },
  "@type": "http://example.com/IgnoreTest",
  "ignoreMe": {"http://example.org/text": "should not be here"}
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr38-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@type": ["http://example.com/IgnoreTest"]
}]"###).expect("Invalid output JSON"));}

// Ignores a term mapping to a value in the form of a keyword (@reverse with @vocab).
//
// Terms in the form of a keyword, which are not keywords, are ignored.
#[tokio::test]
async fn tpr39() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.org/",
    "ignoreMe": {"@reverse": "@ignoreMe"}
  },
  "@type": "http://example.com/IgnoreTest",
  "ignoreMe": {"text": "not reversed"}
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr39-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@type": ["http://example.com/IgnoreTest"],
  "http://example.org/ignoreMe": [{
    "http://example.org/text": [{"@value": "not reversed"}]
  }]
}]
"###).expect("Invalid output JSON"));}

// Protected terms and property-scoped contexts
//
// Check overriding of protected term from property-scoped context.
#[tokio::test]
async fn tpr40() {
    let input = r###"{
  "@context": {
    "@vocab": "http://vocab.org/",
    "@protected": true,
    "bar": "http://ignored.org/bar",
    "foo": {
      "@context": {
        "bar": "http://example.org/other"
      }
    }
  },
  "@id": "ex:outer",
  "foo": {
    "@id": "ex:inner",
    "bar": "baz"
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/pr40-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "@id": "ex:outer",
  "http://vocab.org/foo": [{
    "@id": "ex:inner",
    "http://example.org/other": [{"@value": "baz"}]
  }]
}]"###).expect("Invalid output JSON"));}

// @import is invalid in 1.0.
//
// @import is invalid in 1.0.
#[tokio::test]
async fn tso01() {
    let input = r###"{
  "@context": {
    "@import": "so01-in.jsonld"
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/so01-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid context entry");}

// @import must be a string
//
// @import must be a string.
#[tokio::test]
async fn tso02() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@import": {}
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/so02-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid @import value");}

// @import overflow
//
// Processors must detect source contexts that include @import.
#[tokio::test]
async fn tso03() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@import": "so03-in.jsonld"
  }
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/so03-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid context entry");}

// @propagate: true on type-scoped context with @import
//
// type-scoped context with @propagate: true survive node-objects (with @import)
#[tokio::test]
async fn tso05() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@vocab": "http://example/",
    "Foo": {
      "@context": {
        "@import": "so05-context.jsonld",
        "@propagate": true
      }
    }
  },
  "@type": "Foo",
  "bar": {"baz": "buzz"}
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/so05-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[
  {
    "@type": ["http://example/Foo"],
    "http://example/bar": [{
      "http://example.org/baz": [{"@id": "http://example/buzz"}]
    }]
  }
]
"###).expect("Invalid output JSON"));}

// @propagate: false on property-scoped context with @import
//
// property-scoped context with @propagate: false do not survive node-objects (with @import)
#[tokio::test]
async fn tso06() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@vocab": "http://example/",
    "bar": {
      "@context": {
        "@import": "so06-context.jsonld",
        "@propagate": false
      }
    }
  },
  "bar": {"baz": {"baz": "buzz"}}
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/so06-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
 "http://example/bar": [{
   "http://example.com/baz": [{
     "http://example/baz": [{"@value": "buzz"}]
   }]
 }]
}]
"###).expect("Invalid output JSON"));}

// Protect all terms in sourced context
//
// A protected context protects all term definitions.
#[tokio::test]
async fn tso07() {
    let input = r###"{
  "@context": {
    "@vocab": "http://example.com/",
    "@version": 1.1,
    "@protected": true,
    "@import": "so07-context.jsonld"
  },
  "protected1": {
    "@context": {
      "protected1": "http://example.com/something-else",
      "protected2": "http://example.com/something-else"
    },
    "protected1": "error / property http://example.com/protected1",
    "protected2": "error / property http://example.com/protected2"
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/so07-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "protected term redefinition");}

// Override term defined in sourced context
//
// The containing context is merged into the source context.
#[tokio::test]
async fn tso08() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@import": "so08-context.jsonld",
    "term": "http://example.org/redefined"
  },
  "term": "value"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/so08-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/redefined": [{"@value": "value"}]
}]"###).expect("Invalid output JSON"));}

// Override @vocab defined in sourced context
//
// The containing context is merged into the source context.
#[tokio::test]
async fn tso09() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@import": "so09-context.jsonld",
    "@vocab": "http://example.org/redefined/"
  },
  "term": "value"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/so09-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/redefined/term": [{"@value": "value"}]
}]"###).expect("Invalid output JSON"));}

// Protect terms in sourced context
//
// The containing context is merged into the source context.
#[tokio::test]
async fn tso10() {
    let input = r###"{
  "@context": [{
    "@version": 1.1,
    "@protected": true,
    "@import": "so10-context.jsonld"
  }, {
    "term": "http://example.org/unprotected"
  }],
  "term": "value"
}
  "###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/so10-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "protected term redefinition");}

// Override protected terms in sourced context
//
// The containing context is merged into the source context.
#[tokio::test]
async fn tso11() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@protected": true,
    "@import": "so08-context.jsonld",
    "term": "http://example.org/redefined"
  },
  "term": "value"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/so11-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.org/redefined": [{"@value": "value"}]
}]"###).expect("Invalid output JSON"));}

// @import may not be used in an imported context.
//
// @import only valid within a term definition.
#[tokio::test]
async fn tso12() {
    let input = r###"{
  "@context": {
    "@import": "so12-in.jsonld"
  }
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/so12-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid context entry");}

// @import can only reference a single context
//
// @import can only reference a single context.
#[tokio::test]
async fn tso13() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "@import": "so13-context.jsonld"
  },
  "term": "value"
}
"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/so13-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid remote context");}

// @type: @none is illegal in 1.0.
//
// @type: @none is illegal in json-ld-1.0.
#[tokio::test]
async fn ttn01() {
    let input = r###"{
  "@context": {
    "xsd": "http://www.w3.org/2001/XMLSchema#",
    "notype": {"@id": "http://example.com/notype", "@type": "@none"}
  },
  "notype": [
    "string",
    true,
    false,
    1,
    10.0,
    {"@value": "plain"},
    {"@value": true, "@type": "xsd:boolean"},
    {"@value": "english", "@language": "en"},
    {"@value": "2018-02-17", "@type": "xsd:date"},
    {"@id": "http://example.com/iri"}
  ]
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/tn01-in.jsonld").await;
    assert_eq!(result.expect_err("This test is supposed to fail").error_code(), "invalid type mapping");}

// @type: @none expands strings as value objects
//
// @type: @none leaves inputs other than strings alone
#[tokio::test]
async fn ttn02() {
    let input = r###"{
  "@context": {
    "@version": 1.1,
    "xsd": "http://www.w3.org/2001/XMLSchema#",
    "notype": {"@id": "http://example.com/notype", "@type": "@none"}
  },
  "notype": [
    "string",
    true,
    false,
    1,
    10.0,
    {"@value": "plain"},
    {"@value": true, "@type": "xsd:boolean"},
    {"@value": "english", "@language": "en"},
    {"@value": "2018-02-17", "@type": "xsd:date"},
    {"@id": "http://example.com/iri"}
  ]
}"###;
    let result = crate::tests::expand_base(input, "https://w3c.github.io/json-ld-api/tests/expand/tn02-in.jsonld").await;
    assert_eq!(result.expect("This test should be Ok"), serde_json::from_str::<serde_json::Value>(r###"[{
  "http://example.com/notype": [
    {"@value": "string"},
    {"@value": true},
    {"@value": false},
    {"@value": 1},
    {"@value": 10.0},
    {"@value": "plain"},
    {"@value": true, "@type": "http://www.w3.org/2001/XMLSchema#boolean"},
    {"@value": "english", "@language": "en"},
    {"@value": "2018-02-17", "@type": "http://www.w3.org/2001/XMLSchema#date"},
    {"@id": "http://example.com/iri"}
  ]
}]"###).expect("Invalid output JSON"));}

