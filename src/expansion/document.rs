use futures_util::future::{BoxFuture, FutureExt};
use iref::IriBuf;
use std::collections::HashMap;
use std::str::FromStr;

use crate::context::processing::{Context, ContextProcessingError, context_processing, async_fold};
use crate::expansion::value::expand_value;
use crate::expansion::iri::{ExpandedIri, IriExpandable};
use crate::types::Keyword;
use crate::types::TypeMapping;
use crate::types::LdValue;

#[derive(Debug)]
pub enum DocumentExpansionError {
    InvalidReversePropertyMap,
    CollidingKeyword,
    InvalidIdValue,
    InvalidTypeValue,
    InvalidIncludedValue,
    ContextProcessing(ContextProcessingError),
    InvalidValueObjectValue,
    InvalidLanguageTaggedString,
    InvalidBaseDirection,
    InvalidReverseValue,
    InvalidIndexValue,
    InvalidReversePropertyValue,
    InvalidLanguageMapValue,
    InvalidValueObject,
    InvalidLanguageTaggedValue,
}

impl DocumentExpansionError {
    pub(crate) fn error_code(&self) -> &'static str {
        use DocumentExpansionError::*;
        match *self {
            InvalidReversePropertyMap => "invalid reverse property map",
            CollidingKeyword => "colliding keyword",
            InvalidIdValue => "invalid @value",
            InvalidTypeValue => "invalid @type value",
            InvalidIncludedValue => "invalid @included value",
            ContextProcessing(ref e) => e.error_code(),
            InvalidValueObjectValue => "invalid @value object",
            InvalidLanguageTaggedString => "invalid language-tagged string",
            InvalidBaseDirection => "invalid base direction",
            InvalidReverseValue => "invalid @reverse value",
            InvalidIndexValue => "invalid @index value",
            InvalidReversePropertyValue => "invalid reverse property value",
            InvalidLanguageMapValue => "invalid language map value",
            InvalidValueObject => "invalid value object",
            InvalidLanguageTaggedValue => "invalid language-tagged value",
        }
    }
}

impl From<ContextProcessingError> for DocumentExpansionError {
    fn from(e: ContextProcessingError) -> DocumentExpansionError {
        DocumentExpansionError::ContextProcessing(e)
    }
}

pub fn expand_document<'a>(
    active_ctx: Context,
    active_prop: Option<String>,
    element: serde_json::Value,
    base_url: IriBuf,
    frame_expansion: Option<bool>,
    ordered: Option<bool>,
    from_map: Option<bool>,
) -> BoxFuture<'a, Result<(LdValue, Context), DocumentExpansionError>> {
    async move {
        let frame_expansion = frame_expansion.unwrap_or(false) && active_prop != Some("@default".to_owned());
        let ordered = ordered.unwrap_or(false);
        let from_map = from_map.unwrap_or(false);

        let property_scoped_context = element.as_object().and_then(|o| o.get("@context"));

        Ok(match element {
            serde_json::Value::Null => (LdValue::Null, active_ctx),
            serde_json::Value::String(_) | serde_json::Value::Number(_) |
            serde_json::Value::Bool(_) => match active_prop {
                None => (LdValue::Null, active_ctx),
                Some(x) if x == "@graph".to_owned() => (LdValue::Null, active_ctx),
                Some(active_prop) => {
                    let active_ctx = if let Some(local_ctx) = property_scoped_context {
                        let base_url = active_ctx.term_defs[&active_prop].clone().base_url.expect("TODO");
                        context_processing(
                            active_ctx,
                            local_ctx.clone(),
                            base_url,
                            None,
                            None,
                            None
                        )
                        .await
                        .expect("TODO")
                    } else {
                        active_ctx
                    };
                    
                    expand_value(active_ctx, active_prop, element).await
                }
            },
            serde_json::Value::Array(arr) => {
                let is_list = active_ctx.term_defs.get(&active_prop.clone().unwrap_or_default())
                    .map(|x| x.container_mapping.contains(&Keyword::List))
                    .unwrap_or(false);
                let (r, c, _, _) = async_fold(arr.into_iter(), Ok((Vec::new(), active_ctx, base_url, active_prop)), async move |state: Result<_, DocumentExpansionError>, item| {
                    if let Ok((mut result, active_ctx, base_url, active_prop)) = state {
                        let (expanded_item, active_ctx) = expand_document(
                            active_ctx,
                            active_prop.clone(),
                            item,
                            base_url.clone(),
                            Some(frame_expansion),
                            Some(ordered),
                            Some(from_map)
                        ).await?;

                        if is_list {
                            result.push(LdValue::from(
                                (ExpandedIri::Keyword(Keyword::List), expanded_item)
                            ));
                        } else if let LdValue::Array(arr) = expanded_item {
                            result.extend(arr.clone());
                        } else {
                            result.push(expanded_item);
                        }

                        Ok((result, active_ctx, base_url, active_prop))
                    } else {
                        state
                    }
                }).await?;
                (LdValue::Array(r), c)
            },
            serde_json::Value::Object(ref obj) => {
                // 7
                let active_ctx = if !from_map {
                    if let Some(prev) = active_ctx.previous_context {
                        *prev
                    } else {
                        active_ctx
                    }
                } else {
                    active_ctx
                };

                // 8
                let active_ctx = if let Some(scoped_ctx) = property_scoped_context {
                    let base_url = active_prop.as_ref().and_then(|prop| active_ctx.term_defs.get(prop))
                        .and_then(|def| def.base_url.clone())
                        .unwrap_or(base_url.clone());
                    context_processing(
                        active_ctx,
                        scoped_ctx.clone(),
                        base_url,
                        None,
                        None,
                        None,
                    ).await?
                } else {
                    active_ctx
                };

                // 9
                let active_ctx = if let Some(ctx) = obj.get("@context") {
                    let base_url = active_prop.as_ref().and_then(|prop| active_ctx.term_defs.get(prop))
                        .and_then(|def| def.base_url.clone())
                        .unwrap_or(base_url.clone());
                    context_processing(
                        active_ctx,
                        ctx.clone(),
                        base_url,
                        None,
                        None,
                        None,
                    ).await?
                } else {
                    active_ctx
                };

                // 10
                let type_scoped_context = active_ctx.clone();
                let input_type: Option<serde_json::Value> = None;

                // TODO: order key
                let (input_type, type_scoped_context, active_ctx) = async_fold(
                        obj.iter(),
                        Ok((
                            input_type,
                            type_scoped_context,
                            active_ctx,
                        )),
                        async move |res: Result<_, DocumentExpansionError>, (k, v)| {
                            if let Ok((input_type, type_scoped_context, active_ctx)) = res {
                                let (expanded_k, active_ctx) = k.expand_to_iri(active_ctx, None, None, None, None).await;
                                let res = if expanded_k == ExpandedIri::Keyword(Keyword::Type) {
                                    let input_type = input_type.unwrap_or(v.clone());
                                    // 11.1
                                    let v = match v {
                                        serde_json::Value::Null => vec![],
                                        serde_json::Value::Array(a) => a.clone(),
                                        x => vec![ x.clone() ],
                                    };

                                    // 11.2
                                    let (active_ctx, type_scoped_context) = async_fold(
                                        v.iter(),
                                        Ok((active_ctx, type_scoped_context)),
                                        async move |res: Result<_, DocumentExpansionError>, term| {
                                            if let Ok((active_ctx, type_scoped_context)) = res {
                                                if let Some(term) = term.as_str() {
                                                    if let Some(local_ctx) = type_scoped_context.term_defs.get(term)
                                                        .and_then(|x| if x.local_ctx.is_null() { None} else { Some(x.local_ctx.clone()) }) {
                                                        let bu = active_ctx.term_defs.get(term).expect("TODO").clone().base_url.expect("TODO");
                                                        return Ok((context_processing(
                                                            active_ctx,
                                                            local_ctx,
                                                            bu,
                                                            None,
                                                            None,
                                                            None
                                                        ).await.expect("TODO"), type_scoped_context));
                                                    }
                                                }
                                                Ok((active_ctx, type_scoped_context))
                                            } else {
                                                res
                                            }
                                        }).await?;

                                    (Some(input_type), type_scoped_context, active_ctx)
                                } else {
                                    (input_type, type_scoped_context, active_ctx)
                                };
                                Ok(res)
                            } else {
                                res
                            }
                        }).await?;

                // 12
                let result: HashMap<ExpandedIri, LdValue> = HashMap::new();

                // 13
                let mut key_vals: Vec<_> = obj.into_iter().collect();
                if ordered {
                    key_vals.sort_by_key(|(k, _)| k.clone());
                }
                let (mut result, active_ctx, active_prop, input_type, base_url, type_scoped_context, nests) = async_fold(
                    key_vals.into_iter(),
                    Ok((
                        result,
                        active_ctx,
                        active_prop,
                        input_type,
                        base_url,
                        type_scoped_context,
                        Vec::new(),
                    )),
                    async move |state: Result<_, DocumentExpansionError>, (k, v)| {
                        if let Ok((mut result, active_ctx, active_prop, input_type, base_url, type_scoped_context, mut nests)) = state {
                            if *k == "@context".to_owned() {
                                return Ok((result, active_ctx, active_prop, input_type, base_url, type_scoped_context, nests));
                            }

                            let (expanded_prop, active_ctx) = k.expand_to_iri(active_ctx, Some(false), Some(true), None, None).await;
                            let (active_ctx, type_scoped_context) = match expanded_prop.clone() {
                                ExpandedIri::Keyword(kw) => {
                                    if active_prop == Some("@reverse".to_owned()) {
                                        return Err(DocumentExpansionError::InvalidReversePropertyMap);
                                    }

                                    if result.contains_key(&ExpandedIri::Keyword(kw)) && kw != Keyword::Type && kw != Keyword::Included {
                                        return Err(DocumentExpansionError::CollidingKeyword);
                                    }

                                    let (expanded_value, active_ctx, type_scoped_context) = match kw {
                                        // 13.4.3
                                        Keyword::Id => {
                                            if v.as_str().is_some() || (frame_expansion && (is_string_array(&v) || is_empty_map(&v))) {
                                                let (iri, ctx) = v.expand_to_iri(active_ctx, Some(true), Some(false), None, None).await;
                                                (iri.to_json_ld(), ctx, type_scoped_context)
                                            } else {
                                                return Err(DocumentExpansionError::InvalidIdValue);
                                            }    
                                        },
                                        Keyword::Type => if v.as_str().is_some() {
                                            let (iri, ts_ctx) = v.expand_to_iri(type_scoped_context, Some(true), None, None, None).await;
                                            (iri.to_json_ld(), active_ctx, ts_ctx)
                                        } else if is_string_array(&v) {
                                            let (list, type_scoped_context) = async_fold(
                                                v.as_array().expect("v is a string array but it is not a string").iter(),
                                                (Vec::new(), type_scoped_context),
                                                async move |(mut list, type_scoped_context), x| {
                                                    let (iri, ts_ctx) = x.expand_to_iri(type_scoped_context, Some(true), None, None, None).await;
                                                    list.push(iri.to_json_ld());
                                                    (list, ts_ctx)
                                                }
                                            ).await;
                                            (LdValue::Array(list), active_ctx, type_scoped_context)
                                        } else if frame_expansion && is_empty_map(&v) {
                                            (LdValue::Object(HashMap::new()), active_ctx, type_scoped_context)
                                        } else if frame_expansion && is_default_object(&v) && is_iri(&v["@default"]) {
                                            let (iri, active_ctx) = v.expand_to_iri(type_scoped_context.clone(), Some(true), None, None, None).await;
                                            (LdValue::from(
                                                (ExpandedIri::Keyword(Keyword::Default), iri.to_json_ld()),
                                            ), active_ctx, type_scoped_context)
                                        } else {
                                            return Err(DocumentExpansionError::InvalidTypeValue);
                                        }, // TODO: 13.4.4.5
                                        Keyword::Graph => {
                                            // TODO: "ensuring that expanded value is an array of one or more maps."
                                            let (graph, active_ctx) = expand_document(
                                                active_ctx,
                                                Some("@graph".into()),
                                                v.clone(),
                                                base_url.clone(),
                                                Some(frame_expansion),
                                                Some(ordered),
                                                None
                                            ).await?;
                                            (graph, active_ctx, type_scoped_context)
                                        },
                                        Keyword::Included => {
                                            let (expanded_value, active_ctx) = expand_document(
                                                active_ctx,
                                                Some("@graph".into()),
                                                v.clone(),
                                                base_url.clone(),
                                                Some(frame_expansion),
                                                Some(ordered),
                                                None
                                            ).await?; 
                                            if let serde_json::Value::Array(arr) = expanded_value {
                                                if arr.iter().any(is_node_object) {
                                                    return Err(DocumentExpansionError::InvalidIncludedValue);
                                                } else {
                                                    (serde_json::Value::Array(arr), active_ctx, type_scoped_context)
                                                }
                                            } else {
                                                return Err(DocumentExpansionError::InvalidIncludedValue);
                                            }
                                        },
                                        Keyword::Value => {
                                            if input_type == Some(serde_json::Value::String("@json".into())) {
                                                (v.clone(), active_ctx, type_scoped_context)
                                            } else {
                                                (match v {
                                                    serde_json::Value::Object(_) | serde_json::Value::Array(_) if !frame_expansion => return Err(DocumentExpansionError::InvalidValueObjectValue),
                                                    serde_json::Value::Object(obj) => {
                                                        if obj.len() == 0 {
                                                            v.clone()
                                                        } else {
                                                            return Err(DocumentExpansionError::InvalidValueObjectValue)
                                                        }
                                                    },
                                                    serde_json::Value::Array(arr) => {
                                                        if arr.iter().all(is_scalar) {
                                                            v.clone()
                                                        } else {
                                                            return Err(DocumentExpansionError::InvalidValueObjectValue)
                                                        }
                                                    }
                                                    x => x.clone()
                                                }, active_ctx, type_scoped_context)
                                            }
                                        },
                                        Keyword::Language => {
                                            if !v.is_string() || !(frame_expansion && (is_empty_map(&v) || is_string_array(&v))) {
                                                return Err(DocumentExpansionError::InvalidLanguageTaggedString);
                                            }
                                            // TODO: issue a warning if the value is not well formed
                                            // TODO: normalize the value to lowercase
                                            let expanded_value = v.clone();
                                            (expanded_value, active_ctx, type_scoped_context)
                                        },
                                        Keyword::Direction => {
                                            // TODO: If processing mode is json-ld-1.0, continue with the next key from element.
                                            // TODO: at this point I'm tired of handling frame_expansion,
                                            // so let's say it is false for the moment…
                                            let expanded_value = match v.as_str() {
                                                Some(x) => super::super::types::Direction::from_str(x).ok(),
                                                None => None,
                                            }.ok_or(DocumentExpansionError::InvalidBaseDirection)?;
                                            (serde_json::Value::String(expanded_value.to_string()), active_ctx, type_scoped_context)
                                        },
                                        Keyword::Index => {
                                            if let Some(v) = v.as_str() {
                                                let expanded_value = v;
                                                (serde_json::Value::String(expanded_value.to_owned()), active_ctx, type_scoped_context)
                                            } else {
                                                return Err(DocumentExpansionError::InvalidIndexValue);
                                            }
                                        },
                                        Keyword::List => {
                                            if active_prop == Some("@graph".into()) || active_prop.is_none() {
                                                return Ok((result, active_ctx, active_prop, input_type, base_url.clone(), type_scoped_context, nests));
                                            } else {
                                                let (res, active_ctx) = expand_document(
                                                    active_ctx,
                                                    active_prop.clone(),
                                                    v.clone(),
                                                    base_url.clone(),
                                                    Some(frame_expansion),
                                                    Some(ordered),
                                                    None,
                                                ).await?;
                                                (res, active_ctx, type_scoped_context)
                                            }
                                        },
                                        Keyword::Set => {
                                            let (res, active_ctx) = expand_document(
                                                active_ctx,
                                                active_prop.clone(),
                                                v.clone(),
                                                base_url.clone(),
                                                Some(frame_expansion),
                                                Some(ordered),
                                                None,
                                            ).await?;
                                            (res, active_ctx, type_scoped_context)
                                        },
                                        Keyword::Reverse => {
                                            if let Some(obj) = v.as_object() {
                                                let (expanded_value, active_ctx) = expand_document(
                                                    active_ctx,
                                                    Some("@reverse".to_owned()),
                                                    v.clone(),
                                                    base_url.clone(),
                                                    Some(frame_expansion),
                                                    Some(ordered),
                                                    None,
                                                ).await?;

                                                if expanded_value.get("@reverse").is_some() {
                                                    for (k, v) in expanded_value.as_object().unwrap() {
                                                        // TODO: not actually a blank node ident
                                                        add_value(&mut result, ExpandedIri::BlankNodeIdent(k), v, Some(true));
                                                    }
                                                } else if expanded_value.as_object().map(|x| x.keys().len() > 0).unwrap_or(false) {
                                                    let empty_map = serde_json::json!({});
                                                    let mut reverse_map = result.get(&ExpandedIri::Keyword(Keyword::Reverse)).unwrap_or(&empty_map).clone();

                                                    for (k, v) in expanded_value.as_object().unwrap() {
                                                        if k != "@reverse" {
                                                            if v.get("@value").is_some() || v.get("@list").is_some() {
                                                                return Err(DocumentExpansionError::InvalidReversePropertyValue);
                                                            }
                                                            // TODO: not actually a blank node
                                                            // ident
                                                            add_value(&mut reverse_map, ExpandedIri::BlankNodeIdent(k.clone()), v.clone(), Some(true));
                                                        }
                                                    }
                                                    result.insert(ExpandedIri::Keyword(Keyword::Reverse), reverse_map.clone());
                                                }

                                                return Ok((result, active_ctx, active_prop, input_type, base_url, type_scoped_context, nests));
                                            } else {
                                                return Err(DocumentExpansionError::InvalidReverseValue);
                                            }
                                        },
                                        Keyword::Nest => {
                                            nests.push(k);
                                            return Ok((result, active_ctx, active_prop, input_type, base_url, type_scoped_context, nests));
                                        },
                                        // 13.4.17
                                        // If it is another keyword, we continue with the next property
                                        _ => return Ok((result, active_ctx, active_prop, input_type, base_url, type_scoped_context, nests)), 
                                    };
                                    result.insert(ExpandedIri::Keyword(kw), expanded_value);
                                    (active_ctx, type_scoped_context)
                                },
                                // Other cases are handled later, at the end of step 13
                                _ => (active_ctx, type_scoped_context) // skip this property
                            };
                            
                            let container_mapping = active_ctx.term_defs.get(k).map(|x| x.container_mapping.clone()).unwrap_or_default();
                            let (expanded_value, active_ctx, container_mapping) = if let Some(TypeMapping::Json) = active_ctx.term_defs.get(k).map(|x| x.type_mapping.clone()) {
                                (serde_json::json!({
                                    "@value": v,
                                    "@type": "@json",
                                }), active_ctx, container_mapping)
                            } else if container_mapping.contains(&Keyword::Language) && v.is_object() {
                                let mut exp_val = vec![];
                                let dir = active_ctx.term_defs.get(k)
                                    .and_then(|d| d.direction_mapping.clone())
                                    .or(active_ctx.default_base_direction.clone());
                                // TODO: order by language if ordered is true
                                for (lang, lang_val) in v.as_object().expect("is_object(), but as_object() fails") {
                                    let lang_val = match lang_val {
                                        serde_json::Value::Array(arr) => arr.clone(),
                                        x => vec![x.clone()],
                                    };

                                    for item in lang_val {
                                        match item {
                                            serde_json::Value::Null => continue,
                                            serde_json::Value::String(s) => {
                                                let mut res = serde_json::Map::new();
                                                res.insert("@language".to_owned(), serde_json::Value::String(lang.clone()));
                                                res.insert("@value".to_owned(), serde_json::Value::String(s.clone()));

                                                let expanded_lang = lang; // TODO: actual expansion
                                                if expanded_lang == "@none" {
                                                    res.remove("@language");
                                                }
                                                if let Some(d) = dir.clone() {
                                                    res.insert("@direction".to_owned(), serde_json::Value::String(d.to_string()));
                                                }
                                                exp_val.push(serde_json::Value::Object(res));
                                            },
                                            _ => return Err(DocumentExpansionError::InvalidLanguageMapValue)
                                        }
                                    }
                                }

                                (serde_json::Value::Array(exp_val), active_ctx, container_mapping)
                            } else if (
                                container_mapping.contains(&Keyword::Index) ||
                                container_mapping.contains(&Keyword::Type) ||
                                container_mapping.contains(&Keyword::Id)) && v.is_object() {
                                let index_key = active_ctx.term_defs.get(k)
                                    .and_then(|x| if !x.index_mapping.is_null() { Some(x.index_mapping.clone()) } else { None })
                                    .unwrap_or(ExpandedIri::Keyword(Keyword::Index));

                                // TODO: order by "index" if ordered is true
                                let (active_ctx, index_key, base_url, container_mapping, expanded_value) = async_fold(
                                    v.as_object().expect("is_object(), but as_object() fails").iter(),
                                    Ok((active_ctx, index_key, base_url.clone(), container_mapping, Vec::new())),
                                    async move |res: Result<_, DocumentExpansionError>, (index, index_val)| {
                                        if let Ok((active_ctx, index_key, base_url, container_mapping, expanded_value)) = res {
                                            let map_context = if container_mapping.contains(&Keyword::Id) || container_mapping.contains(&Keyword::Type) {
                                                active_ctx.clone().previous_context.map(|x| *x).unwrap_or(active_ctx.clone())
                                            } else {
                                                active_ctx.clone()
                                            };

                                            let map_context = if container_mapping.contains(&Keyword::Type) &&
                                                map_context.term_defs.get(index).map(|d| !d.local_ctx.is_null()).unwrap_or(false) {
                                                let index_def = map_context.term_defs[index].clone();
                                                context_processing(
                                                    map_context,
                                                    index_def.local_ctx,
                                                    index_def.base_url.expect("TODO"),
                                                    None,
                                                    None,
                                                    None
                                                ).await?
                                            } else {
                                                map_context
                                            };

                                            let (expanded_index, active_ctx) = index.expand_to_iri(
                                                active_ctx,
                                                None,
                                                None,
                                                None,
                                                None
                                            ).await;
                                            let index_val = match index_val {
                                                serde_json::Value::Array(arr) => arr.clone(),
                                                x => vec![ x.clone() ],
                                            };
                                            let (index_val, map_context) = expand_document(
                                                map_context,
                                                Some(k.clone()),
                                                serde_json::Value::Array(index_val),
                                                base_url.clone(),
                                                Some(frame_expansion),
                                                Some(ordered),
                                                None
                                            ).await?;

                                            let (container_mapping, index_key, active_ctx, expanded_index, expanded_value) = async_fold(
                                                index_val.as_array().expect("Document expansion returned something else that an array").iter(),
                                                Ok((container_mapping, index_key, active_ctx, expanded_index, expanded_value)),
                                                async move |res, item| {
                                                    if let Ok((container_mapping, index_key, active_ctx, expanded_index, mut expanded_value)) = res {
                                                        let mut item = if container_mapping.contains(&Keyword::Graph) && !is_graph_object(&item) {
                                                            serde_json::json!({
                                                                "@graph": item.as_array().map(Clone::clone).unwrap_or_else(|| vec![ item.clone() ]),
                                                            })
                                                        } else {
                                                            item.clone()
                                                        };

                                                        let active_ctx = if container_mapping.contains(&Keyword::Index) &&
                                                            index_key.to_string().unwrap() != "@index" &&
                                                            expanded_index.to_string() != Some("@none".to_owned()) {
                                                            let (re_expanded_index, active_ctx) = expand_value(
                                                                active_ctx,
                                                                index_key.to_string().unwrap(),
                                                                serde_json::Value::String(index.clone())
                                                            ).await;
                                                            let re_expanded_index = re_expanded_index.as_array().expect("Expanded value is not an array").clone();
                                                            
                                                            // Apparently (13.8.3.7.2) this should be IRI expanded, but according to the types,
                                                            // it already is…
                                                            let expanded_index_key = index_key.clone(); 
                                                            
                                                            let mut index_prop_values = re_expanded_index;
                                                            index_prop_values.append(
                                                                &mut item.get(index_key.to_string().unwrap())
                                                                    .and_then(|x| x.as_array().map(|a| a.clone()))
                                                                    .unwrap_or_default()
                                                            );

                                                            item[expanded_index_key.to_string().unwrap()] = serde_json::Value::Array(index_prop_values.clone());
                                                            if is_value_object(&item) {
                                                                return Err(DocumentExpansionError::InvalidValueObject);
                                                            }
                                                            active_ctx
                                                        } else if container_mapping.contains(&Keyword::Index) && 
                                                            !item.get("@index").is_some() &&
                                                            expanded_index != ExpandedIri::Keyword(Keyword::None) {
                                                            item["@index"] = serde_json::Value::String(index.clone());
                                                            active_ctx
                                                        } else if container_mapping.contains(&Keyword::Id) && 
                                                            !item.get("@id").is_some() &&
                                                            expanded_index != ExpandedIri::Keyword(Keyword::None) {
                                                            let (expanded_index, active_ctx) = index.expand_to_iri(active_ctx, Some(true), Some(false), None, None).await;
                                                            item["@id"] = expanded_index.to_json();
                                                            active_ctx
                                                        } else if container_mapping.contains(&Keyword::Type) && expanded_index != ExpandedIri::Keyword(Keyword::None) {
                                                            let mut types = match item.get("@type").and_then(|t| t.as_array()) {
                                                                Some(x) => x.clone(),
                                                                None => Vec::with_capacity(1),
                                                            };
                                                            types.push(expanded_index.to_json());
                                                            item["@type"] = serde_json::Value::Array(types);
                                                            active_ctx
                                                        } else {
                                                            active_ctx
                                                        };

                                                        expanded_value.push(item);
                                                        Ok((container_mapping, index_key, active_ctx, expanded_index, expanded_value))
                                                    } else {
                                                        res
                                                    }
                                                }).await?;
                                            Ok((active_ctx, index_key, base_url, container_mapping, expanded_value))
                                        } else {
                                            res
                                        }
                                    }).await?;

                                (serde_json::Value::Array(expanded_value), active_ctx, container_mapping)
                            } else {
                                let (r, c) = expand_document(active_ctx, Some(k.clone()), v.clone(), base_url.clone(), Some(frame_expansion), Some(ordered), None).await?;
                                (r, c, container_mapping)
                            };

                            if expanded_value == serde_json::Value::Null {
                                return Ok((result, active_ctx, active_prop, input_type, base_url, type_scoped_context, nests));
                            }

                            let expanded_value = if container_mapping.contains(&Keyword::List) && !is_list_object(&expanded_value) {
                                serde_json::json!({
                                    "@list": expanded_value.as_array().map(Clone::clone).unwrap_or_else(|| vec![ expanded_value.clone() ]),
                                })
                            } else {
                                expanded_value
                            };

                            let expanded_value = if container_mapping.contains(&Keyword::Graph) &&
                                !container_mapping.contains(&Keyword::Id) &&
                                !container_mapping.contains(&Keyword::Index) {
                                serde_json::Value::Array(
                                    expanded_value.as_array()
                                        .map(Clone::clone)
                                        .unwrap_or_else(|| vec![ expanded_value.clone() ])
                                        .iter()
                                        .map(|ev| serde_json::json!({
                                            "@graph": ev.as_array().map(Clone::clone).unwrap_or_else(|| vec![ ev.clone() ])
                                        }))
                                        .collect()
                                )
                            } else {
                                expanded_value
                            };

                            if active_ctx.term_defs.get(k).map(|d| d.reverse_property).unwrap_or(false) {
                                let reverse_map = result.entry(ExpandedIri::Keyword(Keyword::Reverse)).or_insert(serde_json::json!({}));
                                let expanded_value = expanded_value.as_array()
                                    .map(|x| x.clone())
                                    .unwrap_or_else(|| vec![ expanded_value.clone() ]);
                                for item in expanded_value {
                                    if is_value_object(&item) || is_list_object(&item) {
                                        return Err(DocumentExpansionError::InvalidReversePropertyValue);
                                    }

                                    match reverse_map {
                                        serde_json::Value::Object(map) => {
                                            let mut arr = map.get(&expanded_prop.to_string().unwrap())
                                                .and_then(|x| x.as_array().map(|x| x.clone())).unwrap_or_default();
                                            arr.push(item.clone());
                                            *reverse_map = serde_json::json!({
                                                expanded_prop.to_string().unwrap(): arr,
                                            });
                                        },
                                        _ => {}
                                    }
                                }

                            } else {
                                // 13.14
                                add_value(&mut result, expanded_prop, expanded_value, Some(true));
                            }

                            Ok((result, active_ctx, active_prop, input_type, base_url, type_scoped_context, nests))
                        } else {
                            state
                        }
                    }).await?;

                // 14
                for nesting_key in nests {
                    // TODO: extract 13 and 14 in a function
                }

                // 15
                if result.contains_key(&ExpandedIri::Keyword(Keyword::Value)) {
                    let legal_keys = [
                        ExpandedIri::Keyword(Keyword::Direction),
                        ExpandedIri::Keyword(Keyword::Index),
                        ExpandedIri::Keyword(Keyword::Language),
                        ExpandedIri::Keyword(Keyword::Type),
                        ExpandedIri::Keyword(Keyword::Value),
                    ];
                    if !result.keys().all(|k| legal_keys.contains(k)) ||
                        (result.contains_key(&ExpandedIri::Keyword(Keyword::Type)) && (
                                result.contains_key(&ExpandedIri::Keyword(Keyword::Language)) ||
                                result.contains_key(&ExpandedIri::Keyword(Keyword::Direction))
                        )) {
                        return Err(DocumentExpansionError::InvalidValueObject);
                    }

                    if !result.get(&ExpandedIri::Keyword(Keyword::Json)).and_then(|x| x.as_str()).map(|x| x == "@json").unwrap_or(false) {
                        match result[&ExpandedIri::Keyword(Keyword::Value)] {
                            serde_json::Value::Null => return Ok((serde_json::Value::Null, active_ctx)),
                            serde_json::Value::Array(ref arr) if arr.is_empty() => return Ok((serde_json::Value::Null, active_ctx)),
                            serde_json::Value::String(_) => {},
                            _ if result.contains_key(&ExpandedIri::Keyword(Keyword::Language)) => return Err(DocumentExpansionError::InvalidLanguageTaggedValue),
                            _ => {},
                        }

                        if let Some(typ) = result.get(&ExpandedIri::Keyword(Keyword::Type)) {
                            if !is_iri(typ) {
                                return Err(DocumentExpansionError::InvalidTypeValue);
                            }
                        }
                    }
                }

                // 16
                result.entry(ExpandedIri::Keyword(Keyword::Type))
                    .and_modify(|t| if !t.is_array() {
                        *t = serde_json::Value::Array(vec![ t.clone() ]);
                    });

                // TODO: 17
                
                // 18
                if result.keys().all(|k| *k == ExpandedIri::Keyword(Keyword::Language)) {
                    return Ok((serde_json::Value::Null, active_ctx));
                }

                if active_prop.map(|p| p == "@graph").unwrap_or(true) {
                    if result.len() == 0 || result.keys().all(|k| *k == ExpandedIri::Keyword(Keyword::Value) || *k == ExpandedIri::Keyword(Keyword::List)) {
                        return Ok((serde_json::Value::Null, active_ctx));
                    } else if !frame_expansion && result.keys().all(|k| *k == ExpandedIri::Keyword(Keyword::Id)) {
                        return Ok((serde_json::Value::Null, active_ctx));
                    }
                }

                let mut obj = serde_json::Map::new();
                for (k, v) in result {
                    if let Some(k) = k.to_string() {
                        obj.insert(k, v);
                    }
                }
                (serde_json::Value::Object(obj), active_ctx)
            },
        })
    }.boxed()
}

fn is_iri(v: &serde_json::Value) -> bool {
    match v {
        serde_json::Value::String(s) => IriBuf::new(s).is_ok(),
        _ => false,
    }
}

fn is_empty_map(v: &serde_json::Value) -> bool {
    v.as_object().map(|o| o.len() == 0).unwrap_or(false)
}

fn is_default_object(v: &serde_json::Value) -> bool {
    v.as_object().map(|o| o.len() == 1 && is_iri(&o["@default"])).unwrap_or(false)
}

fn is_string_array(v: &serde_json::Value) -> bool {
    v.as_array().map(|a| a.iter().all(|x| x.as_str().is_some())).unwrap_or(false)
}

fn is_scalar(v: &serde_json::Value) -> bool {
    !(v.is_object() || v.is_array())
}

/// From [the spec](https://www.w3.org/TR/json-ld11/#dfn-node-object):
///
/// > A node object represents zero or more properties of a node in the graph serialized
/// > by the JSON-LD document. A map is a node object if it exists outside of the
/// > JSON-LD context and:
/// > 
/// > - it does not contain the @value, @list, or @set keywords, or
/// > - it is not the top-most map in the JSON-LD document consisting of no other
/// >  entries than @graph and @context.
/// >
/// > The entries of a node object whose keys are not keywords are also called properties
/// > of the node object. See the Node Objects section of JSON-LD 1.1 for a normative description.
///
/// This function considers that `v` is not a map from the context.
fn is_node_object(v: &serde_json::Value) -> bool {
    // TODO: the top most map thing
    match v {
        serde_json::Value::Object(ref obj) => {
            !obj.contains_key("@value") && !obj.contains_key("@list") && !obj.contains_key("@set")
        },
        _ => false,
    }
}

/// Tell is a value is a graph object or not.
///
/// Definition from [the spec](https://www.w3.org/TR/json-ld11/#dfn-graph-object):
///
/// > A graph object represents a named graph as the value of a map entry within a node object.
/// > When expanded, a graph object must have an @graph entry, and may also have @id, and
/// > @index entries. A simple graph object is a graph object which does not have an @id entry.
/// > Note that node objects may have a @graph entry, but are not considered graph objects if they
/// > include any other entries. A top-level object consisting of @graph is also not a graph
/// > object. Note that a node object may also represent a named graph it it includes other
/// > properties. See the Graph Objects section of JSON-LD 1.1 for a normative description.
///
/// Everything after "Note that…" is not yet implemented by this function.
fn is_graph_object(v: &serde_json::Value) -> bool {
    match v {
        serde_json::Value::Object(o) => {
            o.contains_key("@graph") &&
            o.keys().all(|k| k == "@graph" || k == "@id" || k == "@index")
        },
        _ => false,
    }
}

fn is_value_object(v: &serde_json::Value) -> bool {
    // TODO
    false
}

/// Tells if a JSON value is a list object or not.
///
/// Definition from [the spec](https://www.w3.org/TR/json-ld11/#dfn-list-object):
///
/// > A list object is a map that has a @list key. It may also have an @index key,
/// > but no other entries.
fn is_list_object(v: &serde_json::Value) -> bool {
    match v {
        serde_json::Value::Object(o) => {
            o.contains_key("@list") &&
            o.keys().all(|k| k == "@list" || k == "@index")
        },
        _ => false,
    }
}

/// https://www.w3.org/TR/json-ld11-api/#dfn-add-value
/// 
/// This function doesn't modify the map, but returns the value to be added.
fn add_value(map: &HashMap<ExpandedIri, serde_json::Value>, key: ExpandedIri, val: serde_json::Value, as_array: Option<bool>) {
    let as_array = as_array.unwrap_or(false);
    // 1
    let orig_val = match map.get(key) {
        Some(v @ serde_json::Value::Array(_)) => v,
        Some(x) if as_array => serde_json::Value::Array(vec![ x ]),
        Some(x) => x,
        None => serde_json::Value::Array(Vec::new()),
    };

    // 2
    match val {
        serde_json::Value::Array(arr) => {
            for v in arr {
                add_value(&mut map, key, v, Some(as_array));
            }
        }
        _ => match map.get(key) {
            None => {
                map.insert(key, val);
            },
            Some(entry) => {
                let mut new_val = match entry {
                    serde_json::Value::Array(arr) => arr,
                    x => serde_json::Value::Array(vec![ x ]),
                };

                new_val.push(val);

                map.insert(key, new_val);
            }
        }
    }
}
