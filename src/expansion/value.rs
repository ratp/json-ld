use futures_util::future::{BoxFuture, FutureExt};

use crate::types::*;
use crate::context::processing::Context;
use crate::expansion::iri::IriExpandable;

pub fn expand_value<'a>(
    active_ctx: Context,
    active_prop: String,
    to_expand: serde_json::Value,
) -> BoxFuture<'a, (LdValue, Context)> {
    async move {
        let type_mapping = active_ctx.term_defs.get(&active_prop).map(|x| x.type_mapping.clone());
        match (to_expand, type_mapping) {
            (serde_json::Value::String(val), Some(TypeMapping::Id)) => {
                let (expanded, active_ctx) = val.expand_to_iri(active_ctx, Some(true), Some(false), None, None).await;
                (LdValue::from(&[
                    (ExpandedIri::Keyword(Keyword::Id), expanded.to_json_ld())
                ]), active_ctx)
            },
            (serde_json::Value::String(val), Some(TypeMapping::Vocab)) => {
                let (expanded, active_ctx) = val.expand_to_iri(active_ctx, Some(true), None, None, None).await;
                (LdValue::from(&[
                    (ExpandedIri::Keyword(Keyword::Id), expanded.to_json_ld())
                ]), active_ctx)
            },
            (val, type_mapping) => {
                let result = HashMap::new();
                result.insert(ExpandedIri::Keyword(Keyword::Value), val);
                match type_mapping {
                    None | Some(TypeMapping::Id) | Some(TypeMapping::Vocab) | Some(TypeMapping::None) => LdValue::Null,
                    x => result.insert(ExpandedIri::Keyword(Keyword::Type), LdValue::Iri(ExpandedIri::Keyword(x)));
                };
                if val.is_string() {
                    let lang = active_ctx.term_defs.get(&active_prop)
                        .and_then(|x| x.language_mapping.clone())
                        .or(active_ctx.default_language.clone());
                    let dir = active_ctx.term_defs.get(&active_prop)
                        .and_then(|x| x.direction_mapping.clone())
                        .or(active_ctx.default_base_direction.clone());

                    if let Some(lang) = lang {
                        result.insert(ExpandedIri::Keyword(Keyword::Language), LdValue::from(lang));
                    }

                    if let Some(dir) = dir {
                        result.insert(ExpandedIri::Keyword(Keyword::Direction), LdValue::from(dir.to_string()));
                    }
                }

                (result, active_ctx)
            }
        }
    }.boxed()
}
