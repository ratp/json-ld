use futures_util::future::{BoxFuture, FutureExt};
use iref::{IriBuf, IriRef};
use log::warn;
use std::collections::HashMap;
use std::str::FromStr;

use crate::types::{Keyword, LdValue};
use crate::context::processing::Context;
use crate::context::term_definition::{create_term_def, TermDef};

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum ExpandedIri {
    Null,
    Keyword(Keyword),
    BlankNodeIdent(String),
    Iri(IriBuf),
}

impl ExpandedIri {
    pub fn is_null(&self) -> bool {
        match *self {
            ExpandedIri::Null => true,
            _ => false,
        }
    }

    fn into_iri(self) -> Option<IriBuf> {
        match self {
            ExpandedIri::Iri(i) => Some(i),
            _ => None,
        }
    }

    pub fn to_string(&self) -> Option<String> {
        match *self {
            ExpandedIri::Null => None,
            ExpandedIri::Keyword(k) => Some(k.to_string()),
            ExpandedIri::BlankNodeIdent(ref i) => Some(format!("_:{}", i)), // TODO: check they actually have this format
            ExpandedIri::Iri(ref iri) => Some(iri.to_string())
        }
    }

    pub fn to_json(&self) -> serde_json::Value {
        match self.to_string() {
            None => serde_json::Value::Null,
            Some(s) => serde_json::Value::String(s),
        }
    }

    pub fn to_json_ld(self) -> LdValue {
        match self {
            ExpandedIri::Null => LdValue::Null,
            ExpandedIri::Keyword(k) => LdValue::Keyword(k),
            ExpandedIri::BlankNodeIdent(i) => LdValue::BlankNodeIdent(i),
            ExpandedIri::Iri(i) => LdValue::Iri(i),
        }
    }
}

/// The function `expand_to_iri` is supposed to expand a JSON value.
/// However, the following pattern occurs very often:
///
/// ```rust,ignore
/// let json = serde_json::json!("Hello, world");
/// if let Some(s) = json.as_str() {
///     expand_to_iri(..., serde_json::Value::String(s), ...);
/// }
/// ```
///
/// Which adds complexity both for humans reading/writing code, and
/// for the machine.
///
/// Thus, this trait, to make `expand_to_iri` work with both strings, and JSON.
pub trait IriExpandable {
    fn expand_to_iri<'a>(
        &'a self,
        active_ctx: Context,
        document_relative: Option<bool>,
        vocab: Option<bool>,
        local_ctx: Option<serde_json::Value>,
        defined: Option<HashMap<String, bool>>
    ) -> BoxFuture<'a, (ExpandedIri, Context)>;
}

impl IriExpandable for str {
    fn expand_to_iri<'a>(
        &'a self,
        active_ctx: Context,
        document_relative: Option<bool>,
        vocab: Option<bool>,
        local_ctx: Option<serde_json::Value>,
        defined: Option<HashMap<String, bool>>
    ) -> BoxFuture<'a, (ExpandedIri, Context)> {
        async move {
            let vocab = vocab.unwrap_or(false);
            let document_relative = document_relative.unwrap_or(false);
            if let Ok(kw) = Keyword::from_str(self) {
                (ExpandedIri::Keyword(kw), active_ctx)
            } else {
                let active_ctx = if Keyword::keyword_like(self) {
                    warn!("Invalid keyword-like value");
                    return (ExpandedIri::Null, active_ctx);
                } else {
                    let defined = defined.unwrap_or_default();
                    let (active_ctx, defined) = if let Some(ref local_ctx) = local_ctx {
                        if local_ctx.get(self).is_some() {
                            if !defined.get(self).unwrap_or(&false) {
                                create_term_def(
                                    active_ctx.clone(),
                                    local_ctx.clone(),
                                    self.to_owned(),
                                    defined.clone(),
                                    None,
                                    None,
                                    None,
                                ).await
                                .unwrap_or((active_ctx, defined)) // TODO: is it really OK to ignore errors here?
                            } else {
                                (active_ctx, defined)
                            }
                        } else {
                            (active_ctx, defined)
                        }
                    } else {
                        (active_ctx, defined)
                    };

                    if let Some(TermDef { iri: ExpandedIri::Keyword(kw), .. }) = active_ctx.term_defs.get(self) {
                        return (ExpandedIri::Keyword(*kw), active_ctx)
                    }

                    if vocab {
                        if let Some(TermDef { iri: i, .. }) = active_ctx.term_defs.get(self) {
                            return (i.clone(), active_ctx);
                        }
                    }

                    let (active_ctx, defined) = if self[1..].contains(':') {
                        let mut split = self.splitn(2, ':');
                        let prefix = split.next().unwrap();
                        let suffix = split.next().unwrap();
                        if suffix.starts_with("//") {
                            return (IriBuf::new(self).map(ExpandedIri::Iri).unwrap_or(ExpandedIri::Null), active_ctx)
                        } else if prefix == "_" {
                            return (ExpandedIri::BlankNodeIdent(suffix.to_owned()), active_ctx)
                        } else {
                            let (active_ctx, defined) = if local_ctx.as_ref().map(|x| x.get(prefix).is_some()).unwrap_or(false) && !defined.get(prefix).unwrap_or(&false) {
                                create_term_def(
                                    active_ctx.clone(),
                                    local_ctx.unwrap_or_default(),
                                    prefix.to_owned(),
                                    defined.clone(),
                                    None,
                                    None,
                                    None,
                                ).await
                                .unwrap_or((active_ctx, defined)) // TODO: is it OK to ignore errors here too?
                            } else {
                                (active_ctx, defined)
                            };

                            if let Some(ref def) = active_ctx.term_defs.get(prefix) {
                                if !def.iri.is_null() && def.prefix {
                                    return (def.iri.clone().into_iri().and_then(|pref_iri| {
                                            IriBuf::new(&(pref_iri.to_string() + suffix)).ok()
                                        })
                                        .map(ExpandedIri::Iri)
                                        .unwrap_or(ExpandedIri::Null), active_ctx);
                                }
                            }

                            (active_ctx, defined)
                        }
                    } else {
                        (active_ctx, defined)
                    };

                    if vocab {
                        if let Some(voc) = active_ctx.vocabulary_mapping.clone().to_string() {
                            println!("{} + {}", voc, self);
                            return (IriBuf::new(&(voc + self)).map(ExpandedIri::Iri).unwrap_or(ExpandedIri::Null), active_ctx);
                        }
                    }

                    if document_relative {
                        // TODO: this may do more than the "basic algorithm in section 5.2"
                        return (IriRef::new(self)
                            .map(|i| ExpandedIri::Iri(i.resolved(&active_ctx.base_iri)))
                            .unwrap_or(ExpandedIri::Null),
                            active_ctx);
                    }

                    active_ctx
                };

                return (IriBuf::new(self).map(ExpandedIri::Iri).unwrap_or(ExpandedIri::Null), active_ctx);
            }
        }.boxed()    
    }
}

impl IriExpandable for String {
    fn expand_to_iri<'a>(
        &'a self,
        active_ctx: Context,
        document_relative: Option<bool>,
        vocab: Option<bool>,
        local_ctx: Option<serde_json::Value>,
        defined: Option<HashMap<String, bool>>,
    ) -> BoxFuture<'a, (ExpandedIri, Context)> {
        self[..].expand_to_iri(active_ctx, document_relative, vocab, local_ctx, defined)
    }
}

impl IriExpandable for serde_json::Value {
    /// Expands a JSON value to an IRI, following [the spec](https://www.w3.org/TR/json-ld11-api/#iri-expansion).
    ///
    /// Most of the actual code is in the `str` implementation.
    fn expand_to_iri<'a>(
        &'a self,
        active_ctx: Context,
        document_relative: Option<bool>,
        vocab: Option<bool>,
        local_ctx: Option<serde_json::Value>,
        defined: Option<HashMap<String, bool>>,
    ) -> BoxFuture<'a, (ExpandedIri, Context)> {
        async move {
            match *self {
                serde_json::Value::String(ref value) => value.expand_to_iri(active_ctx, document_relative, vocab, local_ctx, defined).await,
                _ => (ExpandedIri::Null, active_ctx),
            }    
        }.boxed()
    }
}
