use crate::expansion::iri::ExpandedIri;
use crate::types::Keyword;
use iref::IriBuf;
use std::collections::HashMap;

/// Mostly like `serde_json::Value`, but object keys are IRI, not strings.
/// It also adds other variants, to handle parsed JSON-LD data.
#[derive(Clone)]
pub enum LdValue {
    Null,
    Number(f64),
    String(String),
    Iri(IriBuf),
    Keyword(Keyword),
    BlankNodeIdent(String),
    Bool(bool),
    Array(Vec<LdValue>),
    Object(HashMap<ExpandedIri, LdValue>),
}

impl LdValue {
    pub fn to_json(self) -> serde_json::Value {
        match self {
            LdValue::Null => serde_json::Value::Null,
            LdValue::Number(x) => serde_json::Value::from(x),
            LdValue::String(s) => serde_json::Value::String(s),
            LdValue::Iri(i) => i.to_string().into(),
            LdValue::Bool(b) => serde_json::Value::Bool(b),
            LdValue::Array(arr) => serde_json::Value::Array(arr.into_iter().map(LdValue::to_json).collect()),
            LdValue::Object(obj) => {
                let mut hm = serde_json::Map::new();
                for (k, v) in obj {
                    if let Some(k) = k.to_string() {
                        hm.insert(k, v.to_json());
                    }
                }
                serde_json::Value::Object(hm)
            },
        }
    }
}

impl From<&[ (ExpandedIri, LdValue) ]> for LdValue {
    fn from(map: &[ (ExpandedIri, LdValue) ]) -> LdValue {
        let mut hm = HashMap::new();
        for (k, v) in map.into_iter() {
            hm.insert(*k, *v);
        }

        LdValue::Object(hm)
    }
}

impl From<(ExpandedIri, LdValue)> for LdValue {
    fn from((k, v): (ExpandedIri, LdValue)) -> LdValue {
        let mut hm = HashMap::new();
        hm.insert(k, v);
        LdValue::Object(hm)
    }
}

impl From<String> for LdValue {
    fn from(s: String) -> LdValue {
        LdValue::String(s)
    }
}

impl<T> From<Option<T>> for LdValue where T: Into<LdValue> {
    fn from(opt: Option<T>) -> LdValue {
        match opt {
            Some(x) => x.into(),
            None => LdValue::Null,
        }
    }
}
