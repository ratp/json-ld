use std::str::FromStr;



/// All the JSON-LD keywords, as defined in [the
/// specification](https://www.w3.org/TR/json-ld11/#syntax-tokens-and-keywords).
///
/// Keywords are prefixed by a "@" sign in JSON-LD. For instance, `Keyword::Container`
/// is written `"@container"` in JSON-LD.
#[derive(Copy, Clone, Debug, Hash, Eq, PartialEq)]
pub enum Keyword {
    /// Used to set the base IRI against which to resolve those relative
    /// IRI references which are otherwise interpreted relative to the document.
    Base,
    /// Used to set the default container type for a term.
    Container,
    /// Used to define the short-hand names that are used throughout a JSON-LD document.
    /// These short-hand names are called terms and help developers to express
    /// specific identifiers in a compact manner.
    Context,
    /// Used to set the base direction of a JSON-LD value, which are not typed
    /// values (e.g. strings, or language-tagged strings).
    Direction,
    /// Used to express a graph.
    Graph,
    /// Used to uniquely identify node objects that are being described in the document with IRIs
    /// or blank node identifiers. A node reference is a node object containing only the @id property,
    /// which may represent a reference to a node object found elsewhere in the document.
    Id,
    /// Used in a context definition to load an external context within which the containing context
    /// definition is merged. This can be useful to add JSON-LD 1.1 features to JSON-LD 1.0 contexts.
    Import,
    /// Used in a top-level node object to define an included block, for including secondary
    /// node objects within another node object. 
    Included,
    /// Used to specify that a container is used to index information and that
    /// processing should continue deeper into a JSON data structure.
    Index,
    /// Used as the @type value of a JSON literal.
    Json,
    /// Used to specify the language for a particular string value or the default language of a JSON-LD document.
    Language,
    /// Used to express an ordered set of data.
    List,
    /// Used to define a property of a node object that groups together properties of
    /// that node, but is not an edge in the graph.
    Nest,
    /// Used as an index value in an index map, id map, language map, type map, or elsewhere where
    /// a map is used to index into other values, when the indexed node does not have the feature
    /// being indexed.
    None,
    /// With the value true, allows this term to be used to construct a compact IRI when compacting.
    Prefix,
    /// Used in a context definition to change the scope of that context. By default,
    /// it is true, meaning that contexts propagate across node objects (other than
    /// for type-scoped contexts, which default to false). Setting this to false causes
    /// term definitions created within that context to be removed when entering a new node object.
    Propagate,
    /// Used to prevent term definitions of a context to be overridden by other contexts.
    Protected,
    /// Used to express reverse properties.
    Reverse,
    /// Used to express an unordered set of data and to ensure that values are always represented
    /// as arrays.
    Set,
    /// Used to set the type of a node or the datatype of a typed value.
    Type,
    /// Used to specify the data that is associated with a particular property in the graph.
    Value,
    /// Used in a context definition to set the processing mode.
    Version,
    /// Used to expand properties and values in @type with a common prefix IRI.
    Vocab,
}

impl Keyword {
    /// Checks if a string looks like a keyword, meaning that it starts
    /// with a "@", followed by any number of alphabetic characters.
    pub fn keyword_like(s: &str) -> bool {
        let mut chars = s.chars();
        chars.next().map(|c| c == '@').unwrap_or(false) &&
        chars.next().map(|c| c.is_alphabetic()).unwrap_or(false)
    }

    /// Checks if a string is an actual JSON-LD keyword.
    pub fn is_keyword(s: &str) -> bool {
        Keyword::from_str(s).is_ok()
    }
}

impl FromStr for Keyword {
    type Err = ();
    fn from_str(s: &str) -> Result<Keyword, ()> {
        match s {
            "@base" => Ok(Keyword::Base),
            "@container" => Ok(Keyword::Base),
            "@context" => Ok(Keyword::Base),
            "@direction" => Ok(Keyword::Direction),
            "@graph" => Ok(Keyword::Graph),
            "@id" => Ok(Keyword::Id),
            "@import" => Ok(Keyword::Import),
            "@included" => Ok(Keyword::Included),
            "@index" => Ok(Keyword::Index),
            "@json" => Ok(Keyword::Json),
            "@language" => Ok(Keyword::Language),
            "@list" => Ok(Keyword::List),
            "@nest" => Ok(Keyword::List),
            "@none" => Ok(Keyword::None),
            "@prefix" => Ok(Keyword::Prefix),
            "@propagate" => Ok(Keyword::Propagate),
            "@protected" => Ok(Keyword::Protected),
            "@reverse" => Ok(Keyword::Reverse),
            "@set" => Ok(Keyword::Set),
            "@type" => Ok(Keyword::Type),
            "@value" => Ok(Keyword::Value),
            "@version" => Ok(Keyword::Version),
            "@vocab" => Ok(Keyword::Vocab),
            _ => Err(()),
        }
    }
}

impl ToString for Keyword {
    fn to_string(&self) -> String {
        match *self {
            Keyword::Base => "@base",
            Keyword::Container => "@container",
            Keyword::Context => "@context",
            Keyword::Direction => "@direction",
            Keyword::Graph => "@graph",
            Keyword::Id => "@id",
            Keyword::Import => "@import",
            Keyword::Included => "@included",
            Keyword::Index => "@index",
            Keyword::Json => "@json",
            Keyword::Language => "@language",
            Keyword::List => "@list",
            Keyword::Nest => "@nest",
            Keyword::None => "@none",
            Keyword::Prefix => "@prefix",
            Keyword::Propagate => "@propagate",
            Keyword::Protected => "@protected",
            Keyword::Reverse => "@reverse",
            Keyword::Set => "@set",
            Keyword::Type => "@type",
            Keyword::Value => "@value",
            Keyword::Version => "@version",
            Keyword::Vocab => "@vocab",
        }.to_owned()
    }
}


