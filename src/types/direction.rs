use std::str::FromStr;

#[derive(Clone, PartialEq)]
pub enum Direction {
    Ltr,
    Rtl,
}

impl FromStr for Direction {
    type Err = ();

    fn from_str(s: &str) -> Result<Direction, ()> {
        match s {
            "rtl" => Ok(Direction::Rtl),
            "ltr" => Ok(Direction::Ltr),
            _ => Err(()),
        }
    }
}

impl ToString for Direction {
    fn to_string(&self) -> String {
        match *self {
            Direction::Rtl => "rtl".into(),
            Direction::Ltr => "ltr".into(),
        }
    }
}
