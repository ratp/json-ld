use std::str::FromStr;
use iref::IriBuf;

#[derive(Clone, PartialEq)]
pub enum TypeMapping {
    Id,
    Json,
    None,
    Vocab,
    Iri(IriBuf),
}

impl FromStr for TypeMapping {
    type Err = ();

    fn from_str(s: &str) -> Result<TypeMapping, ()> {
        match s {
            "@id" => Ok(TypeMapping::Id),
            "@json" => Ok(TypeMapping::Json),
            "@none" => Ok(TypeMapping::None),
            "@vocab" => Ok(TypeMapping::Vocab),
            other => match IriBuf::new(other) {
                Ok(i) => Ok(TypeMapping::Iri(i)),
                Err(_) => Err(()),
            },
        }
    }
}

impl ToString for TypeMapping {
    fn to_string(&self) -> String {
        match *self {
            TypeMapping::Id => "@id".into(),
            TypeMapping::Json => "@json".into(),
            TypeMapping::None => "@none".into(),
            TypeMapping::Vocab => "@vocab".into(),
            TypeMapping::Iri(ref i) => i.to_string(),
        }
    }
}
